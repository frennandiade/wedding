<?php 
if ($_GET['to'] != '') {
  $requestName = $_GET['to'];
}else{
  $requestName = 'Tamu Yang Terhormat';
}

if ($_GET['music'] == '1') {
  $music = 'backsound2.mp3';
} else {
  $music = 'backsound.mp3';
}

$servername = "localhost";
$username = "id20237047_abdanelma";
$password = "9n()SvxpfiQjXU[b";
$dbname = "id20237047_wedding";

// $servername = "localhost";
// $username = "root";
// $password = "";
// $dbname = "wedding";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$result = mysqli_query($conn,"SELECT * FROM wedding_comment");
$data = mysqli_fetch_all($result);
$commentnya = '';
foreach ($data as $key => $value) {
  $commentnya .= '<li class="comment even thread-even depth-1 wdp-item-comment" id="wdp-item-comment-63709" data-likes="0">
    <div id="wdp-comment-63709" class="wdp-comment wdp-clearfix">
    <div class="wdp-comment-avatar">
        <img alt src="https://ui-avatars.com/api/?name=P&rounded=true&bold=true&background=fcb1fe&format=svg&color=ffffff" srcset="https://ui-avatars.com/api/?name=P&rounded=true&bold=true&background=fcb1fe&format=svg&color=ffffff 2x" height="96" width="96"></img>
    </div><!--.wdp-comment-avatar-->
    <div class="wdp-comment-content">
        <div class="wdp-comment-info">
            <a class="wdp-commenter-name" title="'. $value[1].'">'.$value[1].'</a>
            <span class="wdp-post-author"><i class="fas fa-check-circle"></i> '.$value[4].'</span>
            <br>
            <span class="hubungan">
            — '.$value[2].'
            </span>
        </div>
        <!--.wdp-comment-info-->
        <div class="wdp-comment-text">
            <p>'.$value[3].'</p>
        </div>
        <!--.wdp-comment-text-->
        <div class="wdp-comment-actions">
        </div>
    </div>
  </li><!-- #comment-## -->';
 }
$conn->close();
?>
<!DOCTYPE html>
<html lang="id-ID" prefix="og: https://ogp.me/ns#">
  <head>
    <meta charset="UTF-8">
    <style type="text/css">
      .wdp-comment-text img {
        max-width: 100% !important;
      }
    </style>
    <link media="all" href="wp-content/cache/autoptimize/autoptimize_71a09715590ba5f6e2c9a4d0cfdd8833.css" rel="stylesheet" />
    <link media="screen" href="wp-content/cache/autoptimize/autoptimize_64870496d3056322eb42f1a97cb33e44.css" rel="stylesheet" />
    <title>Abdan &amp; Uke</title>
    <meta name="description" content="Sabtu, 4 Februari 2023" />
    <meta name="robots" content="follow, noindex" />
    <meta property="og:locale" content="id_ID" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="Abdan &amp; Uke" />
    <meta property="og:description" content="Sabtu, 19 Februari 2023" />
    <meta property="og:url" content="abdan-uke/" />
    <meta property="og:site_name" content="wedding" />
    <meta property="article:section" content="Customer" />
    <meta property="og:updated_time" content="2023-01-07T11:03:32+07:00" />
    <meta property="og:image" content="wp-content/uploads/2023/01/lef1.jpg" />
    <meta property="og:image:secure_url" content="wp-content/uploads/2023/01/lef1.jpg" />
    <meta property="og:image:width" content="500" />
    <meta property="og:image:height" content="500" />
    <meta property="og:image:alt" content="Undangan Website" />
    <meta property="og:image:type" content="image/jpeg" />
    <meta property="article:published_time" content="2023-01-05T14:02:39+07:00" />
    <meta property="article:modified_time" content="2023-01-07T11:03:32+07:00" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:title" content="Abdan &amp; Uke" />
    <meta name="twitter:description" content="Sabtu, 4 Februari 2023" />
    <meta name="twitter:image" content="wp-content/uploads/2023/01/lef1.jpg" />
    <meta name="twitter:label1" content="Written by" />
    <meta name="twitter:data1" content="abdanuke" />
    <meta name="twitter:label2" content="Time to read" />
    <meta name="twitter:data2" content="1 menit" />
    <script type="application/ld+json" class="rank-math-schema">
      {
        "@context": "https://schema.org",
        "@graph": [{
          "@type": ["Person", "Organization"],
          "@id": "#person",
          "name": "abdanuke"
        }, {
          "@type": "WebSite",
          "@id": "#website",
          "url": "",
          "name": "abdanuke",
          "publisher": {
            "@id": "#person"
          },
          "inLanguage": "id-ID"
        }, {
          "@type": "ImageObject",
          "@id": "wp-content/uploads/2023/01/left-1.jpg",
          "url": "wp-content/uploads/2023/01/left-1.jpg",
          "width": "500",
          "height": "500",
          "inLanguage": "id-ID"
        }, {
          "@type": "Person",
          "@id": "author/abdanuke/",
          "name": "abdanuke",
          "url": "author/abdanuke/",
          "image": {
            "@type": "ImageObject",
            "@id": "https://secure.gravatar.com/avatar/29e9938da4686c66b8740e7cc2293b6d?s=96&amp;d=mm&amp;r=g",
            "url": "https://secure.gravatar.com/avatar/29e9938da4686c66b8740e7cc2293b6d?s=96&amp;d=mm&amp;r=g",
            "caption": "abdanuke",
            "inLanguage": "id-ID"
          }
        }, {
          "@type": "WebPage",
          "@id": "abdan-uke/#webpage",
          "url": "abdan-uke/",
          "name": "THE WEDDING OF: Abdan &amp; Uke",
          "datePublished": "2023-01-05T14:02:39+07:00",
          "dateModified": "2023-01-07T11:03:32+07:00",
          "author": {
            "@id": "author/abdanuke/"
          },
          "isPartOf": {
            "@id": "#website"
          },
          "primaryImageOfPage": {
            "@id": "wp-content/uploads/2023/01/left-1.jpg"
          },
          "inLanguage": "id-ID"
        }, {
          "@type": "BlogPosting",
          "headline": "THE WEDDING OF: Abdan &amp; Uke",
          "keywords": "Undangan Website,Undangan Online,Undangan Pernikahan Online",
          "datePublished": "2023-01-05T14:02:39+07:00",
          "dateModified": "2023-01-07T11:03:32+07:00",
          "author": {
            "@id": "author/abdanuke/"
          },
          "publisher": {
            "@id": "#person"
          },
          "description": "Sabtu, 19 Februari 2023",
          "name": "THE WEDDING OF: Abdan &amp; Uke",
          "@id": "abdan-uke/#richSnippet",
          "isPartOf": {
            "@id": "abdan-uke/#webpage"
          },
          "image": {
            "@id": "wp-content/uploads/2023/01/left-1.jpg"
          },
          "inLanguage": "id-ID",
          "mainEntityOfPage": {
            "@id": "abdan-uke/#webpage"
          }
        }]
      }
    </script>
    <link rel="alternate" type="application/rss+xml" title="Weddingof &raquo; Feed" href="feed/" />
    <link rel="alternate" type="application/rss+xml" title="Weddingof &raquo; Umpan Komentar" href="comments/feed/" />
    <link rel="alternate" type="application/rss+xml" title="Weddingof &raquo; THE WEDDING OF: Abdan &#038; Uke Umpan Komentar" href="abdan-uke/feed/" />
    <script defer src="data:text/javascript;base64,CndpbmRvdy5fd3BlbW9qaVNldHRpbmdzID0geyJiYXNlVXJsIjoiaHR0cHM6XC9cL3Mudy5vcmdcL2ltYWdlc1wvY29yZVwvZW1vamlcLzE0LjAuMFwvNzJ4NzJcLyIsImV4dCI6Ii5wbmciLCJzdmdVcmwiOiJodHRwczpcL1wvcy53Lm9yZ1wvaW1hZ2VzXC9jb3JlXC9lbW9qaVwvMTQuMC4wXC9zdmdcLyIsInN2Z0V4dCI6Ii5zdmciLCJzb3VyY2UiOnsiY29uY2F0ZW1vamkiOiJodHRwczpcL1wvaW52aXRlYW1zLmNvbVwvd3AtaW5jbHVkZXNcL2pzXC93cC1lbW9qaS1yZWxlYXNlLm1pbi5qcz92ZXI9Ni4xLjEifX07Ci8qISBUaGlzIGZpbGUgaXMgYXV0by1nZW5lcmF0ZWQgKi8KIWZ1bmN0aW9uKGUsYSx0KXt2YXIgbixyLG8saT1hLmNyZWF0ZUVsZW1lbnQoImNhbnZhcyIpLHA9aS5nZXRDb250ZXh0JiZpLmdldENvbnRleHQoIjJkIik7ZnVuY3Rpb24gcyhlLHQpe3ZhciBhPVN0cmluZy5mcm9tQ2hhckNvZGUsZT0ocC5jbGVhclJlY3QoMCwwLGkud2lkdGgsaS5oZWlnaHQpLHAuZmlsbFRleHQoYS5hcHBseSh0aGlzLGUpLDAsMCksaS50b0RhdGFVUkwoKSk7cmV0dXJuIHAuY2xlYXJSZWN0KDAsMCxpLndpZHRoLGkuaGVpZ2h0KSxwLmZpbGxUZXh0KGEuYXBwbHkodGhpcyx0KSwwLDApLGU9PT1pLnRvRGF0YVVSTCgpfWZ1bmN0aW9uIGMoZSl7dmFyIHQ9YS5jcmVhdGVFbGVtZW50KCJzY3JpcHQiKTt0LnNyYz1lLHQuZGVmZXI9dC50eXBlPSJ0ZXh0L2phdmFzY3JpcHQiLGEuZ2V0RWxlbWVudHNCeVRhZ05hbWUoImhlYWQiKVswXS5hcHBlbmRDaGlsZCh0KX1mb3Iobz1BcnJheSgiZmxhZyIsImVtb2ppIiksdC5zdXBwb3J0cz17ZXZlcnl0aGluZzohMCxldmVyeXRoaW5nRXhjZXB0RmxhZzohMH0scj0wO3I8by5sZW5ndGg7cisrKXQuc3VwcG9ydHNbb1tyXV09ZnVuY3Rpb24oZSl7aWYocCYmcC5maWxsVGV4dClzd2l0Y2gocC50ZXh0QmFzZWxpbmU9InRvcCIscC5mb250PSI2MDAgMzJweCBBcmlhbCIsZSl7Y2FzZSJmbGFnIjpyZXR1cm4gcyhbMTI3OTg3LDY1MDM5LDgyMDUsOTg5NSw2NTAzOV0sWzEyNzk4Nyw2NTAzOSw4MjAzLDk4OTUsNjUwMzldKT8hMTohcyhbNTUzNTYsNTY4MjYsNTUzNTYsNTY4MTldLFs1NTM1Niw1NjgyNiw4MjAzLDU1MzU2LDU2ODE5XSkmJiFzKFs1NTM1Niw1NzMzMiw1NjEyOCw1NjQyMyw1NjEyOCw1NjQxOCw1NjEyOCw1NjQyMSw1NjEyOCw1NjQzMCw1NjEyOCw1NjQyMyw1NjEyOCw1NjQ0N10sWzU1MzU2LDU3MzMyLDgyMDMsNTYxMjgsNTY0MjMsODIwMyw1NjEyOCw1NjQxOCw4MjAzLDU2MTI4LDU2NDIxLDgyMDMsNTYxMjgsNTY0MzAsODIwMyw1NjEyOCw1NjQyMyw4MjAzLDU2MTI4LDU2NDQ3XSk7Y2FzZSJlbW9qaSI6cmV0dXJuIXMoWzEyOTc3NywxMjc5OTUsODIwNSwxMjk3NzgsMTI3OTk5XSxbMTI5Nzc3LDEyNzk5NSw4MjAzLDEyOTc3OCwxMjc5OTldKX1yZXR1cm4hMX0ob1tyXSksdC5zdXBwb3J0cy5ldmVyeXRoaW5nPXQuc3VwcG9ydHMuZXZlcnl0aGluZyYmdC5zdXBwb3J0c1tvW3JdXSwiZmxhZyIhPT1vW3JdJiYodC5zdXBwb3J0cy5ldmVyeXRoaW5nRXhjZXB0RmxhZz10LnN1cHBvcnRzLmV2ZXJ5dGhpbmdFeGNlcHRGbGFnJiZ0LnN1cHBvcnRzW29bcl1dKTt0LnN1cHBvcnRzLmV2ZXJ5dGhpbmdFeGNlcHRGbGFnPXQuc3VwcG9ydHMuZXZlcnl0aGluZ0V4Y2VwdEZsYWcmJiF0LnN1cHBvcnRzLmZsYWcsdC5ET01SZWFkeT0hMSx0LnJlYWR5Q2FsbGJhY2s9ZnVuY3Rpb24oKXt0LkRPTVJlYWR5PSEwfSx0LnN1cHBvcnRzLmV2ZXJ5dGhpbmd8fChuPWZ1bmN0aW9uKCl7dC5yZWFkeUNhbGxiYWNrKCl9LGEuYWRkRXZlbnRMaXN0ZW5lcj8oYS5hZGRFdmVudExpc3RlbmVyKCJET01Db250ZW50TG9hZGVkIixuLCExKSxlLmFkZEV2ZW50TGlzdGVuZXIoImxvYWQiLG4sITEpKTooZS5hdHRhY2hFdmVudCgib25sb2FkIixuKSxhLmF0dGFjaEV2ZW50KCJvbnJlYWR5c3RhdGVjaGFuZ2UiLGZ1bmN0aW9uKCl7ImNvbXBsZXRlIj09PWEucmVhZHlTdGF0ZSYmdC5yZWFkeUNhbGxiYWNrKCl9KSksKGU9dC5zb3VyY2V8fHt9KS5jb25jYXRlbW9qaT9jKGUuY29uY2F0ZW1vamkpOmUud3BlbW9qaSYmZS50d2Vtb2ppJiYoYyhlLnR3ZW1vamkpLGMoZS53cGVtb2ppKSkpfSh3aW5kb3csZG9jdW1lbnQsd2luZG93Ll93cGVtb2ppU2V0dGluZ3MpOwo="></script>
    <style type="text/css">
      img.wp-smiley,
      img.emoji {
        display: inline !important;
        border: none !important;
        box-shadow: none !important;
        height: 1em !important;
        width: 1em !important;
        margin: 0 0.07em !important;
        vertical-align: -0.1em !important;
        background: none !important;
        padding: 0 !important;
      }
    </style>
    <style id='global-styles-inline-css' type='text/css'>
      body {
        --wp--preset--color--black: #000000;
        --wp--preset--color--cyan-bluish-gray: #abb8c3;
        --wp--preset--color--white: #ffffff;
        --wp--preset--color--pale-pink: #f78da7;
        --wp--preset--color--vivid-red: #cf2e2e;
        --wp--preset--color--luminous-vivid-orange: #ff6900;
        --wp--preset--color--luminous-vivid-amber: #fcb900;
        --wp--preset--color--light-green-cyan: #7bdcb5;
        --wp--preset--color--vivid-green-cyan: #00d084;
        --wp--preset--color--pale-cyan-blue: #8ed1fc;
        --wp--preset--color--vivid-cyan-blue: #0693e3;
        --wp--preset--color--vivid-purple: #9b51e0;
        --wp--preset--gradient--vivid-cyan-blue-to-vivid-purple: linear-gradient(135deg, rgba(6, 147, 227, 1) 0%, rgb(155, 81, 224) 100%);
        --wp--preset--gradient--light-green-cyan-to-vivid-green-cyan: linear-gradient(135deg, rgb(122, 220, 180) 0%, rgb(0, 208, 130) 100%);
        --wp--preset--gradient--luminous-vivid-amber-to-luminous-vivid-orange: linear-gradient(135deg, rgba(252, 185, 0, 1) 0%, rgba(255, 105, 0, 1) 100%);
        --wp--preset--gradient--luminous-vivid-orange-to-vivid-red: linear-gradient(135deg, rgba(255, 105, 0, 1) 0%, rgb(207, 46, 46) 100%);
        --wp--preset--gradient--very-light-gray-to-cyan-bluish-gray: linear-gradient(135deg, rgb(238, 238, 238) 0%, rgb(169, 184, 195) 100%);
        --wp--preset--gradient--cool-to-warm-spectrum: linear-gradient(135deg, rgb(74, 234, 220) 0%, rgb(151, 120, 209) 20%, rgb(207, 42, 186) 40%, rgb(238, 44, 130) 60%, rgb(251, 105, 98) 80%, rgb(254, 248, 76) 100%);
        --wp--preset--gradient--blush-light-purple: linear-gradient(135deg, rgb(255, 206, 236) 0%, rgb(152, 150, 240) 100%);
        --wp--preset--gradient--blush-bordeaux: linear-gradient(135deg, rgb(254, 205, 165) 0%, rgb(254, 45, 45) 50%, rgb(107, 0, 62) 100%);
        --wp--preset--gradient--luminous-dusk: linear-gradient(135deg, rgb(255, 203, 112) 0%, rgb(199, 81, 192) 50%, rgb(65, 88, 208) 100%);
        --wp--preset--gradient--pale-ocean: linear-gradient(135deg, rgb(255, 245, 203) 0%, rgb(182, 227, 212) 50%, rgb(51, 167, 181) 100%);
        --wp--preset--gradient--electric-grass: linear-gradient(135deg, rgb(202, 248, 128) 0%, rgb(113, 206, 126) 100%);
        --wp--preset--gradient--midnight: linear-gradient(135deg, rgb(2, 3, 129) 0%, rgb(40, 116, 252) 100%);
        --wp--preset--duotone--dark-grayscale: url('#wp-duotone-dark-grayscale');
        --wp--preset--duotone--grayscale: url('#wp-duotone-grayscale');
        --wp--preset--duotone--purple-yellow: url('#wp-duotone-purple-yellow');
        --wp--preset--duotone--blue-red: url('#wp-duotone-blue-red');
        --wp--preset--duotone--midnight: url('#wp-duotone-midnight');
        --wp--preset--duotone--magenta-yellow: url('#wp-duotone-magenta-yellow');
        --wp--preset--duotone--purple-green: url('#wp-duotone-purple-green');
        --wp--preset--duotone--blue-orange: url('#wp-duotone-blue-orange');
        --wp--preset--font-size--small: 13px;
        --wp--preset--font-size--medium: 20px;
        --wp--preset--font-size--large: 36px;
        --wp--preset--font-size--x-large: 42px;
        --wp--preset--spacing--20: 0.44rem;
        --wp--preset--spacing--30: 0.67rem;
        --wp--preset--spacing--40: 1rem;
        --wp--preset--spacing--50: 1.5rem;
        --wp--preset--spacing--60: 2.25rem;
        --wp--preset--spacing--70: 3.38rem;
        --wp--preset--spacing--80: 5.06rem;
      }

      :where(.is-layout-flex) {
        gap: 0.5em;
      }

      body .is-layout-flow>.alignleft {
        float: left;
        margin-inline-start: 0;
        margin-inline-end: 2em;
      }

      body .is-layout-flow>.alignright {
        float: right;
        margin-inline-start: 2em;
        margin-inline-end: 0;
      }

      body .is-layout-flow>.aligncenter {
        margin-left: auto !important;
        margin-right: auto !important;
      }

      body .is-layout-constrained>.alignleft {
        float: left;
        margin-inline-start: 0;
        margin-inline-end: 2em;
      }

      body .is-layout-constrained>.alignright {
        float: right;
        margin-inline-start: 2em;
        margin-inline-end: 0;
      }

      body .is-layout-constrained>.aligncenter {
        margin-left: auto !important;
        margin-right: auto !important;
      }

      body .is-layout-constrained> :where(:not(.alignleft):not(.alignright):not(.alignfull)) {
        max-width: var(--wp--style--global--content-size);
        margin-left: auto !important;
        margin-right: auto !important;
      }

      body .is-layout-constrained>.alignwide {
        max-width: var(--wp--style--global--wide-size);
      }

      body .is-layout-flex {
        display: flex;
      }

      body .is-layout-flex {
        flex-wrap: wrap;
        align-items: center;
      }

      body .is-layout-flex>* {
        margin: 0;
      }

      :where(.wp-block-columns.is-layout-flex) {
        gap: 2em;
      }

      .has-black-color {
        color: var(--wp--preset--color--black) !important;
      }

      .has-cyan-bluish-gray-color {
        color: var(--wp--preset--color--cyan-bluish-gray) !important;
      }

      .has-white-color {
        color: var(--wp--preset--color--white) !important;
      }

      .has-pale-pink-color {
        color: var(--wp--preset--color--pale-pink) !important;
      }

      .has-vivid-red-color {
        color: var(--wp--preset--color--vivid-red) !important;
      }

      .has-luminous-vivid-orange-color {
        color: var(--wp--preset--color--luminous-vivid-orange) !important;
      }

      .has-luminous-vivid-amber-color {
        color: var(--wp--preset--color--luminous-vivid-amber) !important;
      }

      .has-light-green-cyan-color {
        color: var(--wp--preset--color--light-green-cyan) !important;
      }

      .has-vivid-green-cyan-color {
        color: var(--wp--preset--color--vivid-green-cyan) !important;
      }

      .has-pale-cyan-blue-color {
        color: var(--wp--preset--color--pale-cyan-blue) !important;
      }

      .has-vivid-cyan-blue-color {
        color: var(--wp--preset--color--vivid-cyan-blue) !important;
      }

      .has-vivid-purple-color {
        color: var(--wp--preset--color--vivid-purple) !important;
      }

      .has-black-background-color {
        background-color: var(--wp--preset--color--black) !important;
      }

      .has-cyan-bluish-gray-background-color {
        background-color: var(--wp--preset--color--cyan-bluish-gray) !important;
      }

      .has-white-background-color {
        background-color: var(--wp--preset--color--white) !important;
      }

      .has-pale-pink-background-color {
        background-color: var(--wp--preset--color--pale-pink) !important;
      }

      .has-vivid-red-background-color {
        background-color: var(--wp--preset--color--vivid-red) !important;
      }

      .has-luminous-vivid-orange-background-color {
        background-color: var(--wp--preset--color--luminous-vivid-orange) !important;
      }

      .has-luminous-vivid-amber-background-color {
        background-color: var(--wp--preset--color--luminous-vivid-amber) !important;
      }

      .has-light-green-cyan-background-color {
        background-color: var(--wp--preset--color--light-green-cyan) !important;
      }

      .has-vivid-green-cyan-background-color {
        background-color: var(--wp--preset--color--vivid-green-cyan) !important;
      }

      .has-pale-cyan-blue-background-color {
        background-color: var(--wp--preset--color--pale-cyan-blue) !important;
      }

      .has-vivid-cyan-blue-background-color {
        background-color: var(--wp--preset--color--vivid-cyan-blue) !important;
      }

      .has-vivid-purple-background-color {
        background-color: var(--wp--preset--color--vivid-purple) !important;
      }

      .has-black-border-color {
        border-color: var(--wp--preset--color--black) !important;
      }

      .has-cyan-bluish-gray-border-color {
        border-color: var(--wp--preset--color--cyan-bluish-gray) !important;
      }

      .has-white-border-color {
        border-color: var(--wp--preset--color--white) !important;
      }

      .has-pale-pink-border-color {
        border-color: var(--wp--preset--color--pale-pink) !important;
      }

      .has-vivid-red-border-color {
        border-color: var(--wp--preset--color--vivid-red) !important;
      }

      .has-luminous-vivid-orange-border-color {
        border-color: var(--wp--preset--color--luminous-vivid-orange) !important;
      }

      .has-luminous-vivid-amber-border-color {
        border-color: var(--wp--preset--color--luminous-vivid-amber) !important;
      }

      .has-light-green-cyan-border-color {
        border-color: var(--wp--preset--color--light-green-cyan) !important;
      }

      .has-vivid-green-cyan-border-color {
        border-color: var(--wp--preset--color--vivid-green-cyan) !important;
      }

      .has-pale-cyan-blue-border-color {
        border-color: var(--wp--preset--color--pale-cyan-blue) !important;
      }

      .has-vivid-cyan-blue-border-color {
        border-color: var(--wp--preset--color--vivid-cyan-blue) !important;
      }

      .has-vivid-purple-border-color {
        border-color: var(--wp--preset--color--vivid-purple) !important;
      }

      .has-vivid-cyan-blue-to-vivid-purple-gradient-background {
        background: var(--wp--preset--gradient--vivid-cyan-blue-to-vivid-purple) !important;
      }

      .has-light-green-cyan-to-vivid-green-cyan-gradient-background {
        background: var(--wp--preset--gradient--light-green-cyan-to-vivid-green-cyan) !important;
      }

      .has-luminous-vivid-amber-to-luminous-vivid-orange-gradient-background {
        background: var(--wp--preset--gradient--luminous-vivid-amber-to-luminous-vivid-orange) !important;
      }

      .has-luminous-vivid-orange-to-vivid-red-gradient-background {
        background: var(--wp--preset--gradient--luminous-vivid-orange-to-vivid-red) !important;
      }

      .has-very-light-gray-to-cyan-bluish-gray-gradient-background {
        background: var(--wp--preset--gradient--very-light-gray-to-cyan-bluish-gray) !important;
      }

      .has-cool-to-warm-spectrum-gradient-background {
        background: var(--wp--preset--gradient--cool-to-warm-spectrum) !important;
      }

      .has-blush-light-purple-gradient-background {
        background: var(--wp--preset--gradient--blush-light-purple) !important;
      }

      .has-blush-bordeaux-gradient-background {
        background: var(--wp--preset--gradient--blush-bordeaux) !important;
      }

      .has-luminous-dusk-gradient-background {
        background: var(--wp--preset--gradient--luminous-dusk) !important;
      }

      .has-pale-ocean-gradient-background {
        background: var(--wp--preset--gradient--pale-ocean) !important;
      }

      .has-electric-grass-gradient-background {
        background: var(--wp--preset--gradient--electric-grass) !important;
      }

      .has-midnight-gradient-background {
        background: var(--wp--preset--gradient--midnight) !important;
      }

      .has-small-font-size {
        font-size: var(--wp--preset--font-size--small) !important;
      }

      .has-medium-font-size {
        font-size: var(--wp--preset--font-size--medium) !important;
      }

      .has-large-font-size {
        font-size: var(--wp--preset--font-size--large) !important;
      }

      .has-x-large-font-size {
        font-size: var(--wp--preset--font-size--x-large) !important;
      }

      .wp-block-navigation a:where(:not(.wp-element-button)) {
        color: inherit;
      }

      :where(.wp-block-columns.is-layout-flex) {
        gap: 2em;
      }

      .wp-block-pullquote {
        font-size: 1.5em;
        line-height: 1.6;
      }
    </style>
    <style id='wdp_style-inline-css' type='text/css'>
      .wdp-wrapper {
        font-size: 14px
      }

      .wdp-wrapper {
        background: #0a0909;
      }

      .wdp-wrapper.wdp-border {
        border: 1px solid #d5deea;
      }

      .wdp-wrapper .wdp-wrap-comments a:link,
      .wdp-wrapper .wdp-wrap-comments a:visited {
        color: #3D7DBC;
      }

      .wdp-wrapper .wdp-wrap-link a.wdp-link {
        color: #3D7DBC;
      }

      .wdp-wrapper .wdp-wrap-link a.wdp-link.wdp-icon-link-true .wdpo-comment {
        color: #3D7DBC;
      }

      .wdp-wrapper .wdp-wrap-link a.wdp-link:hover {
        color: #2a5782;
      }

      .wdp-wrapper .wdp-wrap-link a.wdp-link:hover .wdpo-comment {
        color: #2a5782;
      }

      .wdp-wrapper .wdp-wrap-form {
        border-top: 1px solid #d5deea;
      }

      .wdp-wrapper .wdp-wrap-form .wdp-container-form textarea.wdp-textarea {
        border: 1px solid #d5deea;
        background: #FFFFFF;
        color: #44525F;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-info .wdp-post-author {
        background: #777;
      }

      .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type='text'] {
        border: 1px solid #d5deea;
        background: #FFFFFF;
        color: #44525F;
      }

      .wdp-wrapper .wdp-wrap-form .wdp-container-form input.wdp-input:focus,
      .wdp-wrapper .wdp-wrap-form .wdp-container-form textarea.wdp-textarea:focus {
        border-color: #64B6EC;
      }

      .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type='submit'],
      .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type='button'].wdp-form-btn {
        color: #FFFFFF;
        background: #3D7DBC;
      }

      .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type='submit']:hover,
      .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type='button'].wdp-form-btn:hover {
        background: #4d8ac5;
      }

      .wdp-wrapper .wdp-wrap-form .wdp-container-form .wdp-captcha .wdp-captcha-text {
        color: #44525F;
      }

      .wdp-wrapper .wdp-media-btns a>span {
        color: #3D7DBC;
      }

      .wdp-wrapper .wdp-media-btns a>span:hover {
        color: #2a5782;
      }

      .wdp-wrapper .wdp-comment-status {
        border-top: 1px solid #d5deea;
      }

      .wdp-wrapper .wdp-comment-status p.wdp-ajax-success {
        color: #319342;
      }

      .wdp-wrapper .wdp-comment-status p.wdp-ajax-error {
        color: #C85951;
      }

      .wdp-wrapper .wdp-comment-status.wdp-loading>span {
        color: #3D7DBC;
      }

      .wdp-wrapper ul.wdp-container-comments {
        border-top: 1px solid #d5deea;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment {
        border-bottom: 1px solid #d5deea;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment ul li.wdp-item-comment {
        border-top: 1px solid #d5deea;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-info a.wdp-commenter-name {
        color: #3D7DBC !important;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-info a.wdp-commenter-name:hover {
        color: #2a5782 !important;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-info .wdp-comment-time {
        color: #9DA8B7;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-text p {
        color: #44525F;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-actions a {
        color: #3D7DBC;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-actions a:hover {
        color: #2a5782;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-link>span {
        color: #c9cfd7;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-link>span:hover {
        color: #3D7DBC;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-count {
        color: #9DA8B7;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-count.wdp-rating-positive {
        color: #2C9E48;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-count.wdp-rating-negative {
        color: #D13D3D;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-count.wdpo-loading {
        color: #c9cfd7;
      }

      .wdp-wrapper ul.wdp-container-comments a.wdp-load-more-comments:hover {
        color: #2a5782;
      }

      .wdp-wrapper .wdp-counter-info {
        color: #9DA8B7;
      }

      .wdp-wrapper .wdp-holder span {
        color: #3D7DBC;
      }

      .wdp-wrapper .wdp-holder a,
      .wdp-wrapper .wdp-holder a:link,
      .wdp-wrapper .wdp-holder a:visited {
        color: #3D7DBC;
      }

      .wdp-wrapper .wdp-holder a:hover,
      .wdp-wrapper .wdp-holder a:link:hover,
      .wdp-wrapper .wdp-holder a:visited:hover {
        color: #2a5782;
      }

      .wdp-wrapper .wdp-holder a.jp-previous.jp-disabled,
      .wdp-wrapper .wdp-holder a.jp-previous.jp-disabled:hover,
      .wdp-wrapper .wdp-holder a.jp-next.jp-disabled,
      .wdp-wrapper .wdp-holder a.jp-next.jp-disabled:hover {
        color: #9DA8B7;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-avatar img {
        max-width: 28px;
        max-height: 28px;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content {
        margin-left: 38px;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment ul .wdp-comment-avatar img {
        max-width: 24px;
        max-height: 24px;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment ul ul .wdp-comment-avatar img {
        max-width: 21px;
        max-height: 21px;
      }
    </style>
    <style id='elementor-frontend-inline-css' type='text/css'>
      .elementor-kit-5 {
        --e-global-color-primary: #6EC1E4;
        --e-global-color-secondary: #54595F;
        --e-global-color-text: #7A7A7A;
        --e-global-color-accent: #61CE70;
        --e-global-typography-primary-font-family: "Roboto";
        --e-global-typography-primary-font-weight: 600;
        --e-global-typography-secondary-font-family: "Roboto Slab";
        --e-global-typography-secondary-font-weight: 400;
        --e-global-typography-text-font-family: "Roboto";
        --e-global-typography-text-font-weight: 400;
        --e-global-typography-accent-font-family: "Roboto";
        --e-global-typography-accent-font-weight: 500;
        background-color: #FFFFFF;
      }

      .elementor-section.elementor-section-boxed>.elementor-container {
        max-width: 1140px;
      }

      .elementor-widget:not(:last-child) {
        margin-bottom: 20px;
      }

        {}

      h1.entry-title {
        display: var(--page-title-display);
      }

      @media(max-width:1024px) {
        .elementor-section.elementor-section-boxed>.elementor-container {
          max-width: 1024px;
        }
      }

      @media(max-width:767px) {
        .elementor-section.elementor-section-boxed>.elementor-container {
          max-width: 767px;
        }
      }

      .elementor-widget-heading .elementor-heading-title {
        color: var(--e-global-color-primary);
        font-family: var(--e-global-typography-primary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-primary-font-weight);
      }

      .elementor-widget-image .widget-image-caption {
        color: var(--e-global-color-text);
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-text-editor {
        color: var(--e-global-color-text);
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-text-editor.elementor-drop-cap-view-stacked .elementor-drop-cap {
        background-color: var(--e-global-color-primary);
      }

      .elementor-widget-text-editor.elementor-drop-cap-view-framed .elementor-drop-cap,
      .elementor-widget-text-editor.elementor-drop-cap-view-default .elementor-drop-cap {
        color: var(--e-global-color-primary);
        border-color: var(--e-global-color-primary);
      }

      .elementor-widget-button .elementor-button {
        font-family: var(--e-global-typography-accent-font-family), Sans-serif;
        font-weight: var(--e-global-typography-accent-font-weight);
        background-color: var(--e-global-color-accent);
      }

      .elementor-widget-divider {
        --divider-color: var(--e-global-color-secondary);
      }

      .elementor-widget-divider .elementor-divider__text {
        color: var(--e-global-color-secondary);
        font-family: var(--e-global-typography-secondary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-secondary-font-weight);
      }

      .elementor-widget-divider.elementor-view-stacked .elementor-icon {
        background-color: var(--e-global-color-secondary);
      }

      .elementor-widget-divider.elementor-view-framed .elementor-icon,
      .elementor-widget-divider.elementor-view-default .elementor-icon {
        color: var(--e-global-color-secondary);
        border-color: var(--e-global-color-secondary);
      }

      .elementor-widget-divider.elementor-view-framed .elementor-icon,
      .elementor-widget-divider.elementor-view-default .elementor-icon svg {
        fill: var(--e-global-color-secondary);
      }

      .elementor-widget-image-box .elementor-image-box-title {
        color: var(--e-global-color-primary);
        font-family: var(--e-global-typography-primary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-primary-font-weight);
      }

      .elementor-widget-image-box .elementor-image-box-description {
        color: var(--e-global-color-text);
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-icon.elementor-view-stacked .elementor-icon {
        background-color: var(--e-global-color-primary);
      }

      .elementor-widget-icon.elementor-view-framed .elementor-icon,
      .elementor-widget-icon.elementor-view-default .elementor-icon {
        color: var(--e-global-color-primary);
        border-color: var(--e-global-color-primary);
      }

      .elementor-widget-icon.elementor-view-framed .elementor-icon,
      .elementor-widget-icon.elementor-view-default .elementor-icon svg {
        fill: var(--e-global-color-primary);
      }

      .elementor-widget-icon-box.elementor-view-stacked .elementor-icon {
        background-color: var(--e-global-color-primary);
      }

      .elementor-widget-icon-box.elementor-view-framed .elementor-icon,
      .elementor-widget-icon-box.elementor-view-default .elementor-icon {
        fill: var(--e-global-color-primary);
        color: var(--e-global-color-primary);
        border-color: var(--e-global-color-primary);
      }

      .elementor-widget-icon-box .elementor-icon-box-title {
        color: var(--e-global-color-primary);
      }

      .elementor-widget-icon-box .elementor-icon-box-title,
      .elementor-widget-icon-box .elementor-icon-box-title a {
        font-family: var(--e-global-typography-primary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-primary-font-weight);
      }

      .elementor-widget-icon-box .elementor-icon-box-description {
        color: var(--e-global-color-text);
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-star-rating .elementor-star-rating__title {
        color: var(--e-global-color-text);
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-image-gallery .gallery-item .gallery-caption {
        font-family: var(--e-global-typography-accent-font-family), Sans-serif;
        font-weight: var(--e-global-typography-accent-font-weight);
      }

      .elementor-widget-icon-list .elementor-icon-list-item:not(:last-child):after {
        border-color: var(--e-global-color-text);
      }

      .elementor-widget-icon-list .elementor-icon-list-icon i {
        color: var(--e-global-color-primary);
      }

      .elementor-widget-icon-list .elementor-icon-list-icon svg {
        fill: var(--e-global-color-primary);
      }

      .elementor-widget-icon-list .elementor-icon-list-text {
        color: var(--e-global-color-secondary);
      }

      .elementor-widget-icon-list .elementor-icon-list-item>.elementor-icon-list-text,
      .elementor-widget-icon-list .elementor-icon-list-item>a {
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-counter .elementor-counter-number-wrapper {
        color: var(--e-global-color-primary);
        font-family: var(--e-global-typography-primary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-primary-font-weight);
      }

      .elementor-widget-counter .elementor-counter-title {
        color: var(--e-global-color-secondary);
        font-family: var(--e-global-typography-secondary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-secondary-font-weight);
      }

      .elementor-widget-progress .elementor-progress-wrapper .elementor-progress-bar {
        background-color: var(--e-global-color-primary);
      }

      .elementor-widget-progress .elementor-title {
        color: var(--e-global-color-primary);
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-testimonial .elementor-testimonial-content {
        color: var(--e-global-color-text);
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-testimonial .elementor-testimonial-name {
        color: var(--e-global-color-primary);
        font-family: var(--e-global-typography-primary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-primary-font-weight);
      }

      .elementor-widget-testimonial .elementor-testimonial-job {
        color: var(--e-global-color-secondary);
        font-family: var(--e-global-typography-secondary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-secondary-font-weight);
      }

      .elementor-widget-tabs .elementor-tab-title,
      .elementor-widget-tabs .elementor-tab-title a {
        color: var(--e-global-color-primary);
      }

      .elementor-widget-tabs .elementor-tab-title.elementor-active,
      .elementor-widget-tabs .elementor-tab-title.elementor-active a {
        color: var(--e-global-color-accent);
      }

      .elementor-widget-tabs .elementor-tab-title {
        font-family: var(--e-global-typography-primary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-primary-font-weight);
      }

      .elementor-widget-tabs .elementor-tab-content {
        color: var(--e-global-color-text);
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-accordion .elementor-accordion-icon,
      .elementor-widget-accordion .elementor-accordion-title {
        color: var(--e-global-color-primary);
      }

      .elementor-widget-accordion .elementor-accordion-icon svg {
        fill: var(--e-global-color-primary);
      }

      .elementor-widget-accordion .elementor-active .elementor-accordion-icon,
      .elementor-widget-accordion .elementor-active .elementor-accordion-title {
        color: var(--e-global-color-accent);
      }

      .elementor-widget-accordion .elementor-active .elementor-accordion-icon svg {
        fill: var(--e-global-color-accent);
      }

      .elementor-widget-accordion .elementor-accordion-title {
        font-family: var(--e-global-typography-primary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-primary-font-weight);
      }

      .elementor-widget-accordion .elementor-tab-content {
        color: var(--e-global-color-text);
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-toggle .elementor-toggle-title,
      .elementor-widget-toggle .elementor-toggle-icon {
        color: var(--e-global-color-primary);
      }

      .elementor-widget-toggle .elementor-toggle-icon svg {
        fill: var(--e-global-color-primary);
      }

      .elementor-widget-toggle .elementor-tab-title.elementor-active a,
      .elementor-widget-toggle .elementor-tab-title.elementor-active .elementor-toggle-icon {
        color: var(--e-global-color-accent);
      }

      .elementor-widget-toggle .elementor-toggle-title {
        font-family: var(--e-global-typography-primary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-primary-font-weight);
      }

      .elementor-widget-toggle .elementor-tab-content {
        color: var(--e-global-color-text);
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-alert .elementor-alert-title {
        font-family: var(--e-global-typography-primary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-primary-font-weight);
      }

      .elementor-widget-alert .elementor-alert-description {
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-weddingpress-whatsapp a.elementor-button,
      .elementor-widget-weddingpress-whatsapp .elementor-button {
        font-family: var(--e-global-typography-accent-font-family), Sans-serif;
        font-weight: var(--e-global-typography-accent-font-weight);
        background-color: var(--e-global-color-accent);
      }

      .elementor-widget-weddingpress-countdown .wpkoi-elements-countdown-digits {
        font-family: var(--e-global-typography-secondary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-secondary-font-weight);
      }

      .elementor-widget-weddingpress-countdown .wpkoi-elements-countdown-label {
        font-family: var(--e-global-typography-secondary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-secondary-font-weight);
      }

      .elementor-widget-weddingpress-countdown .wpkoi-elements-countdown-digits::after {
        font-family: var(--e-global-typography-secondary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-secondary-font-weight);
      }

      .elementor-widget-weddingpress-guestbook .guestbook-name {
        font-family: var(--e-global-typography-accent-font-family), Sans-serif;
        font-weight: var(--e-global-typography-accent-font-weight);
      }

      .elementor-widget-weddingpress-guestbook .guestbook-message {
        font-family: var(--e-global-typography-accent-font-family), Sans-serif;
        font-weight: var(--e-global-typography-accent-font-weight);
      }

      .elementor-widget-weddingpress-guestbook .wdp-confirm {
        font-family: var(--e-global-typography-accent-font-family), Sans-serif;
        font-weight: var(--e-global-typography-accent-font-weight);
      }

      .elementor-widget-weddingpress-guestbook a.elementor-button,
      .elementor-widget-weddingpress-guestbook .elementor-button {
        font-family: var(--e-global-typography-accent-font-family), Sans-serif;
        font-weight: var(--e-global-typography-accent-font-weight);
        background-color: var(--e-global-color-accent);
      }

      .elementor-widget-weddingpress-timeline .twae-wrapper .twae-year {
        font-family: var(--e-global-typography-primary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-primary-font-weight);
        color: var(--e-global-color-secondary);
        background-color: var(--e-global-color-secondary);
      }

      .elementor-widget-weddingpress-timeline .twae-wrapper span.twae-label {
        font-family: var(--e-global-typography-primary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-primary-font-weight);
        color: var(--e-global-color-secondary);
      }

      .elementor-widget-weddingpress-timeline .twae-wrapper span.twae-extra-label {
        font-family: var(--e-global-typography-primary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-primary-font-weight);
        color: var(--e-global-color-secondary);
      }

      .elementor-widget-weddingpress-timeline .twae-wrapper span.twae-title {
        font-family: var(--e-global-typography-primary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-primary-font-weight);
      }

      .elementor-widget-weddingpress-timeline .twae-wrapper .twae-description {
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
        color: var(--e-global-color-secondary);
      }

      .elementor-widget-weddingpress-timeline .twae-wrapper .twae-data-container span.twae-title {
        color: var(--e-global-color-secondary);
      }

      .elementor-widget-weddingpress-timeline .twae-horizontal span.twae-title {
        color: var(--e-global-color-secondary);
      }

      .elementor-widget-weddingpress-timeline .twae-wrapper .twae-line::before {
        background-color: var(--e-global-color-secondary);
      }

      .elementor-widget-weddingpress-timeline .twae-wrapper .twae-timeline-centered .twae-icon {
        border-color: var(--e-global-color-secondary);
      }

      .elementor-widget-weddingpress-timeline .twae-wrapper .twae-timeline-centered .twae-year {
        border-color: var(--e-global-color-secondary);
      }

      .elementor-widget-weddingpress-timeline .twae-wrapper:before {
        background-color: var(--e-global-color-secondary);
      }

      .elementor-widget-weddingpress-timeline .twae-wrapper:after {
        background-color: var(--e-global-color-secondary);
      }

      .elementor-widget-weddingpress-timeline .twae-horizontal .twae-pagination.swiper-pagination-progressbar {
        background-color: var(--e-global-color-secondary);
      }

      .elementor-widget-weddingpress-timeline .twae-horizontal .twae-button-prev {
        color: var(--e-global-color-secondary);
      }

      .elementor-widget-weddingpress-timeline .twae-horizontal .twae-button-next {
        color: var(--e-global-color-secondary);
      }

      .elementor-widget-weddingpress-timeline .twae-wrapper .twae-icon {
        background-color: var(--e-global-color-secondary);
        color: var(--e-global-color-secondary);
      }

      .elementor-widget-weddingpress-timeline .twae-horizontal .twae-story-info {
        border-color: var(--e-global-color-secondary);
        background-color: var(--e-global-color-secondary);
      }

      .elementor-widget-weddingpress-timeline .twae-horizontal .twae-story-info:before {
        border-bottom-color: var(--e-global-color-secondary);
      }

      .elementor-widget-weddingpress-timeline .twae-wrapper .twae-data-container:after {
        border-right-color: var(--e-global-color-secondary);
      }

      .elementor-widget-weddingpress-timeline .twae-wrapper .twae-right-aligned .twae-data-container {
        border-left-color: var(--e-global-color-secondary);
      }

      .elementor-widget-weddingpress-timeline .twae-wrapper .twae-left-aligned .twae-data-container {
        border-right-color: var(--e-global-color-secondary);
      }

      body[data-elementor-device-mode=mobile] .elementor-widget-weddingpress-timeline .twae-wrapper .twae-left-aligned .twae-data-container {
        border-left-color: var(--e-global-color-secondary);
      }

      .elementor-widget-weddingpress-timeline .twae-wrapper .twae-data-container {
        background-color: var(--e-global-color-secondary);
      }

      .elementor-widget-weddingpress-audio.elementor-view-stacked .elementor-icon {
        background-color: var(--e-global-color-primary);
      }

      .elementor-widget-weddingpress-audio.elementor-view-framed .elementor-icon,
      .elementor-widget-weddingpress-audio.elementor-view-default .elementor-icon {
        color: var(--e-global-color-primary);
        border-color: var(--e-global-color-primary);
      }

      .elementor-widget-weddingpress-audio.elementor-view-framed .elementor-icon,
      .elementor-widget-weddingpress-audio.elementor-view-default .elementor-icon svg {
        fill: var(--e-global-color-primary);
      }

      .elementor-widget-weddingpress-forms .elementor-wdp-form-wrapper label {
        font-family: var(--e-global-typography-accent-font-family), Sans-serif;
        font-weight: var(--e-global-typography-accent-font-weight);
      }

      .elementor-widget-weddingpress-forms .elementor-wdp-form-wrapper input[type="text"],
      .elementor-widget-weddingpress-forms .elementor-wdp-form-wrapper input[type="jumlah"],
      .elementor-widget-weddingpress-forms .elementor-wdp-form-wrapper textarea {
        font-family: var(--e-global-typography-accent-font-family), Sans-serif;
        font-weight: var(--e-global-typography-accent-font-weight);
      }

      .elementor-widget-weddingpress-forms .elementor-wdp-form-wrapper input[type="submit"],
      .elementor-widget-weddingpress-forms .elementor-wdp-form-wrapper button {
        font-family: var(--e-global-typography-accent-font-family), Sans-serif;
        font-weight: var(--e-global-typography-accent-font-weight);
        background-color: var(--e-global-color-accent);
      }

      .elementor-widget-weddingpress-forms label.to-select-option {
        font-family: var(--e-global-typography-accent-font-family), Sans-serif;
        font-weight: var(--e-global-typography-accent-font-weight);
      }

      .elementor-widget-weddingpress-wellcome a.elementor-button,
      .elementor-widget-weddingpress-wellcome .elementor-button {
        font-family: var(--e-global-typography-accent-font-family), Sans-serif;
        font-weight: var(--e-global-typography-accent-font-weight);
        background-color: var(--e-global-color-accent);
      }

      .elementor-widget-weddingpress-generator-kit .wdp-form-wrapper-kit input[type="text"],
      .elementor-widget-weddingpress-generator-kit .wdp-form-wrapper-kit input[type="email"],
      .elementor-widget-weddingpress-generator-kit .wdp-form-wrapper-kit textarea {
        font-family: var(--e-global-typography-accent-font-family), Sans-serif;
        font-weight: var(--e-global-typography-accent-font-weight);
      }

      .elementor-widget-weddingpress-generator-kit .wdp-form-wrapper-kit input[type="submit"],
      .elementor-widget-weddingpress-generator-kit .wdp-form-wrapper-kit button {
        font-family: var(--e-global-typography-accent-font-family), Sans-serif;
        font-weight: var(--e-global-typography-accent-font-weight);
        background-color: var(--e-global-color-accent);
      }

      .elementor-widget-weddingpress-copy-text .copy-content {
        font-family: var(--e-global-typography-accent-font-family), Sans-serif;
        font-weight: var(--e-global-typography-accent-font-weight);
      }

      .elementor-widget-weddingpress-copy-text .head-title {
        font-family: var(--e-global-typography-accent-font-family), Sans-serif;
        font-weight: var(--e-global-typography-accent-font-weight);
      }

      .elementor-widget-weddingpress-copy-text a.elementor-button,
      .elementor-widget-weddingpress-copy-text .elementor-button {
        font-family: var(--e-global-typography-accent-font-family), Sans-serif;
        font-weight: var(--e-global-typography-accent-font-weight);
        background-color: var(--e-global-color-accent);
      }

      .elementor-widget-weddingpress-senderkit .elementor-wdp-form-wrapper label {
        font-family: var(--e-global-typography-accent-font-family), Sans-serif;
        font-weight: var(--e-global-typography-accent-font-weight);
      }

      .elementor-widget-weddingpress-senderkit .elementor-wdp-form-wrapper input[type="text"],
      .elementor-widget-weddingpress-senderkit .elementor-wdp-form-wrapper input[type="acara"],
      .elementor-widget-weddingpress-senderkit .elementor-wdp-form-wrapper textarea {
        font-family: var(--e-global-typography-accent-font-family), Sans-serif;
        font-weight: var(--e-global-typography-accent-font-weight);
      }

      .elementor-widget-weddingpress-senderkit .elementor-wdp-form-wrapper input[type="submit"],
      .elementor-widget-weddingpress-senderkit .elementor-wdp-form-wrapper button {
        font-family: var(--e-global-typography-accent-font-family), Sans-serif;
        font-weight: var(--e-global-typography-accent-font-weight);
        background-color: var(--e-global-color-accent);
      }

      .elementor-widget-weddingpress-senderkit label.to-select-option {
        font-family: var(--e-global-typography-accent-font-family), Sans-serif;
        font-weight: var(--e-global-typography-accent-font-weight);
      }

      .elementor-widget-bdt-qrcode .wdp-form-wrapper-kit input[type="text"],
      .elementor-widget-bdt-qrcode .wdp-form-wrapper-kit input[type="email"],
      .elementor-widget-bdt-qrcode .wdp-form-wrapper-kit textarea {
        font-family: var(--e-global-typography-accent-font-family), Sans-serif;
        font-weight: var(--e-global-typography-accent-font-weight);
      }

      .elementor-widget-bdt-qrcode .wdp-form-wrapper-kit input[type="submit"],
      .elementor-widget-bdt-qrcode .wdp-form-wrapper-kit button {
        font-family: var(--e-global-typography-accent-font-family), Sans-serif;
        font-weight: var(--e-global-typography-accent-font-weight);
        background-color: var(--e-global-color-accent);
      }

      .elementor-widget-ucaddon_unlimited_timeline .ue_text_one {
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-ucaddon_unlimited_timeline .ue_text_two {
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-ucaddon_unlimited_timeline .ue_text_three {
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-ucaddon_unlimited_timeline .ue_timeline_item_title {
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-ucaddon_unlimited_timeline .ue_timeline_item_subtitle {
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-ucaddon_unlimited_timeline .ue_timeline_item_text {
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-ucaddon_navigation .uc_style_navigation_elementor_uc_items_attribute_text_icon {
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-ucaddon_Home .uc_style_home_elementor_uc_items_attribute_text_icon {
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-ucaddon_navigation_editable .uc_style_navigation_editable_elementor_uc_items_attribute_text_icon {
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-ucaddon_Navigasi_Home_V2_Editable .uc_style_navigasi_home_v2_editable_elementor_uc_items_attribute_text_icon {
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-ucaddon_Home_2 .uc_style_home_2_elementor_uc_items_attribute_text_icon {
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-ucaddon_audio_player_mejs .ue-audio-player__element--audio-title {
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-ucaddon_audio_player_mejs .ue-audio-player__element--audio-description {
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-ucaddon_audio_player_mejs .mejs__time {
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-ucaddon_audio_player_mejs .ue_btn {
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-ha-cf7 .wpcf7-form-control:not(.wpcf7-submit) {
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-ha-cf7 label {
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-ha-cf7 .wpcf7-submit {
        font-family: var(--e-global-typography-accent-font-family), Sans-serif;
        font-weight: var(--e-global-typography-accent-font-weight);
      }

      .elementor-widget-ha-image-grid .ha-filter__item {
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-ha-slider .ha-slick-title {
        font-family: var(--e-global-typography-secondary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-secondary-font-weight);
      }

      .elementor-widget-ha-slider .ha-slick-subtitle {
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-ha-social-icons .ha-social-icon-label {
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-ha-horizontal-timeline .ha-horizontal-timeline-date {
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-ha-horizontal-timeline .ha-horizontal-timeline-title {
        font-family: var(--e-global-typography-secondary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-secondary-font-weight);
      }

      .elementor-widget-ha-horizontal-timeline .ha-horizontal-timeline-subtitle {
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-ha-horizontal-timeline .ha-horizontal-timeline-description {
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-pp-advanced-accordion .pp-accordion-item {
        border-color: var(--e-global-color-text);
      }

      .elementor-widget-pp-advanced-accordion .pp-advanced-accordion .pp-accordion-tab-title {
        color: var(--e-global-color-primary);
        font-family: var(--e-global-typography-primary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-primary-font-weight);
      }

      .elementor-widget-pp-advanced-accordion .pp-advanced-accordion .pp-accordion-tab-title svg {
        fill: var(--e-global-color-primary);
      }

      .elementor-widget-pp-advanced-accordion .pp-advanced-accordion .pp-accordion-tab-title.pp-accordion-tab-active {
        color: var(--e-global-color-accent);
      }

      .elementor-widget-pp-advanced-accordion .pp-advanced-accordion .pp-accordion-tab-title.pp-accordion-tab-active svg {
        fill: var(--e-global-color-accent);
      }

      .elementor-widget-pp-advanced-accordion .pp-advanced-accordion .pp-accordion-item .pp-accordion-tab-content {
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-pp-advanced-accordion .pp-advanced-accordion .pp-accordion-tab-title .pp-accordion-toggle-icon {
        color: var(--e-global-color-primary);
      }

      .elementor-widget-pp-advanced-accordion .pp-advanced-accordion .pp-accordion-tab-title .pp-accordion-toggle-icon svg {
        fill: var(--e-global-color-primary);
      }

      .elementor-widget-pp-advanced-accordion .pp-advanced-accordion .pp-accordion-tab-title.pp-accordion-tab-active .pp-accordion-toggle-icon {
        color: var(--e-global-color-accent);
      }

      .elementor-widget-pp-advanced-accordion .pp-advanced-accordion .pp-accordion-tab-title.pp-accordion-tab-active .pp-accordion-toggle-icon svg {
        fill: var(--e-global-color-accent);
      }

      .pp-modal-popup-window- {
          {
          ID
        }
      }

      .pp-popup-header .pp-popup-title {
        font-family: var(--e-global-typography-primary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-primary-font-weight);
      }

      .pp-modal-popup-window- {
          {
          ID
        }
      }

      .pp-popup-content {
        font-family: var(--e-global-typography-accent-font-family), Sans-serif;
        font-weight: var(--e-global-typography-accent-font-weight);
      }

      .elementor-widget-pp-modal-popup .pp-modal-popup-button {
        background-color: var(--e-global-color-accent);
        font-family: var(--e-global-typography-accent-font-family), Sans-serif;
        font-weight: var(--e-global-typography-accent-font-weight);
      }

      .elementor-widget-pp-one-page-nav .pp-nav-dot-tooltip {
        font-family: var(--e-global-typography-accent-font-family), Sans-serif;
        font-weight: var(--e-global-typography-accent-font-weight);
      }

      .elementor-widget-pp-toggle .pp-primary-toggle-label {
        font-family: var(--e-global-typography-accent-font-family), Sans-serif;
        font-weight: var(--e-global-typography-accent-font-weight);
      }

      .elementor-widget-pp-toggle .pp-secondary-toggle-label {
        font-family: var(--e-global-typography-accent-font-family), Sans-serif;
        font-weight: var(--e-global-typography-accent-font-weight);
      }

      .elementor-widget-pp-toggle .pp-toggle-content-wrap {
        font-family: var(--e-global-typography-accent-font-family), Sans-serif;
        font-weight: var(--e-global-typography-accent-font-weight);
      }

      .elementor-widget-text-path {
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-theme-site-logo .widget-image-caption {
        color: var(--e-global-color-text);
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-theme-site-title .elementor-heading-title {
        color: var(--e-global-color-primary);
        font-family: var(--e-global-typography-primary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-primary-font-weight);
      }

      .elementor-widget-theme-page-title .elementor-heading-title {
        color: var(--e-global-color-primary);
        font-family: var(--e-global-typography-primary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-primary-font-weight);
      }

      .elementor-widget-theme-post-title .elementor-heading-title {
        color: var(--e-global-color-primary);
        font-family: var(--e-global-typography-primary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-primary-font-weight);
      }

      .elementor-widget-theme-post-excerpt .elementor-widget-container {
        color: var(--e-global-color-text);
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-theme-post-content {
        color: var(--e-global-color-text);
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-theme-post-featured-image .widget-image-caption {
        color: var(--e-global-color-text);
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-theme-archive-title .elementor-heading-title {
        color: var(--e-global-color-primary);
        font-family: var(--e-global-typography-primary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-primary-font-weight);
      }

      .elementor-widget-archive-posts .elementor-post__title,
      .elementor-widget-archive-posts .elementor-post__title a {
        color: var(--e-global-color-secondary);
        font-family: var(--e-global-typography-primary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-primary-font-weight);
      }

      .elementor-widget-archive-posts .elementor-post__meta-data {
        font-family: var(--e-global-typography-secondary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-secondary-font-weight);
      }

      .elementor-widget-archive-posts .elementor-post__excerpt p {
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-archive-posts .elementor-post__read-more {
        color: var(--e-global-color-accent);
        font-family: var(--e-global-typography-accent-font-family), Sans-serif;
        font-weight: var(--e-global-typography-accent-font-weight);
      }

      .elementor-widget-archive-posts .elementor-post__card .elementor-post__badge {
        background-color: var(--e-global-color-accent);
        font-family: var(--e-global-typography-accent-font-family), Sans-serif;
        font-weight: var(--e-global-typography-accent-font-weight);
      }

      .elementor-widget-archive-posts .elementor-pagination {
        font-family: var(--e-global-typography-secondary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-secondary-font-weight);
      }

      .elementor-widget-archive-posts .elementor-button {
        font-family: var(--e-global-typography-accent-font-family), Sans-serif;
        font-weight: var(--e-global-typography-accent-font-weight);
        background-color: var(--e-global-color-accent);
      }

      .elementor-widget-archive-posts .e-load-more-message {
        font-family: var(--e-global-typography-secondary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-secondary-font-weight);
      }

      .elementor-widget-archive-posts .elementor-posts-nothing-found {
        color: var(--e-global-color-text);
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-posts .elementor-post__title,
      .elementor-widget-posts .elementor-post__title a {
        color: var(--e-global-color-secondary);
        font-family: var(--e-global-typography-primary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-primary-font-weight);
      }

      .elementor-widget-posts .elementor-post__meta-data {
        font-family: var(--e-global-typography-secondary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-secondary-font-weight);
      }

      .elementor-widget-posts .elementor-post__excerpt p {
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-posts .elementor-post__read-more {
        color: var(--e-global-color-accent);
        font-family: var(--e-global-typography-accent-font-family), Sans-serif;
        font-weight: var(--e-global-typography-accent-font-weight);
      }

      .elementor-widget-posts .elementor-post__card .elementor-post__badge {
        background-color: var(--e-global-color-accent);
        font-family: var(--e-global-typography-accent-font-family), Sans-serif;
        font-weight: var(--e-global-typography-accent-font-weight);
      }

      .elementor-widget-posts .elementor-pagination {
        font-family: var(--e-global-typography-secondary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-secondary-font-weight);
      }

      .elementor-widget-posts .elementor-button {
        font-family: var(--e-global-typography-accent-font-family), Sans-serif;
        font-weight: var(--e-global-typography-accent-font-weight);
        background-color: var(--e-global-color-accent);
      }

      .elementor-widget-posts .e-load-more-message {
        font-family: var(--e-global-typography-secondary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-secondary-font-weight);
      }

      .elementor-widget-portfolio a .elementor-portfolio-item__overlay {
        background-color: var(--e-global-color-accent);
      }

      .elementor-widget-portfolio .elementor-portfolio-item__title {
        font-family: var(--e-global-typography-primary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-primary-font-weight);
      }

      .elementor-widget-portfolio .elementor-portfolio__filter {
        color: var(--e-global-color-text);
        font-family: var(--e-global-typography-primary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-primary-font-weight);
      }

      .elementor-widget-portfolio .elementor-portfolio__filter.elementor-active {
        color: var(--e-global-color-primary);
      }

      .elementor-widget-gallery .elementor-gallery-item__title {
        font-family: var(--e-global-typography-primary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-primary-font-weight);
      }

      .elementor-widget-gallery .elementor-gallery-item__description {
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-gallery {
        --galleries-title-color-normal: var(--e-global-color-primary);
        --galleries-title-color-hover: var(--e-global-color-secondary);
        --galleries-pointer-bg-color-hover: var(--e-global-color-accent);
        --gallery-title-color-active: var(--e-global-color-secondary);
        --galleries-pointer-bg-color-active: var(--e-global-color-accent);
      }

      .elementor-widget-gallery .elementor-gallery-title {
        font-family: var(--e-global-typography-primary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-primary-font-weight);
      }

      .elementor-widget-form .elementor-field-group>label,
      .elementor-widget-form .elementor-field-subgroup label {
        color: var(--e-global-color-text);
      }

      .elementor-widget-form .elementor-field-group>label {
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-form .elementor-field-type-html {
        color: var(--e-global-color-text);
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-form .elementor-field-group .elementor-field {
        color: var(--e-global-color-text);
      }

      .elementor-widget-form .select2-container--default .select2-selection--single .select2-selection__rendered {
        color: var(--e-global-color-text);
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-form ..select2-container--default .select2-selection--multiple .select2-selection__rendered {
        color: var(--e-global-color-text);
      }

      .elementor-widget-form .elementor-field-group .elementor-field,
      .elementor-widget-form .elementor-field-subgroup label {
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-form .select2-container--default .select2-selection--single,
      .elementor-widget-form .select2-container--default .select2-selection--multiple {
        height: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-form .elementor-button {
        font-family: var(--e-global-typography-accent-font-family), Sans-serif;
        font-weight: var(--e-global-typography-accent-font-weight);
      }

      .elementor-widget-form .e-form__buttons__wrapper__button-next {
        background-color: var(--e-global-color-accent);
      }

      .elementor-widget-form .elementor-button[type="submit"] {
        background-color: var(--e-global-color-accent);
      }

      .elementor-widget-form .e-form__buttons__wrapper__button-previous {
        background-color: var(--e-global-color-accent);
      }

      .elementor-widget-form .elementor-message {
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-form .e-form__indicators__indicator,
      .elementor-widget-form .e-form__indicators__indicator__label {
        font-family: var(--e-global-typography-accent-font-family), Sans-serif;
        font-weight: var(--e-global-typography-accent-font-weight);
      }

      .elementor-widget-form {
        --e-form-steps-indicator-inactive-primary-color: var(--e-global-color-text);
        --e-form-steps-indicator-active-primary-color: var(--e-global-color-accent);
        --e-form-steps-indicator-completed-primary-color: var(--e-global-color-accent);
        --e-form-steps-indicator-progress-color: var(--e-global-color-accent);
        --e-form-steps-indicator-progress-background-color: var(--e-global-color-text);
        --e-form-steps-indicator-progress-meter-color: var(--e-global-color-text);
      }

      .elementor-widget-form .e-form__indicators__indicator__progress__meter {
        font-family: var(--e-global-typography-accent-font-family), Sans-serif;
        font-weight: var(--e-global-typography-accent-font-weight);
      }

      .elementor-widget-login .elementor-field-group>a {
        color: var(--e-global-color-text);
      }

      .elementor-widget-login .elementor-field-group>a:hover {
        color: var(--e-global-color-accent);
      }

      .elementor-widget-login .elementor-form-fields-wrapper label {
        color: var(--e-global-color-text);
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-login .elementor-field-group .elementor-field {
        color: var(--e-global-color-text);
      }

      .elementor-widget-login .elementor-field-group .elementor-field,
      .elementor-widget-login .elementor-field-subgroup label {
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-login .elementor-button {
        font-family: var(--e-global-typography-accent-font-family), Sans-serif;
        font-weight: var(--e-global-typography-accent-font-weight);
        background-color: var(--e-global-color-accent);
      }

      .elementor-widget-login .elementor-widget-container .elementor-login__logged-in-message {
        color: var(--e-global-color-text);
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-slides .elementor-slide-heading {
        font-family: var(--e-global-typography-primary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-primary-font-weight);
      }

      .elementor-widget-slides .elementor-slide-description {
        font-family: var(--e-global-typography-secondary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-secondary-font-weight);
      }

      .elementor-widget-slides .elementor-slide-button {
        font-family: var(--e-global-typography-accent-font-family), Sans-serif;
        font-weight: var(--e-global-typography-accent-font-weight);
      }

      .elementor-widget-nav-menu .elementor-nav-menu .elementor-item {
        font-family: var(--e-global-typography-primary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-primary-font-weight);
      }

      .elementor-widget-nav-menu .elementor-nav-menu--main .elementor-item {
        color: var(--e-global-color-text);
        fill: var(--e-global-color-text);
      }

      .elementor-widget-nav-menu .elementor-nav-menu--main .elementor-item:hover,
      .elementor-widget-nav-menu .elementor-nav-menu--main .elementor-item.elementor-item-active,
      .elementor-widget-nav-menu .elementor-nav-menu--main .elementor-item.highlighted,
      .elementor-widget-nav-menu .elementor-nav-menu--main .elementor-item:focus {
        color: var(--e-global-color-accent);
        fill: var(--e-global-color-accent);
      }

      .elementor-widget-nav-menu .elementor-nav-menu--main:not(.e--pointer-framed) .elementor-item:before,
      .elementor-widget-nav-menu .elementor-nav-menu--main:not(.e--pointer-framed) .elementor-item:after {
        background-color: var(--e-global-color-accent);
      }

      .elementor-widget-nav-menu .e--pointer-framed .elementor-item:before,
      .elementor-widget-nav-menu .e--pointer-framed .elementor-item:after {
        border-color: var(--e-global-color-accent);
      }

      .elementor-widget-nav-menu .elementor-nav-menu--dropdown .elementor-item,
      .elementor-widget-nav-menu .elementor-nav-menu--dropdown .elementor-sub-item {
        font-family: var(--e-global-typography-accent-font-family), Sans-serif;
        font-weight: var(--e-global-typography-accent-font-weight);
      }

      .elementor-widget-animated-headline .elementor-headline-dynamic-wrapper path {
        stroke: var(--e-global-color-accent);
      }

      .elementor-widget-animated-headline .elementor-headline-plain-text {
        color: var(--e-global-color-secondary);
      }

      .elementor-widget-animated-headline .elementor-headline {
        font-family: var(--e-global-typography-primary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-primary-font-weight);
      }

      .elementor-widget-animated-headline {
        --dynamic-text-color: var(--e-global-color-secondary);
      }

      .elementor-widget-animated-headline .elementor-headline-dynamic-text {
        font-family: var(--e-global-typography-primary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-primary-font-weight);
      }

      .elementor-widget-hotspot .widget-image-caption {
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-hotspot {
        --hotspot-color: var(--e-global-color-primary);
        --hotspot-box-color: var(--e-global-color-secondary);
        --tooltip-color: var(--e-global-color-secondary);
      }

      .elementor-widget-hotspot .e-hotspot__label {
        font-family: var(--e-global-typography-primary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-primary-font-weight);
      }

      .elementor-widget-hotspot .e-hotspot__tooltip {
        font-family: var(--e-global-typography-secondary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-secondary-font-weight);
      }

      .elementor-widget-price-list .elementor-price-list-header {
        color: var(--e-global-color-primary);
        font-family: var(--e-global-typography-primary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-primary-font-weight);
      }

      .elementor-widget-price-list .elementor-price-list-description {
        color: var(--e-global-color-text);
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-price-list .elementor-price-list-separator {
        border-bottom-color: var(--e-global-color-secondary);
      }

      .elementor-widget-price-table {
        --e-price-table-header-background-color: var(--e-global-color-secondary);
      }

      .elementor-widget-price-table .elementor-price-table__heading {
        font-family: var(--e-global-typography-primary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-primary-font-weight);
      }

      .elementor-widget-price-table .elementor-price-table__subheading {
        font-family: var(--e-global-typography-secondary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-secondary-font-weight);
      }

      .elementor-widget-price-table .elementor-price-table .elementor-price-table__price {
        font-family: var(--e-global-typography-primary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-primary-font-weight);
      }

      .elementor-widget-price-table .elementor-price-table__original-price {
        color: var(--e-global-color-secondary);
        font-family: var(--e-global-typography-primary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-primary-font-weight);
      }

      .elementor-widget-price-table .elementor-price-table__period {
        color: var(--e-global-color-secondary);
        font-family: var(--e-global-typography-secondary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-secondary-font-weight);
      }

      .elementor-widget-price-table .elementor-price-table__features-list {
        --e-price-table-features-list-color: var(--e-global-color-text);
      }

      .elementor-widget-price-table .elementor-price-table__features-list li {
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-price-table .elementor-price-table__features-list li:before {
        border-top-color: var(--e-global-color-text);
      }

      .elementor-widget-price-table .elementor-price-table__button {
        font-family: var(--e-global-typography-accent-font-family), Sans-serif;
        font-weight: var(--e-global-typography-accent-font-weight);
        background-color: var(--e-global-color-accent);
      }

      .elementor-widget-price-table .elementor-price-table__additional_info {
        color: var(--e-global-color-text);
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-price-table .elementor-price-table__ribbon-inner {
        background-color: var(--e-global-color-accent);
        font-family: var(--e-global-typography-accent-font-family), Sans-serif;
        font-weight: var(--e-global-typography-accent-font-weight);
      }

      .elementor-widget-flip-box .elementor-flip-box__front .elementor-flip-box__layer__title {
        font-family: var(--e-global-typography-primary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-primary-font-weight);
      }

      .elementor-widget-flip-box .elementor-flip-box__front .elementor-flip-box__layer__description {
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-flip-box .elementor-flip-box__back .elementor-flip-box__layer__title {
        font-family: var(--e-global-typography-primary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-primary-font-weight);
      }

      .elementor-widget-flip-box .elementor-flip-box__back .elementor-flip-box__layer__description {
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-flip-box .elementor-flip-box__button {
        font-family: var(--e-global-typography-accent-font-family), Sans-serif;
        font-weight: var(--e-global-typography-accent-font-weight);
      }

      .elementor-widget-call-to-action .elementor-cta__title {
        font-family: var(--e-global-typography-primary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-primary-font-weight);
      }

      .elementor-widget-call-to-action .elementor-cta__description {
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-call-to-action .elementor-cta__button {
        font-family: var(--e-global-typography-accent-font-family), Sans-serif;
        font-weight: var(--e-global-typography-accent-font-weight);
      }

      .elementor-widget-call-to-action .elementor-ribbon-inner {
        background-color: var(--e-global-color-accent);
        font-family: var(--e-global-typography-accent-font-family), Sans-serif;
        font-weight: var(--e-global-typography-accent-font-weight);
      }

      .elementor-widget-media-carousel .elementor-carousel-image-overlay {
        font-family: var(--e-global-typography-accent-font-family), Sans-serif;
        font-weight: var(--e-global-typography-accent-font-weight);
      }

      .elementor-widget-testimonial-carousel .elementor-testimonial__text {
        color: var(--e-global-color-text);
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-testimonial-carousel .elementor-testimonial__name {
        color: var(--e-global-color-text);
        font-family: var(--e-global-typography-primary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-primary-font-weight);
      }

      .elementor-widget-testimonial-carousel .elementor-testimonial__title {
        color: var(--e-global-color-primary);
        font-family: var(--e-global-typography-secondary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-secondary-font-weight);
      }

      .elementor-widget-reviews .elementor-testimonial__header,
      .elementor-widget-reviews .elementor-testimonial__name {
        font-family: var(--e-global-typography-primary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-primary-font-weight);
      }

      .elementor-widget-reviews .elementor-testimonial__text {
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-table-of-contents {
        --header-color: var(--e-global-color-secondary);
        --item-text-color: var(--e-global-color-text);
        --item-text-hover-color: var(--e-global-color-accent);
        --marker-color: var(--e-global-color-text);
      }

      .elementor-widget-table-of-contents .elementor-toc__header,
      .elementor-widget-table-of-contents .elementor-toc__header-title {
        font-family: var(--e-global-typography-primary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-primary-font-weight);
      }

      .elementor-widget-table-of-contents .elementor-toc__list-item {
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-countdown .elementor-countdown-item {
        background-color: var(--e-global-color-primary);
      }

      .elementor-widget-countdown .elementor-countdown-digits {
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-countdown .elementor-countdown-label {
        font-family: var(--e-global-typography-secondary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-secondary-font-weight);
      }

      .elementor-widget-countdown .elementor-countdown-expire--message {
        color: var(--e-global-color-text);
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-search-form input[type="search"].elementor-search-form__input {
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-search-form .elementor-search-form__input,
      .elementor-widget-search-form .elementor-search-form__icon,
      .elementor-widget-search-form .elementor-lightbox .dialog-lightbox-close-button,
      .elementor-widget-search-form .elementor-lightbox .dialog-lightbox-close-button:hover,
      .elementor-widget-search-form.elementor-search-form--skin-full_screen input[type="search"].elementor-search-form__input {
        color: var(--e-global-color-text);
        fill: var(--e-global-color-text);
      }

      .elementor-widget-search-form .elementor-search-form__submit {
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
        background-color: var(--e-global-color-secondary);
      }

      .elementor-widget-author-box .elementor-author-box__name {
        color: var(--e-global-color-secondary);
        font-family: var(--e-global-typography-primary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-primary-font-weight);
      }

      .elementor-widget-author-box .elementor-author-box__bio {
        color: var(--e-global-color-text);
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-author-box .elementor-author-box__button {
        color: var(--e-global-color-secondary);
        border-color: var(--e-global-color-secondary);
        font-family: var(--e-global-typography-accent-font-family), Sans-serif;
        font-weight: var(--e-global-typography-accent-font-weight);
      }

      .elementor-widget-author-box .elementor-author-box__button:hover {
        border-color: var(--e-global-color-secondary);
        color: var(--e-global-color-secondary);
      }

      .elementor-widget-post-navigation span.post-navigation__prev--label {
        color: var(--e-global-color-text);
      }

      .elementor-widget-post-navigation span.post-navigation__next--label {
        color: var(--e-global-color-text);
      }

      .elementor-widget-post-navigation span.post-navigation__prev--label,
      .elementor-widget-post-navigation span.post-navigation__next--label {
        font-family: var(--e-global-typography-secondary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-secondary-font-weight);
      }

      .elementor-widget-post-navigation span.post-navigation__prev--title,
      .elementor-widget-post-navigation span.post-navigation__next--title {
        color: var(--e-global-color-secondary);
        font-family: var(--e-global-typography-secondary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-secondary-font-weight);
      }

      .elementor-widget-post-info .elementor-icon-list-item:not(:last-child):after {
        border-color: var(--e-global-color-text);
      }

      .elementor-widget-post-info .elementor-icon-list-icon i {
        color: var(--e-global-color-primary);
      }

      .elementor-widget-post-info .elementor-icon-list-icon svg {
        fill: var(--e-global-color-primary);
      }

      .elementor-widget-post-info .elementor-icon-list-text,
      .elementor-widget-post-info .elementor-icon-list-text a {
        color: var(--e-global-color-secondary);
      }

      .elementor-widget-post-info .elementor-icon-list-item {
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-sitemap .elementor-sitemap-title {
        color: var(--e-global-color-primary);
        font-family: var(--e-global-typography-primary-font-family), Sans-serif;
        font-weight: var(--e-global-typography-primary-font-weight);
      }

      .elementor-widget-sitemap .elementor-sitemap-item,
      .elementor-widget-sitemap span.elementor-sitemap-list,
      .elementor-widget-sitemap .elementor-sitemap-item a {
        color: var(--e-global-color-text);
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-sitemap .elementor-sitemap-item {
        color: var(--e-global-color-text);
      }

      .elementor-widget-blockquote .elementor-blockquote__content {
        color: var(--e-global-color-text);
      }

      .elementor-widget-blockquote .elementor-blockquote__author {
        color: var(--e-global-color-secondary);
      }

      .elementor-widget-lottie {
        --caption-color: var(--e-global-color-text);
      }

      .elementor-widget-lottie .e-lottie__caption {
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-video-playlist .e-tabs-header .e-tabs-title {
        color: var(--e-global-color-text);
      }

      .elementor-widget-video-playlist .e-tabs-header .e-tabs-videos-count {
        color: var(--e-global-color-text);
      }

      .elementor-widget-video-playlist .e-tabs-header .e-tabs-header-right-side i {
        color: var(--e-global-color-text);
      }

      .elementor-widget-video-playlist .e-tabs-header .e-tabs-header-right-side svg {
        fill: var(--e-global-color-text);
      }

      .elementor-widget-video-playlist .e-tab-title .e-tab-title-text {
        color: var(--e-global-color-text);
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-video-playlist .e-tab-title .e-tab-title-text a {
        color: var(--e-global-color-text);
      }

      .elementor-widget-video-playlist .e-tab-title .e-tab-duration {
        color: var(--e-global-color-text);
      }

      .elementor-widget-video-playlist .e-tabs-items-wrapper .e-tab-title:where(.e-active, :hover) .e-tab-title-text {
        color: var(--e-global-color-text);
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-video-playlist .e-tabs-items-wrapper .e-tab-title:where(.e-active, :hover) .e-tab-title-text a {
        color: var(--e-global-color-text);
      }

      .elementor-widget-video-playlist .e-tabs-items-wrapper .e-tab-title:where(.e-active, :hover) .e-tab-duration {
        color: var(--e-global-color-text);
      }

      .elementor-widget-video-playlist .e-tabs-items-wrapper .e-section-title {
        color: var(--e-global-color-text);
      }

      .elementor-widget-video-playlist .e-tabs-inner-tabs .e-inner-tabs-wrapper .e-inner-tab-title a {
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-video-playlist .e-tabs-inner-tabs .e-inner-tabs-content-wrapper .e-inner-tab-content .e-inner-tab-text {
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-widget-video-playlist .e-tabs-inner-tabs .e-inner-tabs-content-wrapper .e-inner-tab-content button {
        color: var(--e-global-color-text);
        font-family: var(--e-global-typography-accent-font-family), Sans-serif;
        font-weight: var(--e-global-typography-accent-font-weight);
      }

      .elementor-widget-video-playlist .e-tabs-inner-tabs .e-inner-tabs-content-wrapper .e-inner-tab-content button:hover {
        color: var(--e-global-color-text);
      }

      .elementor-widget-paypal-button .elementor-button {
        font-family: var(--e-global-typography-accent-font-family), Sans-serif;
        font-weight: var(--e-global-typography-accent-font-weight);
        background-color: var(--e-global-color-accent);
      }

      .elementor-widget-paypal-button .elementor-message {
        font-family: var(--e-global-typography-text-font-family), Sans-serif;
        font-weight: var(--e-global-typography-text-font-weight);
      }

      .elementor-128245 .elementor-element.elementor-element-513f728>.elementor-container {
        max-width: 415px;
      }

      .elementor-128245 .elementor-element.elementor-element-513f728 {
        overflow: hidden;
        transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
        z-index: 1000;
      }

      .elementor-128245 .elementor-element.elementor-element-513f728 .elementor-background-slideshow__slide__image {
        background-size: cover;
        background-position: center center;
      }

      .elementor-128245 .elementor-element.elementor-element-513f728>.elementor-background-overlay {
        background-color: #00000000;
        opacity: 0;
        transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
      }

      .elementor-bc-flex-widget .elementor-128245 .elementor-element.elementor-element-9da6ff9.elementor-column .elementor-column-wrap {
        align-items: flex-end;
      }

      .elementor-128245 .elementor-element.elementor-element-9da6ff9.elementor-column.elementor-element[data-element_type="column"]>.elementor-column-wrap.elementor-element-populated>.elementor-widget-wrap {
        align-content: flex-end;
        align-items: flex-end;
      }

      .elementor-128245 .elementor-element.elementor-element-9da6ff9.elementor-column>.elementor-column-wrap>.elementor-widget-wrap {
        justify-content: center;
      }

      .elementor-128245 .elementor-element.elementor-element-9da6ff9>.elementor-column-wrap>.elementor-widget-wrap>.elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
        margin-bottom: 10px;
      }

      .elementor-128245 .elementor-element.elementor-element-9da6ff9>.elementor-element-populated>.elementor-background-overlay {
        background-color: transparent;
        background-image: linear-gradient(180deg, #00000000 55%, #000000 100%);
        opacity: 1;
      }

      .elementor-128245 .elementor-element.elementor-element-9da6ff9>.elementor-element-populated {
        transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
        margin: 0px 0px 0px 0px;
        --e-column-margin-right: 0px;
        --e-column-margin-left: 0px;
      }

      .elementor-128245 .elementor-element.elementor-element-9da6ff9>.elementor-element-populated>.elementor-background-overlay {
        transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
      }

      .elementor-128245 .elementor-element.elementor-element-9da6ff9>.elementor-element-populated>.elementor-widget-wrap {
        padding: 0em 0em 3em 0em;
      }

      .elementor-128245 .elementor-element.elementor-element-ed84c9e {
        margin-top: 0em;
        margin-bottom: 1.5em;
      }

      .elementor-128245 .elementor-element.elementor-element-e84aeaa.elementor-column>.elementor-column-wrap>.elementor-widget-wrap {
        justify-content: center;
      }

      .elementor-128245 .elementor-element.elementor-element-e84aeaa>.elementor-element-populated>.elementor-widget-wrap {
        padding: 0px 0px 0px 0px;
      }

      .elementor-128245 .elementor-element.elementor-element-6cafba5 {
        --alignment: center;
        --width: 150px;
        --rotate: 0deg;
        font-family: "Poppins", Sans-serif;
        font-size: 25px;
        font-weight: 400;
        letter-spacing: 3px;
        --text-color: #FFFFFF;
        --transition: 0.3s;
        width: auto;
        max-width: auto;
      }

      .elementor-128245 .elementor-element.elementor-element-6cafba5>.elementor-widget-container {
        margin: 0em 0em -4.5em 0em;
      }

      .elementor-128245 .elementor-element.elementor-element-2a8b858 {
        text-align: center;
        z-index: 5;
      }

      .elementor-128245 .elementor-element.elementor-element-2a8b858 .elementor-heading-title {
        color: #FFFFFF;
        font-family: "Analogue", Sans-serif;
        font-size: 42px;
        font-weight: normal;
      }

      .elementor-128245 .elementor-element.elementor-element-2a8b858>.elementor-widget-container {
        margin: 0em 0em 0em 0em;
        padding: 0px 0px 0px 0px;
      }

      .elementor-128245 .elementor-element.elementor-element-7d4475a {
        text-align: center;
        z-index: 5;
      }

      .elementor-128245 .elementor-element.elementor-element-7d4475a .elementor-heading-title {
        color: #FFFFFF;
        font-family: "Poppins", Sans-serif;
        font-size: 16px;
        font-weight: normal;
        letter-spacing: 2px;
      }

      .elementor-128245 .elementor-element.elementor-element-7d4475a>.elementor-widget-container {
        margin: 0.5em 0em 0em 0em;
      }

      .elementor-128245 .elementor-element.elementor-element-dcfcf52 {
        text-align: center;
        z-index: 5;
      }

      .elementor-128245 .elementor-element.elementor-element-dcfcf52 .elementor-heading-title {
        color: #FFFFFF;
        font-family: "Poppins", Sans-serif;
        font-size: 20px;
        font-weight: 500;
        letter-spacing: 0px;
      }

      .elementor-128245 .elementor-element.elementor-element-dcfcf52>.elementor-widget-container {
        margin: 0em 0em 0em 0em;
      }

      .elementor-128245 .elementor-element.elementor-element-447df93 .elementor-button {
        font-family: "Poppins", Sans-serif;
        font-size: 15px;
        font-weight: normal;
        fill: #FFFFFF;
        color: #FFFFFF;
        background-color: #000000;
        border-radius: 0px 0px 0px 0px;
        padding: 12px 15px 12px 15px;
      }

      .elementor-128245 .elementor-element.elementor-element-447df93>.elementor-widget-container {
        margin: 1.5em 0em 0em 0em;
        padding: 0px 0px 0px 0px;
      }

      .elementor-128245 .elementor-element.elementor-element-447df93 {
        z-index: 9999;
        width: auto;
        max-width: auto;
      }

      .elementor-128245 .elementor-element.elementor-element-34a8077>.elementor-container {
        max-width: 415px;
      }

      .elementor-128245 .elementor-element.elementor-element-34a8077 {
        overflow: hidden;
        transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
      }

      .elementor-128245 .elementor-element.elementor-element-34a8077:not(.elementor-motion-effects-element-type-background),
      .elementor-128245 .elementor-element.elementor-element-34a8077>.elementor-motion-effects-container>.elementor-motion-effects-layer {
        background-color: #F8F8F8;
      }

      .elementor-128245 .elementor-element.elementor-element-34a8077>.elementor-background-overlay {
        opacity: 0.5;
        transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
      }

      .elementor-128245 .elementor-element.elementor-element-b5d8cb8>.elementor-column-wrap>.elementor-widget-wrap>.elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
        margin-bottom: 0px;
      }

      .elementor-128245 .elementor-element.elementor-element-b5d8cb8>.elementor-element-populated>.elementor-widget-wrap {
        padding: 0px 0px 0px 0px;
      }

      .elementor-128245 .elementor-element.elementor-element-4a0a9f7>.elementor-container {
        min-height: 100vh;
      }

      .elementor-128245 .elementor-element.elementor-element-4a0a9f7 {
        overflow: hidden;
        transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
      }

      .elementor-128245 .elementor-element.elementor-element-4a0a9f7 .elementor-background-slideshow__slide__image {
        background-size: cover;
        background-position: center center;
      }

      .elementor-128245 .elementor-element.elementor-element-4a0a9f7>.elementor-background-overlay {
        background-color: #000000;
        opacity: 0.2;
        transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
      }

      .elementor-bc-flex-widget .elementor-128245 .elementor-element.elementor-element-3967eea.elementor-column .elementor-column-wrap {
        align-items: flex-end;
      }

      .elementor-128245 .elementor-element.elementor-element-3967eea.elementor-column.elementor-element[data-element_type="column"]>.elementor-column-wrap.elementor-element-populated>.elementor-widget-wrap {
        align-content: flex-end;
        align-items: flex-end;
      }

      .elementor-128245 .elementor-element.elementor-element-3967eea.elementor-column>.elementor-column-wrap>.elementor-widget-wrap {
        justify-content: center;
      }

      .elementor-128245 .elementor-element.elementor-element-3967eea>.elementor-column-wrap>.elementor-widget-wrap>.elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
        margin-bottom: 0px;
      }

      .elementor-128245 .elementor-element.elementor-element-3967eea>.elementor-element-populated>.elementor-background-overlay {
        background-color: transparent;
        background-image: linear-gradient(180deg, #00000000 50%, #000000 100%);
        opacity: 0.65;
      }

      .elementor-128245 .elementor-element.elementor-element-3967eea>.elementor-element-populated {
        transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
        margin: 0px 0px 0px 0px;
        --e-column-margin-right: 0px;
        --e-column-margin-left: 0px;
      }

      .elementor-128245 .elementor-element.elementor-element-3967eea>.elementor-element-populated>.elementor-background-overlay {
        transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
      }

      .elementor-128245 .elementor-element.elementor-element-3967eea>.elementor-element-populated>.elementor-widget-wrap {
        padding: 2em 0em 1em 0em;
      }

      .elementor-bc-flex-widget .elementor-128245 .elementor-element.elementor-element-8104037.elementor-column .elementor-column-wrap {
        align-items: center;
      }

      .elementor-128245 .elementor-element.elementor-element-8104037.elementor-column.elementor-element[data-element_type="column"]>.elementor-column-wrap.elementor-element-populated>.elementor-widget-wrap {
        align-content: center;
        align-items: center;
      }

      .elementor-128245 .elementor-element.elementor-element-8104037.elementor-column>.elementor-column-wrap>.elementor-widget-wrap {
        justify-content: center;
      }

      .elementor-128245 .elementor-element.elementor-element-8104037>.elementor-column-wrap>.elementor-widget-wrap>.elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
        margin-bottom: 0px;
      }

      .elementor-128245 .elementor-element.elementor-element-8104037>.elementor-element-populated>.elementor-widget-wrap {
        padding: 0px 0px 0px 0px;
      }

      .elementor-128245 .elementor-element.elementor-element-f73ce84 {
        text-align: center;
        width: auto;
        max-width: auto;
      }

      .elementor-128245 .elementor-element.elementor-element-f73ce84 .elementor-heading-title {
        color: #FFFFFF;
        font-family: "Poppins", Sans-serif;
        font-size: 16px;
        font-weight: normal;
        letter-spacing: 2px;
      }

      .elementor-128245 .elementor-element.elementor-element-6ad7d31 {
        text-align: center;
        z-index: 5;
      }

      .elementor-128245 .elementor-element.elementor-element-6ad7d31 .elementor-heading-title {
        color: #FFFFFF;
        font-family: "Analogue", Sans-serif;
        font-size: 40px;
        font-weight: normal;
        text-transform: uppercase;
        letter-spacing: 1px;
      }

      .elementor-128245 .elementor-element.elementor-element-6ad7d31>.elementor-widget-container {
        margin: 1em 0em 2em 0em;
        padding: 0px 0px 0px 0px;
      }

      .elementor-128245 .elementor-element.elementor-element-17e43e4 {
        text-align: center;
        z-index: 999;
        width: auto;
        max-width: auto;
      }

      .elementor-128245 .elementor-element.elementor-element-17e43e4 .elementor-heading-title {
        color: #FFFFFF;
        font-family: "Poppins", Sans-serif;
        font-size: 11px;
        font-weight: bold;
        letter-spacing: 4px;
      }

      .elementor-128245 .elementor-element.elementor-element-17e43e4>.elementor-widget-container {
        margin: 0em 0em 0em 0em;
        padding: 0px 0px 0px 0px;
        border-radius: 0px 0px 0px 0px;
      }

      .elementor-128245 .elementor-element.elementor-element-17e43e4.ha-has-bg-overlay>.elementor-widget-container:before {
        transition: background 0.3s;
      }

      .elementor-128245 .elementor-element.elementor-element-6664a5c {
        text-align: center;
        width: auto;
        max-width: auto;
      }

      .elementor-128245 .elementor-element.elementor-element-6664a5c .elementor-heading-title {
        color: #FFFFFF;
        font-family: "Poppins", Sans-serif;
        font-size: 11px;
        font-weight: normal;
        letter-spacing: 4px;
      }

      .elementor-128245 .elementor-element.elementor-element-6664a5c>.elementor-widget-container {
        margin: 0em 0em 0em 0.6em;
      }

      .elementor-128245 .elementor-element.elementor-element-d3cf127 .elementor-countdown-wrapper {
        max-width: 80%;
      }

      .elementor-128245 .elementor-element.elementor-element-d3cf127 .elementor-countdown-item {
        background-color: #FFFFFF00;
      }

      body:not(.rtl) .elementor-128245 .elementor-element.elementor-element-d3cf127 .elementor-countdown-item:not(:first-of-type) {
        margin-left: calc(10px/2);
      }

      body:not(.rtl) .elementor-128245 .elementor-element.elementor-element-d3cf127 .elementor-countdown-item:not(:last-of-type) {
        margin-right: calc(10px/2);
      }

      body.rtl .elementor-128245 .elementor-element.elementor-element-d3cf127 .elementor-countdown-item:not(:first-of-type) {
        margin-right: calc(10px/2);
      }

      body.rtl .elementor-128245 .elementor-element.elementor-element-d3cf127 .elementor-countdown-item:not(:last-of-type) {
        margin-left: calc(10px/2);
      }

      .elementor-128245 .elementor-element.elementor-element-d3cf127 .elementor-countdown-digits {
        font-family: "Poppins", Sans-serif;
        font-size: 18px;
        font-weight: normal;
      }

      .elementor-128245 .elementor-element.elementor-element-d3cf127 .elementor-countdown-label {
        font-family: "Poppins", Sans-serif;
        font-size: 10px;
        font-weight: 400;
      }

      .elementor-128245 .elementor-element.elementor-element-d3cf127>.elementor-widget-container {
        margin: 1em 0em 0em 0em;
      }

      .elementor-128245 .elementor-element.elementor-element-2b2552b {
        --lottie-container-max-width: 40px;
      }

      .elementor-128245 .elementor-element.elementor-element-663bb43:not(.elementor-motion-effects-element-type-background),
      .elementor-128245 .elementor-element.elementor-element-663bb43>.elementor-motion-effects-container>.elementor-motion-effects-layer {
        background-color: #EDEDED;
      }

      .elementor-128245 .elementor-element.elementor-element-663bb43 {
        transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
      }

      .elementor-128245 .elementor-element.elementor-element-663bb43>.elementor-background-overlay {
        transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
      }

      .elementor-128245 .elementor-element.elementor-element-35e80ce>.elementor-element-populated>.elementor-widget-wrap {
        padding: 5em 1.5em 12em 1.5em;
      }

      .elementor-128245 .elementor-element.elementor-element-0765357 img {
        opacity: 0.1;
      }

      .elementor-128245 .elementor-element.elementor-element-0765357 {
        width: auto;
        max-width: auto;
        top: 0px;
      }

      body:not(.rtl) .elementor-128245 .elementor-element.elementor-element-0765357 {
        left: 0px;
      }

      body.rtl .elementor-128245 .elementor-element.elementor-element-0765357 {
        right: 0px;
      }

      .elementor-128245 .elementor-element.elementor-element-5eb7525 {
        margin-top: 0em;
        margin-bottom: 3em;
      }

      .elementor-128245 .elementor-element.elementor-element-598fcc0.elementor-column>.elementor-column-wrap>.elementor-widget-wrap {
        justify-content: center;
      }

      .elementor-128245 .elementor-element.elementor-element-13d8394 {
        text-align: center;
        width: auto;
        max-width: auto;
      }

      .elementor-128245 .elementor-element.elementor-element-13d8394 .elementor-heading-title {
        color: #5E5E5E;
        font-family: "Power", Sans-serif;
        font-size: 60px;
        font-weight: normal;
        font-style: normal;
      }

      .elementor-128245 .elementor-element.elementor-element-13d8394>.elementor-widget-container {
        margin: 0em 0em 0em 0em;
      }

      .elementor-128245 .elementor-element.elementor-element-0cd1e16 {
        --divider-border-style: solid;
        --divider-color: #5E5E5E;
        --divider-border-width: 1px;
        width: auto;
        max-width: auto;
      }

      .elementor-128245 .elementor-element.elementor-element-0cd1e16 .elementor-divider-separator {
        width: 90px;
        margin: 0 auto;
        margin-center: 0;
      }

      .elementor-128245 .elementor-element.elementor-element-0cd1e16 .elementor-divider {
        text-align: center;
        padding-top: 0px;
        padding-bottom: 0px;
      }

      .elementor-128245 .elementor-element.elementor-element-0cd1e16>.elementor-widget-container {
        margin: 2em -1.5em 2em -1.5em;
        --e-transform-rotateZ: 90deg;
      }

      .elementor-128245 .elementor-element.elementor-element-9fb2d43 {
        text-align: center;
        width: auto;
        max-width: auto;
      }

      .elementor-128245 .elementor-element.elementor-element-9fb2d43 .elementor-heading-title {
        color: #5E5E5E;
        font-family: "Power", Sans-serif;
        font-size: 60px;
        font-weight: normal;
        font-style: normal;
      }

      .elementor-128245 .elementor-element.elementor-element-9fb2d43>.elementor-widget-container {
        margin: 0em 0em 0em 0em;
      }

      .elementor-128245 .elementor-element.elementor-element-71f739f {
        text-align: center;
      }

      .elementor-128245 .elementor-element.elementor-element-71f739f .elementor-heading-title {
        color: #5E5E5E;
        font-family: "Analogue", Sans-serif;
        font-size: 24px;
        font-weight: normal;
        font-style: italic;
        letter-spacing: 2px;
      }

      .elementor-128245 .elementor-element.elementor-element-666d668 {
        text-align: center;
      }

      .elementor-128245 .elementor-element.elementor-element-666d668 .elementor-heading-title {
        color: #5E5E5E;
        font-family: "Poppins", Sans-serif;
        font-size: 12px;
        font-weight: normal;
        font-style: italic;
        line-height: 1.8em;
      }

      .elementor-128245 .elementor-element.elementor-element-6b182d4>.elementor-container {
        min-height: 350px;
      }

      .elementor-128245 .elementor-element.elementor-element-6b182d4 {
        transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
        margin-top: -4em;
        margin-bottom: 0em;
        z-index: 1;
      }

      .elementor-128245 .elementor-element.elementor-element-6b182d4>.elementor-background-overlay {
        transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
      }

      .elementor-bc-flex-widget .elementor-128245 .elementor-element.elementor-element-d46a144.elementor-column .elementor-column-wrap {
        align-items: center;
      }

      .elementor-128245 .elementor-element.elementor-element-d46a144.elementor-column.elementor-element[data-element_type="column"]>.elementor-column-wrap.elementor-element-populated>.elementor-widget-wrap {
        align-content: center;
        align-items: center;
      }

      .elementor-128245 .elementor-element.elementor-element-d46a144:not(.elementor-motion-effects-element-type-background)>.elementor-column-wrap,
      .elementor-128245 .elementor-element.elementor-element-d46a144>.elementor-column-wrap>.elementor-motion-effects-container>.elementor-motion-effects-layer {
        background-image: url("wp-content/uploads/2023/01/men.jpg");
        background-position: center center;
        background-size: cover;
      }

      .elementor-128245 .elementor-element.elementor-element-d46a144>.elementor-element-populated {
        box-shadow: 25px 25px 0px 0px rgba(230, 230, 230, 0.5);
        transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
      }

      .elementor-128245 .elementor-element.elementor-element-d46a144>.elementor-element-populated>.elementor-background-overlay {
        transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
      }

      .elementor-128245 .elementor-element.elementor-element-d46a144>.elementor-element-populated>.elementor-widget-wrap {
        padding: 0px 0px 0px 0px;
      }

      .elementor-128245 .elementor-element.elementor-element-ddd5b51 {
        text-align: center;
        width: 350px;
        max-width: 350px;
      }

      .elementor-128245 .elementor-element.elementor-element-ddd5b51 .elementor-heading-title {
        color: #FFFFFF;
        font-family: "Analogue", Sans-serif;
        font-size: 50px;
        font-weight: normal;
        letter-spacing: 2px;
      }

      .elementor-128245 .elementor-element.elementor-element-ddd5b51>.elementor-widget-container {
        margin: 0em 0em 0em 0em;
      }

      body:not(.rtl) .elementor-128245 .elementor-element.elementor-element-ddd5b51 {
        left: 27%;
      }

      body.rtl .elementor-128245 .elementor-element.elementor-element-ddd5b51 {
        right: 27%;
      }

      .elementor-128245 .elementor-element.elementor-element-a5b8dba>.elementor-element-populated>.elementor-widget-wrap {
        padding: 0px 0px 0px 0px;
      }

      .elementor-128245 .elementor-element.elementor-element-e44369e>.elementor-container {
        min-height: 300px;
      }

      .elementor-128245 .elementor-element.elementor-element-e44369e:not(.elementor-motion-effects-element-type-background),
      .elementor-128245 .elementor-element.elementor-element-e44369e>.elementor-motion-effects-container>.elementor-motion-effects-layer {
        background-color: #F8F8F8;
      }

      .elementor-128245 .elementor-element.elementor-element-e44369e {
        transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
      }

      .elementor-128245 .elementor-element.elementor-element-e44369e>.elementor-background-overlay {
        transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
      }

      .elementor-128245 .elementor-element.elementor-element-4e05e79>.elementor-column-wrap>.elementor-widget-wrap>.elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
        margin-bottom: 5px;
      }

      .elementor-128245 .elementor-element.elementor-element-4e05e79>.elementor-element-populated>.elementor-widget-wrap {
        padding: 3em 2em 0em 2em;
      }

      .elementor-128245 .elementor-element.elementor-element-35e168a {
        text-align: left;
      }

      .elementor-128245 .elementor-element.elementor-element-35e168a .elementor-heading-title {
        color: #5E5E5E;
        font-family: "Analogue", Sans-serif;
        font-weight: normal;
      }

      .elementor-128245 .elementor-element.elementor-element-35e168a>.elementor-widget-container {
        margin: 0em 0em 0.8em 0em;
      }

      .elementor-128245 .elementor-element.elementor-element-44ca2c9 {
        text-align: left;
      }

      .elementor-128245 .elementor-element.elementor-element-44ca2c9 .elementor-heading-title {
        color: #5E5E5E;
        font-family: "Poppins", Sans-serif;
        font-size: 12px;
        font-weight: 500;
        font-style: normal;
      }

      .elementor-128245 .elementor-element.elementor-element-0156c7b {
        text-align: left;
      }

      .elementor-128245 .elementor-element.elementor-element-0156c7b .elementor-heading-title {
        color: #5E5E5E;
        font-family: "Poppins", Sans-serif;
        font-size: 12px;
        font-weight: normal;
        font-style: normal;
        line-height: 1.5em;
      }

      .elementor-128245 .elementor-element.elementor-element-56d9e2d .elementor-button {
        font-family: "Poppins", Sans-serif;
        font-size: 12px;
        font-weight: normal;
        letter-spacing: 2px;
        fill: #FFFFFF;
        color: #FFFFFF;
        background-color: #5E5E5E;
        border-radius: 0px 0px 0px 0px;
        padding: 8px 20px 8px 20px;
      }

      .elementor-128245 .elementor-element.elementor-element-56d9e2d .elementor-button:hover,
      .elementor-128245 .elementor-element.elementor-element-56d9e2d .elementor-button:focus {
        background-color: #434343;
      }

      .elementor-128245 .elementor-element.elementor-element-56d9e2d {
        z-index: 999;
        width: auto;
        max-width: auto;
      }

      .elementor-128245 .elementor-element.elementor-element-b92dacd>.elementor-container {
        min-height: 350px;
      }

      .elementor-128245 .elementor-element.elementor-element-b92dacd {
        transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
        margin-top: 0em;
        margin-bottom: 0em;
        z-index: 1;
      }

      .elementor-128245 .elementor-element.elementor-element-b92dacd>.elementor-background-overlay {
        transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
      }

      .elementor-bc-flex-widget .elementor-128245 .elementor-element.elementor-element-bc50505.elementor-column .elementor-column-wrap {
        align-items: center;
      }

      .elementor-128245 .elementor-element.elementor-element-bc50505.elementor-column.elementor-element[data-element_type="column"]>.elementor-column-wrap.elementor-element-populated>.elementor-widget-wrap {
        align-content: center;
        align-items: center;
      }

      .elementor-128245 .elementor-element.elementor-element-bc50505>.elementor-element-populated {
        transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
      }

      .elementor-128245 .elementor-element.elementor-element-bc50505>.elementor-element-populated>.elementor-background-overlay {
        transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
      }

      .elementor-128245 .elementor-element.elementor-element-bc50505>.elementor-element-populated>.elementor-widget-wrap {
        padding: 0px 0px 0px 0px;
      }

      .elementor-bc-flex-widget .elementor-128245 .elementor-element.elementor-element-31d0772.elementor-column .elementor-column-wrap {
        align-items: center;
      }

      .elementor-128245 .elementor-element.elementor-element-31d0772.elementor-column.elementor-element[data-element_type="column"]>.elementor-column-wrap.elementor-element-populated>.elementor-widget-wrap {
        align-content: center;
        align-items: center;
      }

      .elementor-128245 .elementor-element.elementor-element-31d0772:not(.elementor-motion-effects-element-type-background)>.elementor-column-wrap,
      .elementor-128245 .elementor-element.elementor-element-31d0772>.elementor-column-wrap>.elementor-motion-effects-container>.elementor-motion-effects-layer {
        background-image: url("wp-content/uploads/2023/01/women.jpg");
        background-position: center center;
        background-size: cover;
      }

      .elementor-128245 .elementor-element.elementor-element-31d0772>.elementor-element-populated {
        box-shadow: -25px 25px 0px 0px rgba(230, 230, 230, 0.5);
        transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
      }

      .elementor-128245 .elementor-element.elementor-element-31d0772>.elementor-element-populated>.elementor-background-overlay {
        transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
      }

      .elementor-128245 .elementor-element.elementor-element-31d0772>.elementor-element-populated>.elementor-widget-wrap {
        padding: 0px 0px 0px 0px;
      }

      .elementor-128245 .elementor-element.elementor-element-d475a39 {
        text-align: center;
        width: 350px;
        max-width: 350px;
      }

      .elementor-128245 .elementor-element.elementor-element-d475a39 .elementor-heading-title {
        color: #FFFFFF;
        font-family: "Analogue", Sans-serif;
        font-size: 50px;
        font-weight: normal;
        letter-spacing: 2px;
      }

      .elementor-128245 .elementor-element.elementor-element-d475a39>.elementor-widget-container {
        margin: 0em 0em 0em 0em;
      }

      body:not(.rtl) .elementor-128245 .elementor-element.elementor-element-d475a39 {
        right: 27%;
      }

      body.rtl .elementor-128245 .elementor-element.elementor-element-d475a39 {
        left: 27%;
      }

      .elementor-128245 .elementor-element.elementor-element-62175bc>.elementor-container {
        min-height: 300px;
      }

      .elementor-128245 .elementor-element.elementor-element-62175bc:not(.elementor-motion-effects-element-type-background),
      .elementor-128245 .elementor-element.elementor-element-62175bc>.elementor-motion-effects-container>.elementor-motion-effects-layer {
        background-color: transparent;
        background-image: linear-gradient(180deg, #F8F8F8 0%, #D8D8D8 100%);
      }

      .elementor-128245 .elementor-element.elementor-element-62175bc {
        transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
      }

      .elementor-128245 .elementor-element.elementor-element-62175bc>.elementor-background-overlay {
        transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
      }

      .elementor-128245 .elementor-element.elementor-element-9bedd01.elementor-column>.elementor-column-wrap>.elementor-widget-wrap {
        justify-content: flex-end;
      }

      .elementor-128245 .elementor-element.elementor-element-9bedd01>.elementor-column-wrap>.elementor-widget-wrap>.elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
        margin-bottom: 5px;
      }

      .elementor-128245 .elementor-element.elementor-element-9bedd01>.elementor-element-populated>.elementor-widget-wrap {
        padding: 3em 2em 0em 2em;
      }

      .elementor-128245 .elementor-element.elementor-element-9dedd2c {
        text-align: right;
      }

      .elementor-128245 .elementor-element.elementor-element-9dedd2c .elementor-heading-title {
        color: #5E5E5E;
        font-family: "Analogue", Sans-serif;
        font-weight: normal;
      }

      .elementor-128245 .elementor-element.elementor-element-9dedd2c>.elementor-widget-container {
        margin: 0em 0em 0.8em 0em;
      }

      .elementor-128245 .elementor-element.elementor-element-5e0f78b {
        text-align: right;
      }

      .elementor-128245 .elementor-element.elementor-element-5e0f78b .elementor-heading-title {
        color: #5E5E5E;
        font-family: "Poppins", Sans-serif;
        font-size: 12px;
        font-weight: 500;
        font-style: normal;
      }

      .elementor-128245 .elementor-element.elementor-element-1cf51fb {
        text-align: right;
      }

      .elementor-128245 .elementor-element.elementor-element-1cf51fb .elementor-heading-title {
        color: #5E5E5E;
        font-family: "Poppins", Sans-serif;
        font-size: 12px;
        font-weight: normal;
        font-style: normal;
        line-height: 1.5em;
      }

      .elementor-128245 .elementor-element.elementor-element-b0c8d5e .elementor-button {
        font-family: "Poppins", Sans-serif;
        font-size: 12px;
        font-weight: normal;
        letter-spacing: 2px;
        fill: #FFFFFF;
        color: #FFFFFF;
        background-color: #5E5E5E;
        border-radius: 0px 0px 0px 0px;
        padding: 8px 20px 8px 20px;
      }

      .elementor-128245 .elementor-element.elementor-element-b0c8d5e .elementor-button:hover,
      .elementor-128245 .elementor-element.elementor-element-b0c8d5e .elementor-button:focus {
        background-color: #434343;
      }

      .elementor-128245 .elementor-element.elementor-element-b0c8d5e {
        width: auto;
        max-width: auto;
      }

      .elementor-128245 .elementor-element.elementor-element-c92e288>.elementor-container {
        min-height: 400px;
      }

      .elementor-128245 .elementor-element.elementor-element-c92e288 {
        overflow: hidden;
        transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
      }

      .elementor-128245 .elementor-element.elementor-element-c92e288 .elementor-background-slideshow__slide__image {
        background-size: cover;
        background-position: center center;
      }

      .elementor-128245 .elementor-element.elementor-element-c92e288>.elementor-background-overlay {
        background-color: #F8F8F8;
        opacity: 0.8;
        transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
      }

      .elementor-128245 .elementor-element.elementor-element-ae0ff4e.elementor-column>.elementor-column-wrap>.elementor-widget-wrap {
        justify-content: center;
      }

      .elementor-128245 .elementor-element.elementor-element-ae0ff4e>.elementor-column-wrap>.elementor-widget-wrap>.elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
        margin-bottom: 5px;
      }

      .elementor-128245 .elementor-element.elementor-element-ae0ff4e>.elementor-element-populated>.elementor-widget-wrap {
        padding: 5em 2em 8em 2em;
      }

      .elementor-128245 .elementor-element.elementor-element-64a3ba8 {
        text-align: left;
      }

      .elementor-128245 .elementor-element.elementor-element-64a3ba8 .elementor-heading-title {
        color: #5E5E5E;
        font-family: "Power", Sans-serif;
        font-size: 40px;
        font-weight: normal;
        font-style: italic;
      }

      .elementor-128245 .elementor-element.elementor-element-64a3ba8>.elementor-widget-container {
        margin: 0em 0em 0em 0em;
      }

      .elementor-128245 .elementor-element.elementor-element-f03ebad {
        --divider-border-style: solid;
        --divider-color: #5E5E5E;
        --divider-border-width: 1px;
      }

      .elementor-128245 .elementor-element.elementor-element-f03ebad .elementor-divider-separator {
        width: 60%;
        margin: 0 auto;
        margin-right: 0;
      }

      .elementor-128245 .elementor-element.elementor-element-f03ebad .elementor-divider {
        text-align: right;
      }

      .elementor-128245 .elementor-element.elementor-element-f03ebad>.elementor-widget-container {
        margin: 0em -3em 0em 0em;
      }

      .elementor-128245 .elementor-element.elementor-element-e521c8f {
        text-align: left;
      }

      .elementor-128245 .elementor-element.elementor-element-e521c8f .elementor-heading-title {
        color: #5E5E5E;
        font-family: "Creattion", Sans-serif;
        font-size: 50px;
        font-weight: normal;
        font-style: italic;
      }

      .elementor-128245 .elementor-element.elementor-element-e521c8f>.elementor-widget-container {
        margin: -1em 0em 0em 0em;
      }

      .elementor-128245 .elementor-element.elementor-element-d43cd28 {
        overflow: hidden;
        transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
        margin-top: 2em;
        margin-bottom: 0em;
      }

      .elementor-128245 .elementor-element.elementor-element-d43cd28:not(.elementor-motion-effects-element-type-background),
      .elementor-128245 .elementor-element.elementor-element-d43cd28>.elementor-motion-effects-container>.elementor-motion-effects-layer {
        background-color: #FFFFFF;
      }

      .elementor-128245 .elementor-element.elementor-element-d43cd28,
      .elementor-128245 .elementor-element.elementor-element-d43cd28>.elementor-background-overlay {
        border-radius: 0px 150px 0px 0px;
      }

      .elementor-128245 .elementor-element.elementor-element-d43cd28>.elementor-background-overlay {
        transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
      }

      .elementor-128245 .elementor-element.elementor-element-44aacf7>.elementor-element-populated {
        margin: 0px 0px 0px 0px;
        --e-column-margin-right: 0px;
        --e-column-margin-left: 0px;
      }

      .elementor-128245 .elementor-element.elementor-element-44aacf7>.elementor-element-populated>.elementor-widget-wrap {
        padding: 0px 0px 0px 0px;
      }

      .elementor-128245 .elementor-element.elementor-element-c8f738e>.elementor-container {
        min-height: 225px;
      }

      .elementor-128245 .elementor-element.elementor-element-c8f738e {
        overflow: hidden;
        transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
      }

      .elementor-128245 .elementor-element.elementor-element-c8f738e,
      .elementor-128245 .elementor-element.elementor-element-c8f738e>.elementor-background-overlay {
        border-radius: 0px 150px 0px 0px;
      }

      .elementor-128245 .elementor-element.elementor-element-c8f738e>.elementor-background-overlay {
        transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
      }

      .elementor-128245 .elementor-element.elementor-element-ab06960 .elementor-background-slideshow__slide__image {
        background-size: cover;
        background-position: top center;
      }

      .elementor-128245 .elementor-element.elementor-element-ab06960>.elementor-element-populated,
      .elementor-128245 .elementor-element.elementor-element-ab06960>.elementor-element-populated>.elementor-background-overlay,
      .elementor-128245 .elementor-element.elementor-element-ab06960>.elementor-background-slideshow {
        border-radius: 0px 150px 0px 0px;
      }

      .elementor-128245 .elementor-element.elementor-element-ab06960>.elementor-element-populated {
        transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
      }

      .elementor-128245 .elementor-element.elementor-element-ab06960>.elementor-element-populated>.elementor-background-overlay {
        transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
      }

      .elementor-128245 .elementor-element.elementor-element-697a9c5>.elementor-container {
        min-height: 350px;
      }

      .elementor-bc-flex-widget .elementor-128245 .elementor-element.elementor-element-7f27de5.elementor-column .elementor-column-wrap {
        align-items: center;
      }

      .elementor-128245 .elementor-element.elementor-element-7f27de5.elementor-column.elementor-element[data-element_type="column"]>.elementor-column-wrap.elementor-element-populated>.elementor-widget-wrap {
        align-content: center;
        align-items: center;
      }

      .elementor-128245 .elementor-element.elementor-element-7f27de5.elementor-column>.elementor-column-wrap>.elementor-widget-wrap {
        justify-content: center;
      }

      .elementor-128245 .elementor-element.elementor-element-7f27de5>.elementor-column-wrap>.elementor-widget-wrap>.elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
        margin-bottom: 0px;
      }

      .elementor-128245 .elementor-element.elementor-element-7f27de5:not(.elementor-motion-effects-element-type-background)>.elementor-column-wrap,
      .elementor-128245 .elementor-element.elementor-element-7f27de5>.elementor-column-wrap>.elementor-motion-effects-container>.elementor-motion-effects-layer {
        background-color: #5E5E5E;
      }

      .elementor-128245 .elementor-element.elementor-element-7f27de5>.elementor-element-populated {
        transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
      }

      .elementor-128245 .elementor-element.elementor-element-7f27de5>.elementor-element-populated>.elementor-background-overlay {
        transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
      }

      .elementor-128245 .elementor-element.elementor-element-6ac62a0 {
        text-align: center;
        width: auto;
        max-width: auto;
      }

      .elementor-128245 .elementor-element.elementor-element-6ac62a0 .elementor-heading-title {
        color: #FFFFFF;
        font-family: "Power", Sans-serif;
        font-size: 40px;
        font-weight: normal;
        font-style: normal;
      }

      .elementor-128245 .elementor-element.elementor-element-6ac62a0>.elementor-widget-container {
        margin: 8em -8em 8em -8em;
        padding: 0px 0px 0px 0px;
        --e-transform-rotateZ: -90deg;
      }

      .elementor-bc-flex-widget .elementor-128245 .elementor-element.elementor-element-f5a3804.elementor-column .elementor-column-wrap {
        align-items: center;
      }

      .elementor-128245 .elementor-element.elementor-element-f5a3804.elementor-column.elementor-element[data-element_type="column"]>.elementor-column-wrap.elementor-element-populated>.elementor-widget-wrap {
        align-content: center;
        align-items: center;
      }

      .elementor-128245 .elementor-element.elementor-element-f5a3804>.elementor-column-wrap>.elementor-widget-wrap>.elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
        margin-bottom: 5px;
      }

      .elementor-128245 .elementor-element.elementor-element-f5a3804>.elementor-element-populated>.elementor-widget-wrap {
        padding: 2em 1em 1em 1.5em;
      }

      .elementor-128245 .elementor-element.elementor-element-c4d6feb {
        border-style: solid;
        border-width: 0px 0px 1px 0px;
        border-color: #5E5E5E50;
        transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
        margin-top: 0em;
        margin-bottom: 0.8em;
      }

      .elementor-128245 .elementor-element.elementor-element-c4d6feb>.elementor-background-overlay {
        transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
      }

      .elementor-bc-flex-widget .elementor-128245 .elementor-element.elementor-element-ee784a5.elementor-column .elementor-column-wrap {
        align-items: center;
      }

      .elementor-128245 .elementor-element.elementor-element-ee784a5.elementor-column.elementor-element[data-element_type="column"]>.elementor-column-wrap.elementor-element-populated>.elementor-widget-wrap {
        align-content: center;
        align-items: center;
      }

      .elementor-128245 .elementor-element.elementor-element-ee784a5.elementor-column>.elementor-column-wrap>.elementor-widget-wrap {
        justify-content: center;
      }

      .elementor-128245 .elementor-element.elementor-element-ee784a5>.elementor-column-wrap>.elementor-widget-wrap>.elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
        margin-bottom: 0px;
      }

      .elementor-128245 .elementor-element.elementor-element-ee784a5>.elementor-element-populated>.elementor-widget-wrap {
        padding: 0px 0px 0px 0px;
      }

      .elementor-128245 .elementor-element.elementor-element-029d5a9 {
        text-align: left;
      }

      .elementor-128245 .elementor-element.elementor-element-029d5a9 .elementor-heading-title {
        color: #5E5E5E;
        font-family: "Analogue", Sans-serif;
        font-size: 65px;
        font-weight: normal;
        font-style: normal;
      }

      .elementor-128245 .elementor-element.elementor-element-029d5a9>.elementor-widget-container {
        margin: 0em 0em 0.25em 0em;
        padding: 0px 0px 0px 0px;
      }

      .elementor-bc-flex-widget .elementor-128245 .elementor-element.elementor-element-91caa13.elementor-column .elementor-column-wrap {
        align-items: center;
      }

      .elementor-128245 .elementor-element.elementor-element-91caa13.elementor-column.elementor-element[data-element_type="column"]>.elementor-column-wrap.elementor-element-populated>.elementor-widget-wrap {
        align-content: center;
        align-items: center;
      }

      .elementor-128245 .elementor-element.elementor-element-91caa13.elementor-column>.elementor-column-wrap>.elementor-widget-wrap {
        justify-content: center;
      }

      .elementor-128245 .elementor-element.elementor-element-91caa13>.elementor-column-wrap>.elementor-widget-wrap>.elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
        margin-bottom: 0px;
      }

      .elementor-128245 .elementor-element.elementor-element-91caa13>.elementor-element-populated>.elementor-widget-wrap {
        padding: 0px 0px 0px 0px;
      }

      .elementor-128245 .elementor-element.elementor-element-0760287 {
        text-align: left;
      }

      .elementor-128245 .elementor-element.elementor-element-0760287 .elementor-heading-title {
        color: #5E5E5E;
        font-family: "Poppins", Sans-serif;
        font-size: 15px;
        font-weight: 400;
        font-style: normal;
        letter-spacing: 2px;
      }

      .elementor-128245 .elementor-element.elementor-element-0760287>.elementor-widget-container {
        margin: 0em 0em 0.5em 0em;
      }

      .elementor-128245 .elementor-element.elementor-element-4fb147c {
        text-align: left;
      }

      .elementor-128245 .elementor-element.elementor-element-4fb147c .elementor-heading-title {
        color: #5E5E5E;
        font-family: "Poppins", Sans-serif;
        font-size: 15px;
        font-weight: 400;
        font-style: normal;
        letter-spacing: 2px;
      }

      .elementor-128245 .elementor-element.elementor-element-4fb147c>.elementor-widget-container {
        margin: 0em 0em 0.5em 0em;
      }

      .elementor-128245 .elementor-element.elementor-element-376027b {
        text-align: left;
      }

      .elementor-128245 .elementor-element.elementor-element-376027b .elementor-heading-title {
        color: #5E5E5E;
        font-family: "Poppins", Sans-serif;
        font-size: 15px;
        font-weight: 400;
        font-style: normal;
        letter-spacing: 2px;
      }

      .elementor-128245 .elementor-element.elementor-element-376027b>.elementor-widget-container {
        margin: 0em 0em 0em 0em;
      }

      .elementor-128245 .elementor-element.elementor-element-56c487c .elementor-icon-list-icon i {
        color: #5E5E5E;
      }

      .elementor-128245 .elementor-element.elementor-element-56c487c .elementor-icon-list-icon svg {
        fill: #5E5E5E;
      }

      .elementor-128245 .elementor-element.elementor-element-56c487c {
        --e-icon-list-icon-size: 14px;
      }

      .elementor-128245 .elementor-element.elementor-element-56c487c .elementor-icon-list-text {
        color: #5E5E5E;
      }

      .elementor-128245 .elementor-element.elementor-element-56c487c .elementor-icon-list-item>.elementor-icon-list-text,
      .elementor-128245 .elementor-element.elementor-element-56c487c .elementor-icon-list-item>a {
        font-family: "Poppins", Sans-serif;
        font-size: 15px;
        font-weight: 400;
        letter-spacing: 1.5px;
      }

      .elementor-128245 .elementor-element.elementor-element-926d8b2 {
        text-align: left;
      }

      .elementor-128245 .elementor-element.elementor-element-926d8b2 .elementor-heading-title {
        color: #5E5E5E;
        font-family: "Poppins", Sans-serif;
        font-size: 15px;
        font-weight: 600;
        text-transform: uppercase;
        font-style: normal;
        letter-spacing: 1.5px;
      }

      .elementor-128245 .elementor-element.elementor-element-926d8b2>.elementor-widget-container {
        margin: 2em 0em 1em 0em;
      }

      .elementor-128245 .elementor-element.elementor-element-65eec21 {
        text-align: left;
      }

      .elementor-128245 .elementor-element.elementor-element-65eec21 .elementor-heading-title {
        color: #5E5E5E;
        font-family: "Poppins", Sans-serif;
        font-size: 12px;
        font-weight: normal;
        font-style: normal;
        line-height: 1.5em;
      }

      .elementor-128245 .elementor-element.elementor-element-65eec21>.elementor-widget-container {
        padding: 0em 0em 0em 0em;
      }

      .elementor-128245 .elementor-element.elementor-element-2fb5f40 .elementor-button {
        font-family: "Poppins", Sans-serif;
        font-size: 12px;
        font-weight: normal;
        letter-spacing: 2px;
        fill: #5E5E5E;
        color: #5E5E5E;
        background-color: #5E5E5E00;
        border-style: solid;
        border-width: 1px 1px 1px 1px;
        border-color: #5E5E5E;
        border-radius: 0px 0px 0px 0px;
        padding: 8px 15px 8px 15px;
      }

      .elementor-128245 .elementor-element.elementor-element-2fb5f40 .elementor-button:hover,
      .elementor-128245 .elementor-element.elementor-element-2fb5f40 .elementor-button:focus {
        color: #FFFFFF;
        background-color: #5E5E5E;
      }

      .elementor-128245 .elementor-element.elementor-element-2fb5f40 .elementor-button:hover svg,
      .elementor-128245 .elementor-element.elementor-element-2fb5f40 .elementor-button:focus svg {
        fill: #FFFFFF;
      }

      .elementor-128245 .elementor-element.elementor-element-2fb5f40 {
        z-index: 999;
        width: auto;
        max-width: auto;
      }

      .elementor-128245 .elementor-element.elementor-element-feeb831 {
        overflow: hidden;
        transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
        margin-top: 4em;
        margin-bottom: 0em;
      }

      .elementor-128245 .elementor-element.elementor-element-feeb831:not(.elementor-motion-effects-element-type-background),
      .elementor-128245 .elementor-element.elementor-element-feeb831>.elementor-motion-effects-container>.elementor-motion-effects-layer {
        background-color: #FFFFFF;
      }

      .elementor-128245 .elementor-element.elementor-element-feeb831,
      .elementor-128245 .elementor-element.elementor-element-feeb831>.elementor-background-overlay {
        border-radius: 150px 0px 0px 0px;
      }

      .elementor-128245 .elementor-element.elementor-element-feeb831>.elementor-background-overlay {
        transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
      }

      .elementor-128245 .elementor-element.elementor-element-8bae223>.elementor-element-populated {
        margin: 0px 0px 0px 0px;
        --e-column-margin-right: 0px;
        --e-column-margin-left: 0px;
      }

      .elementor-128245 .elementor-element.elementor-element-8bae223>.elementor-element-populated>.elementor-widget-wrap {
        padding: 0px 0px 0px 0px;
      }

      .elementor-128245 .elementor-element.elementor-element-d89b3fe>.elementor-container {
        min-height: 225px;
      }

      .elementor-128245 .elementor-element.elementor-element-d89b3fe {
        overflow: hidden;
        transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
      }

      .elementor-128245 .elementor-element.elementor-element-d89b3fe,
      .elementor-128245 .elementor-element.elementor-element-d89b3fe>.elementor-background-overlay {
        border-radius: 150px 0px 0px 0px;
      }

      .elementor-128245 .elementor-element.elementor-element-d89b3fe>.elementor-background-overlay {
        transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
      }

      .elementor-128245 .elementor-element.elementor-element-629e426>.elementor-element-populated,
      .elementor-128245 .elementor-element.elementor-element-629e426>.elementor-element-populated>.elementor-background-overlay,
      .elementor-128245 .elementor-element.elementor-element-629e426>.elementor-background-slideshow {
        border-radius: 150px 0px 0px 0px;
      }

      .elementor-128245 .elementor-element.elementor-element-629e426>.elementor-element-populated {
        transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
      }

      .elementor-128245 .elementor-element.elementor-element-629e426>.elementor-element-populated>.elementor-background-overlay {
        transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
      }

      .elementor-128245 .elementor-element.elementor-element-7323c99>.elementor-container {
        min-height: 350px;
      }

      .elementor-bc-flex-widget .elementor-128245 .elementor-element.elementor-element-37f8b28.elementor-column .elementor-column-wrap {
        align-items: center;
      }

      .elementor-128245 .elementor-element.elementor-element-37f8b28.elementor-column.elementor-element[data-element_type="column"]>.elementor-column-wrap.elementor-element-populated>.elementor-widget-wrap {
        align-content: center;
        align-items: center;
      }

      .elementor-128245 .elementor-element.elementor-element-37f8b28.elementor-column>.elementor-column-wrap>.elementor-widget-wrap {
        justify-content: flex-start;
      }

      .elementor-128245 .elementor-element.elementor-element-37f8b28>.elementor-column-wrap>.elementor-widget-wrap>.elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
        margin-bottom: 5px;
      }

      .elementor-128245 .elementor-element.elementor-element-37f8b28>.elementor-element-populated>.elementor-widget-wrap {
        padding: 2em 1em 1em 1.5em;
      }

      .elementor-128245 .elementor-element.elementor-element-d7c932d {
        border-style: solid;
        border-width: 0px 0px 1px 0px;
        border-color: #5E5E5E50;
        transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
        margin-top: 0em;
        margin-bottom: 0.8em;
      }

      .elementor-128245 .elementor-element.elementor-element-d7c932d>.elementor-background-overlay {
        transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
      }

      .elementor-bc-flex-widget .elementor-128245 .elementor-element.elementor-element-c9ad365.elementor-column .elementor-column-wrap {
        align-items: center;
      }

      .elementor-128245 .elementor-element.elementor-element-c9ad365.elementor-column.elementor-element[data-element_type="column"]>.elementor-column-wrap.elementor-element-populated>.elementor-widget-wrap {
        align-content: center;
        align-items: center;
      }

      .elementor-128245 .elementor-element.elementor-element-c9ad365.elementor-column>.elementor-column-wrap>.elementor-widget-wrap {
        justify-content: center;
      }

      .elementor-128245 .elementor-element.elementor-element-c9ad365>.elementor-column-wrap>.elementor-widget-wrap>.elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
        margin-bottom: 0px;
      }

      .elementor-128245 .elementor-element.elementor-element-c9ad365>.elementor-element-populated>.elementor-widget-wrap {
        padding: 0px 0px 0px 0px;
      }

      .elementor-128245 .elementor-element.elementor-element-1ba1140 {
        text-align: left;
      }

      .elementor-128245 .elementor-element.elementor-element-1ba1140 .elementor-heading-title {
        color: #5E5E5E;
        font-family: "Analogue", Sans-serif;
        font-size: 65px;
        font-weight: normal;
        font-style: normal;
      }

      .elementor-128245 .elementor-element.elementor-element-1ba1140>.elementor-widget-container {
        margin: 0em 0em 0.25em 0em;
        padding: 0px 0px 0px 0px;
      }

      .elementor-bc-flex-widget .elementor-128245 .elementor-element.elementor-element-e041a66.elementor-column .elementor-column-wrap {
        align-items: center;
      }

      .elementor-128245 .elementor-element.elementor-element-e041a66.elementor-column.elementor-element[data-element_type="column"]>.elementor-column-wrap.elementor-element-populated>.elementor-widget-wrap {
        align-content: center;
        align-items: center;
      }

      .elementor-128245 .elementor-element.elementor-element-e041a66.elementor-column>.elementor-column-wrap>.elementor-widget-wrap {
        justify-content: center;
      }

      .elementor-128245 .elementor-element.elementor-element-e041a66>.elementor-column-wrap>.elementor-widget-wrap>.elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
        margin-bottom: 0px;
      }

      .elementor-128245 .elementor-element.elementor-element-e041a66>.elementor-element-populated>.elementor-widget-wrap {
        padding: 0px 0px 0px 0px;
      }

      .elementor-128245 .elementor-element.elementor-element-fe77200 {
        text-align: left;
      }

      .elementor-128245 .elementor-element.elementor-element-fe77200 .elementor-heading-title {
        color: #5E5E5E;
        font-family: "Poppins", Sans-serif;
        font-size: 15px;
        font-weight: 400;
        font-style: normal;
        letter-spacing: 2px;
      }

      .elementor-128245 .elementor-element.elementor-element-fe77200>.elementor-widget-container {
        margin: 0em 0em 0.5em 0em;
      }

      .elementor-128245 .elementor-element.elementor-element-9ec9fd0 {
        text-align: left;
      }

      .elementor-128245 .elementor-element.elementor-element-9ec9fd0 .elementor-heading-title {
        color: #5E5E5E;
        font-family: "Poppins", Sans-serif;
        font-size: 15px;
        font-weight: 400;
        font-style: normal;
        letter-spacing: 2px;
      }

      .elementor-128245 .elementor-element.elementor-element-9ec9fd0>.elementor-widget-container {
        margin: 0em 0em 0.5em 0em;
      }

      .elementor-128245 .elementor-element.elementor-element-b878d5b {
        text-align: left;
      }

      .elementor-128245 .elementor-element.elementor-element-b878d5b .elementor-heading-title {
        color: #5E5E5E;
        font-family: "Poppins", Sans-serif;
        font-size: 15px;
        font-weight: 400;
        font-style: normal;
        letter-spacing: 2px;
      }

      .elementor-128245 .elementor-element.elementor-element-b878d5b>.elementor-widget-container {
        margin: 0em 0em 0em 0em;
      }

      .elementor-128245 .elementor-element.elementor-element-047d9c4 .elementor-icon-list-icon i {
        color: #5E5E5E;
      }

      .elementor-128245 .elementor-element.elementor-element-047d9c4 .elementor-icon-list-icon svg {
        fill: #5E5E5E;
      }

      .elementor-128245 .elementor-element.elementor-element-047d9c4 {
        --e-icon-list-icon-size: 14px;
      }

      .elementor-128245 .elementor-element.elementor-element-047d9c4 .elementor-icon-list-text {
        color: #5E5E5E;
      }

      .elementor-128245 .elementor-element.elementor-element-047d9c4 .elementor-icon-list-item>.elementor-icon-list-text,
      .elementor-128245 .elementor-element.elementor-element-047d9c4 .elementor-icon-list-item>a {
        font-family: "Poppins", Sans-serif;
        font-size: 15px;
        font-weight: 400;
        letter-spacing: 1.5px;
      }

      .elementor-128245 .elementor-element.elementor-element-d3d0c6d {
        text-align: left;
      }

      .elementor-128245 .elementor-element.elementor-element-d3d0c6d .elementor-heading-title {
        color: #5E5E5E;
        font-family: "Poppins", Sans-serif;
        font-size: 15px;
        font-weight: 600;
        text-transform: uppercase;
        font-style: normal;
        letter-spacing: 1.5px;
      }

      .elementor-128245 .elementor-element.elementor-element-d3d0c6d>.elementor-widget-container {
        margin: 2em 0em 1em 0em;
      }

      .elementor-128245 .elementor-element.elementor-element-5815811 {
        text-align: left;
      }

      .elementor-128245 .elementor-element.elementor-element-5815811 .elementor-heading-title {
        color: #5E5E5E;
        font-family: "Poppins", Sans-serif;
        font-size: 12px;
        font-weight: normal;
        font-style: normal;
        line-height: 1.5em;
      }

      .elementor-128245 .elementor-element.elementor-element-5815811>.elementor-widget-container {
        padding: 0em 0em 0em 0em;
      }

      .elementor-128245 .elementor-element.elementor-element-ac48de4 .elementor-button {
        font-family: "Poppins", Sans-serif;
        font-size: 12px;
        font-weight: normal;
        letter-spacing: 2px;
        fill: #5E5E5E;
        color: #5E5E5E;
        background-color: #5E5E5E00;
        border-style: solid;
        border-width: 1px 1px 1px 1px;
        border-color: #5E5E5E;
        border-radius: 0px 0px 0px 0px;
        padding: 8px 15px 8px 15px;
      }

      .elementor-128245 .elementor-element.elementor-element-ac48de4 .elementor-button:hover,
      .elementor-128245 .elementor-element.elementor-element-ac48de4 .elementor-button:focus {
        color: #FFFFFF;
        background-color: #5E5E5E;
      }

      .elementor-128245 .elementor-element.elementor-element-ac48de4 .elementor-button:hover svg,
      .elementor-128245 .elementor-element.elementor-element-ac48de4 .elementor-button:focus svg {
        fill: #FFFFFF;
      }

      .elementor-128245 .elementor-element.elementor-element-ac48de4 {
        z-index: 999;
        width: auto;
        max-width: auto;
      }

      .elementor-bc-flex-widget .elementor-128245 .elementor-element.elementor-element-49e38d9.elementor-column .elementor-column-wrap {
        align-items: center;
      }

      .elementor-128245 .elementor-element.elementor-element-49e38d9.elementor-column.elementor-element[data-element_type="column"]>.elementor-column-wrap.elementor-element-populated>.elementor-widget-wrap {
        align-content: center;
        align-items: center;
      }

      .elementor-128245 .elementor-element.elementor-element-49e38d9.elementor-column>.elementor-column-wrap>.elementor-widget-wrap {
        justify-content: center;
      }

      .elementor-128245 .elementor-element.elementor-element-49e38d9>.elementor-column-wrap>.elementor-widget-wrap>.elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
        margin-bottom: 0px;
      }

      .elementor-128245 .elementor-element.elementor-element-49e38d9:not(.elementor-motion-effects-element-type-background)>.elementor-column-wrap,
      .elementor-128245 .elementor-element.elementor-element-49e38d9>.elementor-column-wrap>.elementor-motion-effects-container>.elementor-motion-effects-layer {
        background-color: #5E5E5E;
      }

      .elementor-128245 .elementor-element.elementor-element-49e38d9>.elementor-element-populated {
        transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
      }

      .elementor-128245 .elementor-element.elementor-element-49e38d9>.elementor-element-populated>.elementor-background-overlay {
        transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
      }

      .elementor-128245 .elementor-element.elementor-element-862a1a3 {
        text-align: center;
        width: auto;
        max-width: auto;
      }

      .elementor-128245 .elementor-element.elementor-element-862a1a3 .elementor-heading-title {
        color: #FFFFFF;
        font-family: "Power", Sans-serif;
        font-size: 40px;
        font-weight: normal;
        font-style: normal;
      }

      .elementor-128245 .elementor-element.elementor-element-862a1a3>.elementor-widget-container {
        margin: 8em -8em 8em -8em;
        padding: 0px 0px 0px 0px;
        --e-transform-rotateZ: 90deg;
      }

      .elementor-128245 .elementor-element.elementor-element-7fa2887 {
        overflow: hidden;
        transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
      }

      .elementor-128245 .elementor-element.elementor-element-7fa2887:not(.elementor-motion-effects-element-type-background),
      .elementor-128245 .elementor-element.elementor-element-7fa2887>.elementor-motion-effects-container>.elementor-motion-effects-layer {
        background-color: #F8F8F8;
      }

      .elementor-128245 .elementor-element.elementor-element-7fa2887>.elementor-background-overlay {
        transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
      }

      .elementor-128245 .elementor-element.elementor-element-d84e2f0>.elementor-column-wrap>.elementor-widget-wrap>.elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
        margin-bottom: 5px;
      }

      .elementor-128245 .elementor-element.elementor-element-d84e2f0>.elementor-element-populated>.elementor-widget-wrap {
        padding: 5em 1.5em 7em 1.5em;
      }

      .elementor-128245 .elementor-element.elementor-element-cf26391 {
        text-align: right;
      }

      .elementor-128245 .elementor-element.elementor-element-cf26391 .elementor-heading-title {
        color: #5E5E5E;
        font-family: "Power", Sans-serif;
        font-size: 40px;
        font-weight: normal;
        font-style: italic;
      }

      .elementor-128245 .elementor-element.elementor-element-cf26391>.elementor-widget-container {
        margin: 0em 0em 0em 0em;
      }

      .elementor-128245 .elementor-element.elementor-element-b4d998c {
        --divider-border-style: solid;
        --divider-color: #5E5E5E;
        --divider-border-width: 1px;
      }

      .elementor-128245 .elementor-element.elementor-element-b4d998c .elementor-divider-separator {
        width: 55%;
        margin: 0 auto;
        margin-left: 0;
      }

      .elementor-128245 .elementor-element.elementor-element-b4d998c .elementor-divider {
        text-align: left;
      }

      .elementor-128245 .elementor-element.elementor-element-b4d998c>.elementor-widget-container {
        margin: -1em 0em 0em -3em;
      }

      .elementor-128245 .elementor-element.elementor-element-68857af {
        text-align: right;
      }

      .elementor-128245 .elementor-element.elementor-element-68857af .elementor-heading-title {
        color: #5E5E5E;
        font-family: "Creattion", Sans-serif;
        font-size: 50px;
        font-weight: normal;
        font-style: italic;
      }

      .elementor-128245 .elementor-element.elementor-element-68857af>.elementor-widget-container {
        margin: -1em 1em 2em 0em;
      }

      .elementor-128245 .elementor-element.elementor-element-208dc63 {
        --image-border-radius: 0px;
        --image-transition-duration: 800ms;
        --overlay-transition-duration: 800ms;
        --content-text-align: center;
        --content-padding: 20px;
        --content-transition-duration: 800ms;
        --content-transition-delay: 800ms;
      }

      .elementor-128245 .elementor-element.elementor-element-208dc63 .e-gallery-item:hover .elementor-gallery-item__overlay {
        background-color: rgba(0, 0, 0, 0.5);
      }

      .elementor-128245 .elementor-element.elementor-element-208dc63>.elementor-widget-container {
        margin: 0px 0px 0px 0px;
      }

      .elementor-128245 .elementor-element.elementor-element-69c9018:not(.elementor-motion-effects-element-type-background),
      .elementor-128245 .elementor-element.elementor-element-69c9018>.elementor-motion-effects-container>.elementor-motion-effects-layer {
        background-color: transparent;
        background-image: linear-gradient(180deg, #F8F8F8 0%, #D8D8D8 100%);
      }

      .elementor-128245 .elementor-element.elementor-element-69c9018 {
        transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
      }

      .elementor-128245 .elementor-element.elementor-element-69c9018>.elementor-background-overlay {
        transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
      }

      .elementor-bc-flex-widget .elementor-128245 .elementor-element.elementor-element-de87ad8.elementor-column .elementor-column-wrap {
        align-items: center;
      }

      .elementor-128245 .elementor-element.elementor-element-de87ad8.elementor-column.elementor-element[data-element_type="column"]>.elementor-column-wrap.elementor-element-populated>.elementor-widget-wrap {
        align-content: center;
        align-items: center;
      }

      .elementor-128245 .elementor-element.elementor-element-de87ad8.elementor-column>.elementor-column-wrap>.elementor-widget-wrap {
        justify-content: center;
      }

      .elementor-128245 .elementor-element.elementor-element-de87ad8>.elementor-column-wrap>.elementor-widget-wrap>.elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
        margin-bottom: 0px;
      }

      .elementor-128245 .elementor-element.elementor-element-de87ad8>.elementor-element-populated>.elementor-widget-wrap {
        padding: 02em 2em 6em 2em;
      }

      .elementor-128245 .elementor-element.elementor-element-aeb2e16 {
        text-align: center;
      }

      .elementor-128245 .elementor-element.elementor-element-aeb2e16 .elementor-heading-title {
        color: #5E5E5E;
        font-family: "Power", Sans-serif;
        font-size: 40px;
        font-weight: normal;
        font-style: italic;
      }

      .elementor-128245 .elementor-element.elementor-element-aeb2e16>.elementor-widget-container {
        margin: 0em 0em 2em 0em;
      }

      .elementor-128245 .elementor-element.elementor-element-141ff40 {
        text-align: center;
      }

      .elementor-128245 .elementor-element.elementor-element-141ff40 .elementor-heading-title {
        color: #5E5E5E;
        font-family: "Poppins", Sans-serif;
        font-size: 13px;
        font-weight: normal;
        font-style: normal;
        line-height: 1.5em;
      }

      .elementor-128245 .elementor-element.elementor-element-141ff40>.elementor-widget-container {
        padding: 0em 0em 2em 0em;
      }

      .elementor-128245 .elementor-element.elementor-element-1db3065 .elementor-button {
        font-family: "Poppins", Sans-serif;
        font-size: 12px;
        font-weight: normal;
        letter-spacing: 2px;
        fill: #FFFFFF;
        color: #FFFFFF;
        background-color: #5E5E5E;
        border-radius: 0px 0px 0px 0px;
        padding: 8px 20px 8px 20px;
      }

      .elementor-128245 .elementor-element.elementor-element-1db3065 .elementor-button:hover,
      .elementor-128245 .elementor-element.elementor-element-1db3065 .elementor-button:focus {
        background-color: #434343;
      }

      .elementor-128245 .elementor-element.elementor-element-1db3065 {
        z-index: 999;
      }

      .elementor-128245 .elementor-element.elementor-element-4b39e42 {
        margin-top: 2em;
        margin-bottom: 0em;
      }

      .elementor-128245 .elementor-element.elementor-element-0fd6034>.elementor-element-populated {
        margin: 0px 0px 0px 0px;
        --e-column-margin-right: 0px;
        --e-column-margin-left: 0px;
      }

      .elementor-128245 .elementor-element.elementor-element-0fd6034>.elementor-element-populated>.elementor-widget-wrap {
        padding: 0px 0px 0px 0px;
      }

      .elementor-128245 .elementor-element.elementor-element-055ee47:not(.elementor-motion-effects-element-type-background),
      .elementor-128245 .elementor-element.elementor-element-055ee47>.elementor-motion-effects-container>.elementor-motion-effects-layer {
        background-color: transparent;
        background-image: linear-gradient(50deg, #FFFFFF 0%, #D8D8D8 100%);
      }

      .elementor-128245 .elementor-element.elementor-element-055ee47 {
        transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
        margin-top: 0em;
        margin-bottom: 1em;
      }

      .elementor-128245 .elementor-element.elementor-element-055ee47>.elementor-background-overlay {
        transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
      }

      .elementor-bc-flex-widget .elementor-128245 .elementor-element.elementor-element-ff8e7fd.elementor-column .elementor-column-wrap {
        align-items: center;
      }

      .elementor-128245 .elementor-element.elementor-element-ff8e7fd.elementor-column.elementor-element[data-element_type="column"]>.elementor-column-wrap.elementor-element-populated>.elementor-widget-wrap {
        align-content: center;
        align-items: center;
      }

      .elementor-128245 .elementor-element.elementor-element-ff8e7fd.elementor-column>.elementor-column-wrap>.elementor-widget-wrap {
        justify-content: center;
      }

      .elementor-128245 .elementor-element.elementor-element-ff8e7fd>.elementor-column-wrap>.elementor-widget-wrap>.elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
        margin-bottom: 5px;
      }

      .elementor-128245 .elementor-element.elementor-element-ff8e7fd>.elementor-element-populated>.elementor-widget-wrap {
        padding: 1em 1em 1em 1em;
      }

      .elementor-128245 .elementor-element.elementor-element-897e4f3 {
        text-align: center;
      }

      .elementor-128245 .elementor-element.elementor-element-897e4f3 .elementor-heading-title {
        color: #5E5E5E;
        font-family: "Poppins", Sans-serif;
        font-size: 20px;
        font-weight: 600;
        text-transform: uppercase;
        font-style: normal;
        letter-spacing: 1.5px;
      }

      .elementor-128245 .elementor-element.elementor-element-897e4f3>.elementor-widget-container {
        margin: 0em 0em 1.5em 0em;
      }

      .elementor-128245 .elementor-element.elementor-element-c6ce70c {
        text-align: center;
      }

      .elementor-128245 .elementor-element.elementor-element-c6ce70c .elementor-heading-title {
        color: #5E5E5E;
        font-family: "Poppins", Sans-serif;
        font-size: 13px;
        font-weight: 600;
        font-style: normal;
        line-height: 1.5em;
        letter-spacing: 1.5px;
      }

      .elementor-128245 .elementor-element.elementor-element-c6ce70c>.elementor-widget-container {
        padding: 0em 0em 0em 0em;
      }

      .elementor-128245 .elementor-element.elementor-element-ca01162 {
        text-align: center;
      }

      .elementor-128245 .elementor-element.elementor-element-ca01162 .elementor-heading-title {
        color: #5E5E5E;
        font-family: "Poppins", Sans-serif;
        font-size: 12px;
        font-weight: normal;
        font-style: normal;
        line-height: 1.5em;
        letter-spacing: 1px;
      }

      .elementor-128245 .elementor-element.elementor-element-ca01162>.elementor-widget-container {
        padding: 0em 0em 0em 0em;
      }

      .elementor-128245 .elementor-element.elementor-element-1ca71e5 .elementor-button .elementor-align-icon-right {
        margin-left: 5px;
      }

      .elementor-128245 .elementor-element.elementor-element-1ca71e5 .elementor-button .elementor-align-icon-left {
        margin-right: 5px;
      }

      .elementor-128245 .elementor-element.elementor-element-1ca71e5 a.elementor-button,
      .elementor-128245 .elementor-element.elementor-element-1ca71e5 .elementor-button {
        font-family: "Poppins", Sans-serif;
        font-size: 12px;
        font-weight: 400;
        letter-spacing: 1.5px;
        background-color: #5E5E5E;
        border-radius: 0px 0px 0px 0px;
        padding: 8px 10px 8px 10px;
      }

      .elementor-128245 .elementor-element.elementor-element-1ca71e5 a.elementor-button:hover,
      .elementor-128245 .elementor-element.elementor-element-1ca71e5 .elementor-button:hover,
      .elementor-128245 .elementor-element.elementor-element-1ca71e5 a.elementor-button:focus,
      .elementor-128245 .elementor-element.elementor-element-1ca71e5 .elementor-button:focus {
        background-color: #434343;
      }

      .elementor-128245 .elementor-element.elementor-element-1ca71e5>.elementor-widget-container {
        margin: 1em 0em 0em 0em;
      }

      .elementor-128245 .elementor-element.elementor-element-1ca71e5 {
        width: auto;
        max-width: auto;
      }

      .elementor-128245 .elementor-element.elementor-element-7cc92ba:not(.elementor-motion-effects-element-type-background),
      .elementor-128245 .elementor-element.elementor-element-7cc92ba>.elementor-motion-effects-container>.elementor-motion-effects-layer {
        background-color: transparent;
        background-image: linear-gradient(50deg, #FFFFFF 0%, #D8D8D8 100%);
      }

      .elementor-128245 .elementor-element.elementor-element-7cc92ba {
        transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
        margin-top: 0em;
        margin-bottom: 1em;
      }

      .elementor-128245 .elementor-element.elementor-element-7cc92ba>.elementor-background-overlay {
        transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
      }

      .elementor-bc-flex-widget .elementor-128245 .elementor-element.elementor-element-937a061.elementor-column .elementor-column-wrap {
        align-items: center;
      }

      .elementor-128245 .elementor-element.elementor-element-937a061.elementor-column.elementor-element[data-element_type="column"]>.elementor-column-wrap.elementor-element-populated>.elementor-widget-wrap {
        align-content: center;
        align-items: center;
      }

      .elementor-128245 .elementor-element.elementor-element-937a061.elementor-column>.elementor-column-wrap>.elementor-widget-wrap {
        justify-content: center;
      }

      .elementor-128245 .elementor-element.elementor-element-937a061>.elementor-column-wrap>.elementor-widget-wrap>.elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
        margin-bottom: 5px;
      }

      .elementor-128245 .elementor-element.elementor-element-937a061>.elementor-element-populated>.elementor-widget-wrap {
        padding: 1em 1em 1em 1em;
      }

      .elementor-128245 .elementor-element.elementor-element-c268313 {
        text-align: center;
      }

      .elementor-128245 .elementor-element.elementor-element-c268313 .elementor-heading-title {
        color: #5E5E5E;
        font-family: "Poppins", Sans-serif;
        font-size: 20px;
        font-weight: 600;
        text-transform: uppercase;
        font-style: normal;
        letter-spacing: 1.5px;
      }

      .elementor-128245 .elementor-element.elementor-element-c268313>.elementor-widget-container {
        margin: 0em 0em 1.5em 0em;
      }

      .elementor-128245 .elementor-element.elementor-element-d216bd0 {
        text-align: center;
      }

      .elementor-128245 .elementor-element.elementor-element-d216bd0 .elementor-heading-title {
        color: #5E5E5E;
        font-family: "Poppins", Sans-serif;
        font-size: 13px;
        font-weight: 600;
        font-style: normal;
        line-height: 1.5em;
        letter-spacing: 1.5px;
      }

      .elementor-128245 .elementor-element.elementor-element-d216bd0>.elementor-widget-container {
        padding: 0em 0em 0em 0em;
      }

      .elementor-128245 .elementor-element.elementor-element-c46f73a {
        text-align: center;
      }

      .elementor-128245 .elementor-element.elementor-element-c46f73a .elementor-heading-title {
        color: #5E5E5E;
        font-family: "Poppins", Sans-serif;
        font-size: 12px;
        font-weight: normal;
        font-style: normal;
        line-height: 1.5em;
        letter-spacing: 1px;
      }

      .elementor-128245 .elementor-element.elementor-element-c46f73a>.elementor-widget-container {
        padding: 0em 0em 0em 0em;
      }

      .elementor-128245 .elementor-element.elementor-element-009e162 .elementor-button .elementor-align-icon-right {
        margin-left: 5px;
      }

      .elementor-128245 .elementor-element.elementor-element-009e162 .elementor-button .elementor-align-icon-left {
        margin-right: 5px;
      }

      .elementor-128245 .elementor-element.elementor-element-009e162 a.elementor-button,
      .elementor-128245 .elementor-element.elementor-element-009e162 .elementor-button {
        font-family: "Poppins", Sans-serif;
        font-size: 12px;
        font-weight: 400;
        letter-spacing: 1.5px;
        background-color: #5E5E5E;
        border-radius: 0px 0px 0px 0px;
        padding: 8px 10px 8px 10px;
      }

      .elementor-128245 .elementor-element.elementor-element-009e162 a.elementor-button:hover,
      .elementor-128245 .elementor-element.elementor-element-009e162 .elementor-button:hover,
      .elementor-128245 .elementor-element.elementor-element-009e162 a.elementor-button:focus,
      .elementor-128245 .elementor-element.elementor-element-009e162 .elementor-button:focus {
        background-color: #434343;
      }

      .elementor-128245 .elementor-element.elementor-element-009e162>.elementor-widget-container {
        margin: 1em 0em 0em 0em;
      }

      .elementor-128245 .elementor-element.elementor-element-009e162 {
        width: auto;
        max-width: auto;
      }

      .elementor-128245 .elementor-element.elementor-element-dd4de0e:not(.elementor-motion-effects-element-type-background),
      .elementor-128245 .elementor-element.elementor-element-dd4de0e>.elementor-motion-effects-container>.elementor-motion-effects-layer {
        background-color: transparent;
        background-image: linear-gradient(50deg, #FFFFFF 0%, #D8D8D8 100%);
      }

      .elementor-128245 .elementor-element.elementor-element-dd4de0e {
        transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
        margin-top: 0em;
        margin-bottom: 1em;
      }

      .elementor-128245 .elementor-element.elementor-element-dd4de0e>.elementor-background-overlay {
        transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
      }

      .elementor-bc-flex-widget .elementor-128245 .elementor-element.elementor-element-5b01b78.elementor-column .elementor-column-wrap {
        align-items: center;
      }

      .elementor-128245 .elementor-element.elementor-element-5b01b78.elementor-column.elementor-element[data-element_type="column"]>.elementor-column-wrap.elementor-element-populated>.elementor-widget-wrap {
        align-content: center;
        align-items: center;
      }

      .elementor-128245 .elementor-element.elementor-element-5b01b78.elementor-column>.elementor-column-wrap>.elementor-widget-wrap {
        justify-content: center;
      }

      .elementor-128245 .elementor-element.elementor-element-5b01b78>.elementor-column-wrap>.elementor-widget-wrap>.elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
        margin-bottom: 5px;
      }

      .elementor-128245 .elementor-element.elementor-element-5b01b78>.elementor-element-populated>.elementor-widget-wrap {
        padding: 1em 1em 1em 1em;
      }

      .elementor-128245 .elementor-element.elementor-element-c086915 {
        text-align: center;
      }

      .elementor-128245 .elementor-element.elementor-element-c086915 .elementor-heading-title {
        color: #5E5E5E;
        font-family: "Poppins", Sans-serif;
        font-size: 20px;
        font-weight: 600;
        text-transform: uppercase;
        font-style: normal;
        letter-spacing: 1.5px;
      }

      .elementor-128245 .elementor-element.elementor-element-c086915>.elementor-widget-container {
        margin: 0em 0em 1.5em 0em;
      }

      .elementor-128245 .elementor-element.elementor-element-09b92b2 {
        text-align: center;
      }

      .elementor-128245 .elementor-element.elementor-element-09b92b2 .elementor-heading-title {
        color: #5E5E5E;
        font-family: "Poppins", Sans-serif;
        font-size: 13px;
        font-weight: 600;
        font-style: normal;
        line-height: 1.5em;
        letter-spacing: 1.5px;
      }

      .elementor-128245 .elementor-element.elementor-element-09b92b2>.elementor-widget-container {
        margin: 0em 0em 1em 0em;
        padding: 0em 0em 0em 0em;
      }

      .elementor-128245 .elementor-element.elementor-element-bca796e {
        text-align: center;
      }

      .elementor-128245 .elementor-element.elementor-element-bca796e .elementor-heading-title {
        color: #5E5E5E;
        font-family: "Poppins", Sans-serif;
        font-size: 12px;
        font-weight: normal;
        font-style: normal;
        line-height: 1.5em;
        letter-spacing: 1px;
      }

      .elementor-128245 .elementor-element.elementor-element-bca796e>.elementor-widget-container {
        padding: 0em 0em 0em 0em;
      }

      .elementor-128245 .elementor-element.elementor-element-8813067 .elementor-button .elementor-align-icon-right {
        margin-left: 5px;
      }

      .elementor-128245 .elementor-element.elementor-element-8813067 .elementor-button .elementor-align-icon-left {
        margin-right: 5px;
      }

      .elementor-128245 .elementor-element.elementor-element-8813067 a.elementor-button,
      .elementor-128245 .elementor-element.elementor-element-8813067 .elementor-button {
        font-family: "Poppins", Sans-serif;
        font-size: 12px;
        font-weight: 400;
        letter-spacing: 1.5px;
        background-color: #5E5E5E;
        border-radius: 0px 0px 0px 0px;
        padding: 8px 10px 8px 10px;
      }

      .elementor-128245 .elementor-element.elementor-element-8813067 a.elementor-button:hover,
      .elementor-128245 .elementor-element.elementor-element-8813067 .elementor-button:hover,
      .elementor-128245 .elementor-element.elementor-element-8813067 a.elementor-button:focus,
      .elementor-128245 .elementor-element.elementor-element-8813067 .elementor-button:focus {
        background-color: #434343;
      }

      .elementor-128245 .elementor-element.elementor-element-8813067>.elementor-widget-container {
        margin: 1em 0em 0em 0em;
      }

      .elementor-128245 .elementor-element.elementor-element-8813067 {
        width: auto;
        max-width: auto;
      }

      .elementor-128245 .elementor-element.elementor-element-7bd8417>.elementor-container {
        min-height: 175px;
      }

      .elementor-128245 .elementor-element.elementor-element-7bd8417:not(.elementor-motion-effects-element-type-background),
      .elementor-128245 .elementor-element.elementor-element-7bd8417>.elementor-motion-effects-container>.elementor-motion-effects-layer {
        background-image: url("wp-content/uploads/2023/01/left-2.jpg");
        background-position: center center;
        background-size: cover;
      }

      .elementor-128245 .elementor-element.elementor-element-7bd8417>.elementor-background-overlay {
        background-color: #5E5E5E;
        opacity: 0.55;
        transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
      }

      .elementor-128245 .elementor-element.elementor-element-7bd8417 {
        transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
      }

      .elementor-bc-flex-widget .elementor-128245 .elementor-element.elementor-element-aecd59c.elementor-column .elementor-column-wrap {
        align-items: center;
      }

      .elementor-128245 .elementor-element.elementor-element-aecd59c.elementor-column.elementor-element[data-element_type="column"]>.elementor-column-wrap.elementor-element-populated>.elementor-widget-wrap {
        align-content: center;
        align-items: center;
      }

      .elementor-128245 .elementor-element.elementor-element-aecd59c>.elementor-column-wrap>.elementor-widget-wrap>.elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
        margin-bottom: 0px;
      }

      .elementor-128245 .elementor-element.elementor-element-aecd59c>.elementor-element-populated>.elementor-widget-wrap {
        padding: 0px 0px 0px 0px;
      }

      .elementor-128245 .elementor-element.elementor-element-6d805ac {
        text-align: center;
      }

      .elementor-128245 .elementor-element.elementor-element-6d805ac .elementor-heading-title {
        color: #FFFFFF;
        font-family: "Power", Sans-serif;
        font-size: 40px;
        font-weight: normal;
        font-style: italic;
      }

      .elementor-128245 .elementor-element.elementor-element-6d805ac>.elementor-widget-container {
        margin: 0em 2em 0em 0em;
      }

      .elementor-128245 .elementor-element.elementor-element-48307fa {
        text-align: center;
      }

      .elementor-128245 .elementor-element.elementor-element-48307fa .elementor-heading-title {
        color: #FFFFFF;
        font-family: "Creattion", Sans-serif;
        font-size: 35px;
        font-weight: normal;
        font-style: italic;
      }

      .elementor-128245 .elementor-element.elementor-element-48307fa>.elementor-widget-container {
        margin: -1em 0em 0em 2em;
      }

      .elementor-128245 .elementor-element.elementor-element-ea4c586:not(.elementor-motion-effects-element-type-background),
      .elementor-128245 .elementor-element.elementor-element-ea4c586>.elementor-motion-effects-container>.elementor-motion-effects-layer {
        background-color: #FFFFFF;
      }

      .elementor-128245 .elementor-element.elementor-element-ea4c586 {
        transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
      }

      .elementor-128245 .elementor-element.elementor-element-ea4c586>.elementor-background-overlay {
        transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
      }

      .elementor-128245 .elementor-element.elementor-element-eff1217>.elementor-column-wrap>.elementor-widget-wrap>.elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
        margin-bottom: 5px;
      }

      .elementor-128245 .elementor-element.elementor-element-eff1217>.elementor-element-populated>.elementor-widget-wrap {
        padding: 2em 1em 8em 1em;
      }

      .elementor-128245 .elementor-element.elementor-element-2c2adda img {
        opacity: 0.1;
      }

      .elementor-128245 .elementor-element.elementor-element-2c2adda {
        z-index: 0;
        width: auto;
        max-width: auto;
        top: 0px;
      }

      body:not(.rtl) .elementor-128245 .elementor-element.elementor-element-2c2adda {
        left: 0px;
      }

      body.rtl .elementor-128245 .elementor-element.elementor-element-2c2adda {
        right: 0px;
      }

      .elementor-128245 .elementor-element.elementor-element-ee6f05c>.elementor-widget-container {
        margin: 0em 0em 6em 0em;
      }

      .elementor-128245 .elementor-element.elementor-element-9e4f3a3 {
        text-align: center;
      }

      .elementor-128245 .elementor-element.elementor-element-9e4f3a3 .elementor-heading-title {
        color: #5E5E5E;
        font-family: "Poppins", Sans-serif;
        font-size: 15px;
        font-weight: 600;
        text-transform: uppercase;
        font-style: normal;
        letter-spacing: 1.5px;
      }

      .elementor-128245 .elementor-element.elementor-element-9e4f3a3>.elementor-widget-container {
        margin: 0em 0em 2em 0em;
      }

      .elementor-128245 .elementor-element.elementor-element-1cf38ae {
        text-align: center;
      }

      .elementor-128245 .elementor-element.elementor-element-1cf38ae .elementor-heading-title {
        color: #5E5E5E;
        font-family: "Poppins", Sans-serif;
        font-size: 13px;
        font-weight: normal;
        font-style: normal;
        line-height: 1.5em;
      }

      .elementor-128245 .elementor-element.elementor-element-1cf38ae>.elementor-widget-container {
        padding: 0em 0em 0em 0em;
      }

      .elementor-128245 .elementor-element.elementor-element-ca73dc9 {
        margin-top: 1em;
        margin-bottom: 0em;
      }

      .elementor-128245 .elementor-element.elementor-element-1e3fbf7>.elementor-column-wrap>.elementor-widget-wrap>.elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
        margin-bottom: 5px;
      }

      .elementor-128245 .elementor-element.elementor-element-688989c .elementor-icon-wrapper {
        text-align: center;
      }

      .elementor-128245 .elementor-element.elementor-element-688989c.elementor-view-stacked .elementor-icon {
        background-color: #EDEDED;
      }

      .elementor-128245 .elementor-element.elementor-element-688989c.elementor-view-framed .elementor-icon,
      .elementor-128245 .elementor-element.elementor-element-688989c.elementor-view-default .elementor-icon {
        color: #EDEDED;
        border-color: #EDEDED;
      }

      .elementor-128245 .elementor-element.elementor-element-688989c.elementor-view-framed .elementor-icon,
      .elementor-128245 .elementor-element.elementor-element-688989c.elementor-view-default .elementor-icon svg {
        fill: #EDEDED;
      }

      .elementor-128245 .elementor-element.elementor-element-688989c .elementor-icon {
        font-size: 35px;
        padding: 12px;
      }

      .elementor-128245 .elementor-element.elementor-element-688989c .elementor-icon i,
      .elementor-128245 .elementor-element.elementor-element-688989c .elementor-icon svg {
        transform: rotate(0deg);
      }

      .elementor-128245 .elementor-element.elementor-element-bf769a1 {
        text-align: center;
      }

      .elementor-128245 .elementor-element.elementor-element-bf769a1 .elementor-heading-title {
        color: #5E5E5E;
        font-family: "Poppins", Sans-serif;
        font-size: 11px;
        font-weight: normal;
        font-style: italic;
        line-height: 1.5em;
      }

      .elementor-128245 .elementor-element.elementor-element-bf769a1>.elementor-widget-container {
        padding: 0em 0em 0em 0em;
      }

      .elementor-128245 .elementor-element.elementor-element-02f10ac>.elementor-column-wrap>.elementor-widget-wrap>.elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
        margin-bottom: 4px;
      }

      .elementor-128245 .elementor-element.elementor-element-cd9aff3 .elementor-icon-wrapper {
        text-align: center;
      }

      .elementor-128245 .elementor-element.elementor-element-cd9aff3.elementor-view-stacked .elementor-icon {
        background-color: #EDEDED;
      }

      .elementor-128245 .elementor-element.elementor-element-cd9aff3.elementor-view-framed .elementor-icon,
      .elementor-128245 .elementor-element.elementor-element-cd9aff3.elementor-view-default .elementor-icon {
        color: #EDEDED;
        border-color: #EDEDED;
      }

      .elementor-128245 .elementor-element.elementor-element-cd9aff3.elementor-view-framed .elementor-icon,
      .elementor-128245 .elementor-element.elementor-element-cd9aff3.elementor-view-default .elementor-icon svg {
        fill: #EDEDED;
      }

      .elementor-128245 .elementor-element.elementor-element-cd9aff3 .elementor-icon {
        font-size: 35px;
        padding: 12px;
      }

      .elementor-128245 .elementor-element.elementor-element-cd9aff3 .elementor-icon i,
      .elementor-128245 .elementor-element.elementor-element-cd9aff3 .elementor-icon svg {
        transform: rotate(0deg);
      }

      .elementor-128245 .elementor-element.elementor-element-ec6e767 {
        text-align: center;
      }

      .elementor-128245 .elementor-element.elementor-element-ec6e767 .elementor-heading-title {
        color: #5E5E5E;
        font-family: "Poppins", Sans-serif;
        font-size: 11px;
        font-weight: normal;
        font-style: italic;
        line-height: 1.5em;
      }

      .elementor-128245 .elementor-element.elementor-element-ec6e767>.elementor-widget-container {
        padding: 0em 0em 0em 0em;
      }

      .elementor-128245 .elementor-element.elementor-element-96a2fe0>.elementor-column-wrap>.elementor-widget-wrap>.elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
        margin-bottom: 4px;
      }

      .elementor-128245 .elementor-element.elementor-element-00cd332 .elementor-icon-wrapper {
        text-align: center;
      }

      .elementor-128245 .elementor-element.elementor-element-00cd332.elementor-view-stacked .elementor-icon {
        background-color: #EDEDED;
      }

      .elementor-128245 .elementor-element.elementor-element-00cd332.elementor-view-framed .elementor-icon,
      .elementor-128245 .elementor-element.elementor-element-00cd332.elementor-view-default .elementor-icon {
        color: #EDEDED;
        border-color: #EDEDED;
      }

      .elementor-128245 .elementor-element.elementor-element-00cd332.elementor-view-framed .elementor-icon,
      .elementor-128245 .elementor-element.elementor-element-00cd332.elementor-view-default .elementor-icon svg {
        fill: #EDEDED;
      }

      .elementor-128245 .elementor-element.elementor-element-00cd332 .elementor-icon {
        font-size: 35px;
        padding: 12px;
      }

      .elementor-128245 .elementor-element.elementor-element-00cd332 .elementor-icon i,
      .elementor-128245 .elementor-element.elementor-element-00cd332 .elementor-icon svg {
        transform: rotate(0deg);
      }

      .elementor-128245 .elementor-element.elementor-element-f475063 {
        text-align: center;
      }

      .elementor-128245 .elementor-element.elementor-element-f475063 .elementor-heading-title {
        color: #5E5E5E;
        font-family: "Poppins", Sans-serif;
        font-size: 11px;
        font-weight: normal;
        font-style: italic;
        line-height: 1.5em;
      }

      .elementor-128245 .elementor-element.elementor-element-f475063>.elementor-widget-container {
        padding: 0em 0em 0em 0em;
      }

      .elementor-128245 .elementor-element.elementor-element-8ab1d9a>.elementor-container {
        min-height: 600px;
      }

      .elementor-128245 .elementor-element.elementor-element-8ab1d9a .elementor-background-slideshow__slide__image {
        background-size: cover;
        background-position: center center;
      }

      .elementor-128245 .elementor-element.elementor-element-8ab1d9a>.elementor-background-overlay {
        background-color: transparent;
        background-image: linear-gradient(180deg, #FFFFFF 0%, #00000050 65%);
        opacity: 1;
        transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
      }

      .elementor-128245 .elementor-element.elementor-element-8ab1d9a {
        transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
      }

      .elementor-bc-flex-widget .elementor-128245 .elementor-element.elementor-element-6a60023.elementor-column .elementor-column-wrap {
        align-items: center;
      }

      .elementor-128245 .elementor-element.elementor-element-6a60023.elementor-column.elementor-element[data-element_type="column"]>.elementor-column-wrap.elementor-element-populated>.elementor-widget-wrap {
        align-content: center;
        align-items: center;
      }

      .elementor-128245 .elementor-element.elementor-element-6a60023.elementor-column>.elementor-column-wrap>.elementor-widget-wrap {
        justify-content: center;
      }

      .elementor-128245 .elementor-element.elementor-element-6a60023>.elementor-column-wrap>.elementor-widget-wrap>.elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
        margin-bottom: 5px;
      }

      .elementor-128245 .elementor-element.elementor-element-6a60023>.elementor-element-populated>.elementor-widget-wrap {
        padding: 0em 2em 0em 2em;
      }

      .elementor-128245 .elementor-element.elementor-element-b73e346 {
        text-align: center;
      }

      .elementor-128245 .elementor-element.elementor-element-b73e346 .elementor-heading-title {
        color: #FFFFFF;
        font-family: "Creattion", Sans-serif;
        font-size: 50px;
        font-weight: normal;
        font-style: italic;
      }

      .elementor-128245 .elementor-element.elementor-element-b73e346>.elementor-widget-container {
        margin: 0em 0em 2em 0em;
      }

      .elementor-128245 .elementor-element.elementor-element-7b50b4d {
        text-align: center;
      }

      .elementor-128245 .elementor-element.elementor-element-7b50b4d .elementor-heading-title {
        color: #FFFFFF;
        font-family: "Poppins", Sans-serif;
        font-size: 13px;
        font-weight: normal;
        font-style: normal;
        line-height: 2em;
      }

      .elementor-128245 .elementor-element.elementor-element-7b50b4d>.elementor-widget-container {
        margin: 0em 0em 4em 0em;
      }

      .elementor-128245 .elementor-element.elementor-element-066c6bf {
        text-align: center;
      }

      .elementor-128245 .elementor-element.elementor-element-066c6bf .elementor-heading-title {
        color: #FFFFFF;
        font-family: "Poppins", Sans-serif;
        font-size: 13px;
        font-weight: 500;
        font-style: normal;
        letter-spacing: 1px;
      }

      .elementor-128245 .elementor-element.elementor-element-066c6bf>.elementor-widget-container {
        margin: 0em 0em 1em 0em;
        padding: 0em 0em 0em 0em;
      }

      .elementor-128245 .elementor-element.elementor-element-6fb722a {
        text-align: center;
      }

      .elementor-128245 .elementor-element.elementor-element-6fb722a .elementor-heading-title {
        color: #FFFFFF;
        font-family: "Analogue", Sans-serif;
        font-size: 26px;
        font-weight: normal;
        font-style: italic;
      }

      .elementor-128245 .elementor-element.elementor-element-6fb722a>.elementor-widget-container {
        margin: 0em 0em 0em 0em;
      }

      .elementor-128245 .elementor-element.elementor-element-f3e09e2>.elementor-container {
        min-height: 200px;
      }

      .elementor-128245 .elementor-element.elementor-element-f3e09e2:not(.elementor-motion-effects-element-type-background),
      .elementor-128245 .elementor-element.elementor-element-f3e09e2>.elementor-motion-effects-container>.elementor-motion-effects-layer {
        background-color: #EDEDED;
      }

      .elementor-128245 .elementor-element.elementor-element-f3e09e2 {
        transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
      }

      .elementor-128245 .elementor-element.elementor-element-f3e09e2>.elementor-background-overlay {
        transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
      }

      .elementor-bc-flex-widget .elementor-128245 .elementor-element.elementor-element-23d8bb0.elementor-column .elementor-column-wrap {
        align-items: center;
      }

      .elementor-128245 .elementor-element.elementor-element-23d8bb0.elementor-column.elementor-element[data-element_type="column"]>.elementor-column-wrap.elementor-element-populated>.elementor-widget-wrap {
        align-content: center;
        align-items: center;
      }

      .elementor-128245 .elementor-element.elementor-element-23d8bb0.elementor-column>.elementor-column-wrap>.elementor-widget-wrap {
        justify-content: center;
      }

      .elementor-128245 .elementor-element.elementor-element-23d8bb0>.elementor-column-wrap>.elementor-widget-wrap>.elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
        margin-bottom: 0px;
      }

      .elementor-128245 .elementor-element.elementor-element-23d8bb0>.elementor-element-populated>.elementor-widget-wrap {
        padding: 5em 0em 5em 0em;
      }

      .elementor-128245 .elementor-element.elementor-element-cb19f87 {
        margin-top: 0em;
        margin-bottom: 1em;
      }

      .elementor-bc-flex-widget .elementor-128245 .elementor-element.elementor-element-6bbb1dc.elementor-column .elementor-column-wrap {
        align-items: center;
      }

      .elementor-128245 .elementor-element.elementor-element-6bbb1dc.elementor-column.elementor-element[data-element_type="column"]>.elementor-column-wrap.elementor-element-populated>.elementor-widget-wrap {
        align-content: center;
        align-items: center;
      }

      .elementor-128245 .elementor-element.elementor-element-6bbb1dc.elementor-column>.elementor-column-wrap>.elementor-widget-wrap {
        justify-content: center;
      }

      .elementor-128245 .elementor-element.elementor-element-6bbb1dc>.elementor-element-populated>.elementor-widget-wrap {
        padding: 0px 0px 0px 0px;
      }

      .elementor-128245 .elementor-element.elementor-element-0522a5f .elementor-icon-wrapper {
        text-align: center;
      }

      .elementor-128245 .elementor-element.elementor-element-0522a5f.elementor-view-stacked .elementor-icon {
        background-color: #EDEDED;
        color: #5E5E5E;
      }

      .elementor-128245 .elementor-element.elementor-element-0522a5f.elementor-view-framed .elementor-icon,
      .elementor-128245 .elementor-element.elementor-element-0522a5f.elementor-view-default .elementor-icon {
        color: #EDEDED;
        border-color: #EDEDED;
      }

      .elementor-128245 .elementor-element.elementor-element-0522a5f.elementor-view-framed .elementor-icon,
      .elementor-128245 .elementor-element.elementor-element-0522a5f.elementor-view-default .elementor-icon svg {
        fill: #EDEDED;
      }

      .elementor-128245 .elementor-element.elementor-element-0522a5f.elementor-view-framed .elementor-icon {
        background-color: #5E5E5E;
      }

      .elementor-128245 .elementor-element.elementor-element-0522a5f.elementor-view-stacked .elementor-icon svg {
        fill: #5E5E5E;
      }

      .elementor-128245 .elementor-element.elementor-element-0522a5f .elementor-icon {
        font-size: 16px;
        padding: 6px;
        border-width: 0px 0px 0px 0px;
      }

      .elementor-128245 .elementor-element.elementor-element-0522a5f .elementor-icon i,
      .elementor-128245 .elementor-element.elementor-element-0522a5f .elementor-icon svg {
        transform: rotate(0deg);
      }

      .elementor-128245 .elementor-element.elementor-element-0522a5f>.elementor-widget-container {
        margin: 0px 0px -6px 0px;
      }

      .elementor-128245 .elementor-element.elementor-element-0522a5f {
        width: auto;
        max-width: auto;
      }

      .elementor-128245 .elementor-element.elementor-element-2e933f1 {
        text-align: center;
        width: auto;
        max-width: auto;
      }

      .elementor-128245 .elementor-element.elementor-element-2e933f1 .elementor-heading-title {
        color: #5E5E5E;
        font-family: "Nunito Sans", Sans-serif;
        font-size: 1em;
        font-weight: normal;
        letter-spacing: 2px;
      }

      .elementor-128245 .elementor-element.elementor-element-2e933f1>.elementor-widget-container {
        margin: 0em 0em 0em 0.5em;
      }

      .elementor-128245 .elementor-element.elementor-element-97fd3cb {
        text-align: center;
      }

      .elementor-128245 .elementor-element.elementor-element-97fd3cb .elementor-heading-title {
        color: #5E5E5E;
        font-family: "Nunito Sans", Sans-serif;
        font-size: 0.6em;
        font-weight: normal;
        line-height: 2em;
        letter-spacing: 2px;
      }

      .elementor-128245 .elementor-element.elementor-element-97fd3cb>.elementor-widget-container {
        margin: 0em 0em 1em 0em;
      }

      .elementor-128245 .elementor-element.elementor-element-a330d8d .elementor-button {
        font-family: "Nunito Sans", Sans-serif;
        font-size: 0.7em;
        font-weight: normal;
        letter-spacing: 2px;
        fill: #5E5E5E;
        color: #5E5E5E;
        background-color: #95A68200;
        border-radius: 0px 0px 0px 0px;
        padding: 0px 0px 0px 0px;
      }

      .elementor-128245 .elementor-element.elementor-element-a330d8d {
        z-index: 999;
        width: auto;
        max-width: auto;
      }

      .elementor-128245 .elementor-element.elementor-element-89ac2e9>.elementor-container {
        max-width: 415px;
      }

      .elementor-128245 .elementor-element.elementor-element-89ac2e9>.elementor-container>.elementor-row>.elementor-column>.elementor-column-wrap>.elementor-widget-wrap {
        align-content: center;
        align-items: center;
      }

      .elementor-128245 .elementor-element.elementor-element-89ac2e9 {
        padding: 0px 0px 20px 0px;
      }

      .elementor-128245 .elementor-element.elementor-element-59a0679.elementor-column>.elementor-column-wrap>.elementor-widget-wrap {
        justify-content: flex-end;
      }

      .elementor-128245 .elementor-element.elementor-element-59a0679>.elementor-element-populated>.elementor-widget-wrap {
        padding: 0em 1em 1em 0em;
      }

      .elementor-128245 .elementor-element.elementor-element-e32d3eb .elementor-icon-wrapper {
        text-align: right;
      }

      .elementor-128245 .elementor-element.elementor-element-e32d3eb.elementor-view-stacked .elementor-icon {
        background-color: #FFFFFF;
        color: #000000;
      }

      .elementor-128245 .elementor-element.elementor-element-e32d3eb.elementor-view-framed .elementor-icon,
      .elementor-128245 .elementor-element.elementor-element-e32d3eb.elementor-view-default .elementor-icon {
        color: #FFFFFF;
        border-color: #FFFFFF;
      }

      .elementor-128245 .elementor-element.elementor-element-e32d3eb.elementor-view-framed .elementor-icon,
      .elementor-128245 .elementor-element.elementor-element-e32d3eb.elementor-view-default .elementor-icon svg {
        fill: #FFFFFF;
      }

      .elementor-128245 .elementor-element.elementor-element-e32d3eb.elementor-view-framed .elementor-icon {
        background-color: #000000;
      }

      .elementor-128245 .elementor-element.elementor-element-e32d3eb.elementor-view-stacked .elementor-icon svg {
        fill: #000000;
      }

      .elementor-128245 .elementor-element.elementor-element-e32d3eb.elementor-view-stacked .elementor-icon:hover {
        background-color: #000000;
        color: #FFFFFF;
      }

      .elementor-128245 .elementor-element.elementor-element-e32d3eb.elementor-view-framed .elementor-icon:hover,
      .elementor-128245 .elementor-element.elementor-element-e32d3eb.elementor-view-default .elementor-icon:hover {
        color: #000000;
        border-color: #000000;
      }

      .elementor-128245 .elementor-element.elementor-element-e32d3eb.elementor-view-framed .elementor-icon:hover,
      .elementor-128245 .elementor-element.elementor-element-e32d3eb.elementor-view-default .elementor-icon:hover svg {
        fill: #000000;
      }

      .elementor-128245 .elementor-element.elementor-element-e32d3eb.elementor-view-framed .elementor-icon:hover {
        background-color: #FFFFFF;
      }

      .elementor-128245 .elementor-element.elementor-element-e32d3eb.elementor-view-stacked .elementor-icon:hover svg {
        fill: #FFFFFF;
      }

      .elementor-128245 .elementor-element.elementor-element-e32d3eb .elementor-icon {
        font-size: 38px;
        padding: 2px;
      }

      .elementor-128245 .elementor-element.elementor-element-e32d3eb .elementor-icon i,
      .elementor-128245 .elementor-element.elementor-element-e32d3eb .elementor-icon svg {
        transform: rotate(0deg);
      }

      .elementor-128245 .elementor-element.elementor-element-e32d3eb {
        width: auto;
        max-width: auto;
      }

      body.elementor-page-128245:not(.elementor-motion-effects-element-type-background),
      body.elementor-page-128245>.elementor-motion-effects-container>.elementor-motion-effects-layer {
        background-color: #181818;
      }

      @media(max-width:1024px) {
        .elementor-128245 .elementor-element.elementor-element-6cafba5 {
          --width: 150px;
        }

        .elementor-128245 .elementor-element.elementor-element-dcfcf52>.elementor-widget-container {
          margin: 10px 0px 10px 0px;
        }
      }

      @media(max-width:767px) {
        .elementor-128245 .elementor-element.elementor-element-9da6ff9.elementor-column>.elementor-column-wrap>.elementor-widget-wrap {
          justify-content: center;
        }

        .elementor-128245 .elementor-element.elementor-element-6cafba5 {
          --width: 150px;
        }

        .elementor-128245 .elementor-element.elementor-element-7d4475a .elementor-heading-title {
          font-size: 14px;
        }

        .elementor-128245 .elementor-element.elementor-element-dcfcf52 .elementor-heading-title {
          font-size: 18px;
        }

        .elementor-128245 .elementor-element.elementor-element-3967eea.elementor-column>.elementor-column-wrap>.elementor-widget-wrap {
          justify-content: center;
        }

        .elementor-128245 .elementor-element.elementor-element-6b182d4>.elementor-container {
          min-height: 350px;
        }

        .elementor-128245 .elementor-element.elementor-element-d46a144 {
          width: 65%;
        }

        body:not(.rtl) .elementor-128245 .elementor-element.elementor-element-ddd5b51 {
          left: 25%;
        }

        body.rtl .elementor-128245 .elementor-element.elementor-element-ddd5b51 {
          right: 25%;
        }

        .elementor-128245 .elementor-element.elementor-element-a5b8dba {
          width: 35%;
        }

        .elementor-128245 .elementor-element.elementor-element-b92dacd>.elementor-container {
          min-height: 350px;
        }

        .elementor-128245 .elementor-element.elementor-element-bc50505 {
          width: 35%;
        }

        .elementor-128245 .elementor-element.elementor-element-31d0772 {
          width: 65%;
        }

        body:not(.rtl) .elementor-128245 .elementor-element.elementor-element-d475a39 {
          right: 25%;
        }

        body.rtl .elementor-128245 .elementor-element.elementor-element-d475a39 {
          left: 25%;
        }

        .elementor-128245 .elementor-element.elementor-element-f03ebad .elementor-divider-separator {
          width: 50%;
        }

        .elementor-128245 .elementor-element.elementor-element-7f27de5 {
          width: 24%;
        }

        .elementor-128245 .elementor-element.elementor-element-f5a3804 {
          width: 76%;
        }

        .elementor-bc-flex-widget .elementor-128245 .elementor-element.elementor-element-f5a3804.elementor-column .elementor-column-wrap {
          align-items: center;
        }

        .elementor-128245 .elementor-element.elementor-element-f5a3804.elementor-column.elementor-element[data-element_type="column"]>.elementor-column-wrap.elementor-element-populated>.elementor-widget-wrap {
          align-content: center;
          align-items: center;
        }

        .elementor-128245 .elementor-element.elementor-element-ee784a5 {
          width: 40%;
        }

        .elementor-128245 .elementor-element.elementor-element-91caa13 {
          width: 60%;
        }

        .elementor-128245 .elementor-element.elementor-element-56c487c .elementor-icon-list-item>.elementor-icon-list-text,
        .elementor-128245 .elementor-element.elementor-element-56c487c .elementor-icon-list-item>a {
          font-size: 12px;
        }

        .elementor-128245 .elementor-element.elementor-element-37f8b28 {
          width: 76%;
        }

        .elementor-bc-flex-widget .elementor-128245 .elementor-element.elementor-element-37f8b28.elementor-column .elementor-column-wrap {
          align-items: center;
        }

        .elementor-128245 .elementor-element.elementor-element-37f8b28.elementor-column.elementor-element[data-element_type="column"]>.elementor-column-wrap.elementor-element-populated>.elementor-widget-wrap {
          align-content: center;
          align-items: center;
        }

        .elementor-128245 .elementor-element.elementor-element-c9ad365 {
          width: 40%;
        }

        .elementor-128245 .elementor-element.elementor-element-e041a66 {
          width: 60%;
        }

        .elementor-128245 .elementor-element.elementor-element-047d9c4 .elementor-icon-list-item>.elementor-icon-list-text,
        .elementor-128245 .elementor-element.elementor-element-047d9c4 .elementor-icon-list-item>a {
          font-size: 12px;
        }

        .elementor-128245 .elementor-element.elementor-element-49e38d9 {
          width: 24%;
        }

        .elementor-128245 .elementor-element.elementor-element-b4d998c .elementor-divider-separator {
          width: 50%;
        }

        .elementor-128245 .elementor-element.elementor-element-7bd8417>.elementor-container {
          min-height: 150px;
        }

        .elementor-128245 .elementor-element.elementor-element-1e3fbf7 {
          width: 33.3333%;
        }

        .elementor-128245 .elementor-element.elementor-element-688989c .elementor-icon {
          font-size: 35px;
        }

        .elementor-128245 .elementor-element.elementor-element-02f10ac {
          width: 33.3333%;
        }

        .elementor-128245 .elementor-element.elementor-element-cd9aff3 .elementor-icon {
          font-size: 35px;
        }

        .elementor-128245 .elementor-element.elementor-element-96a2fe0 {
          width: 33.3333%;
        }

        .elementor-128245 .elementor-element.elementor-element-00cd332 .elementor-icon {
          font-size: 35px;
        }

        .elementor-128245 .elementor-element.elementor-element-2e933f1 {
          text-align: center;
        }

        .elementor-128245 .elementor-element.elementor-element-2e933f1 .elementor-heading-title {
          line-height: 1.4em;
        }

        .elementor-128245 .elementor-element.elementor-element-97fd3cb {
          text-align: center;
        }

        .elementor-128245 .elementor-element.elementor-element-97fd3cb .elementor-heading-title {
          line-height: 1.4em;
        }

        .elementor-128245 .elementor-element.elementor-element-89ac2e9>.elementor-container {
          max-width: 500px;
        }

        .elementor-128245 .elementor-element.elementor-element-89ac2e9 {
          margin-top: 0px;
          margin-bottom: 0px;
        }
      }

      @media(min-width:768px) {
        .elementor-128245 .elementor-element.elementor-element-d46a144 {
          width: 65%;
        }

        .elementor-128245 .elementor-element.elementor-element-a5b8dba {
          width: 35%;
        }

        .elementor-128245 .elementor-element.elementor-element-bc50505 {
          width: 35%;
        }

        .elementor-128245 .elementor-element.elementor-element-31d0772 {
          width: 65%;
        }

        .elementor-128245 .elementor-element.elementor-element-7f27de5 {
          width: 24%;
        }

        .elementor-128245 .elementor-element.elementor-element-f5a3804 {
          width: 76%;
        }

        .elementor-128245 .elementor-element.elementor-element-ee784a5 {
          width: 38%;
        }

        .elementor-128245 .elementor-element.elementor-element-91caa13 {
          width: 62%;
        }

        .elementor-128245 .elementor-element.elementor-element-37f8b28 {
          width: 76%;
        }

        .elementor-128245 .elementor-element.elementor-element-c9ad365 {
          width: 38%;
        }

        .elementor-128245 .elementor-element.elementor-element-e041a66 {
          width: 62%;
        }

        .elementor-128245 .elementor-element.elementor-element-49e38d9 {
          width: 24%;
        }
      }

      @media(max-width:1024px) and (min-width:768px) {
        .elementor-128245 .elementor-element.elementor-element-d46a144 {
          width: 65%;
        }

        .elementor-128245 .elementor-element.elementor-element-a5b8dba {
          width: 35%;
        }

        .elementor-128245 .elementor-element.elementor-element-bc50505 {
          width: 35%;
        }

        .elementor-128245 .elementor-element.elementor-element-31d0772 {
          width: 65%;
        }
      }

      /* Start custom CSS for section, class: .elementor-element-ed84c9e */
      /* End custom CSS */
      /* Start custom CSS for button, class: .elementor-element-447df93 */
      #b_o {
        cursor: pointer;
        -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
      }

      /* End custom CSS */
      /* Start custom CSS for column, class: .elementor-element-9da6ff9 */
      .elementor-128245 .elementor-element.elementor-element-9da6ff9 {
        height: calc(var(--vh, 1vh) * 100);
      }

      /* End custom CSS */
      /* Start custom CSS for section, class: .elementor-element-513f728 */
      #s0 {
        top: 0;
        width: 100%;
        position: absolute;
        height: calc(var(--vh, 1vh) * 100);
      }

      @media only screen and (min-width: 415px) {
        #s0 {
          width: 415px
        }
      }

      /* End custom CSS */
      /* Start custom CSS for column, class: .elementor-element-3967eea */
      .elementor-128245 .elementor-element.elementor-element-3967eea {
        height: calc(var(--vh, 1vh) * 100);
      }

      /* End custom CSS */
      /* Start custom CSS for section, class: .elementor-element-4a0a9f7 */
      .elementor-128245 .elementor-element.elementor-element-4a0a9f7 {
        height: calc(var(--vh, 1vh) * 100);
        background: black !important;
      }

      /* End custom CSS */
      /* Start custom CSS for heading, class: .elementor-element-ddd5b51 */
      .elementor-128245 .elementor-element.elementor-element-ddd5b51 {
        transform: rotate(90deg)
      }

      /* End custom CSS */
      /* Start custom CSS for heading, class: .elementor-element-d475a39 */
      .elementor-128245 .elementor-element.elementor-element-d475a39 {
        transform: rotate(-90deg)
      }

      /* End custom CSS */
      /* Start custom CSS for gallery, class: .elementor-element-208dc63 */
      #galeri {
        display: block;
      }

      /* End custom CSS */
      /* Start custom CSS for button, class: .elementor-element-1db3065 */
      #btn_gift {
        cursor: pointer
      }

      /* End custom CSS */
      /* Start custom CSS for section, class: .elementor-element-4b39e42 */
      .elementor-128245 .elementor-element.elementor-element-4b39e42 {
        display: none;
      }

      /* End custom CSS */
      /* Start custom CSS for html, class: .elementor-element-795e8b5 */
      .elementor-128245 .elementor-element.elementor-element-795e8b5 {
        display: none;
      }

      /* End custom CSS */
      /* Start custom CSS for section, class: .elementor-element-7bd8417 */
      .elementor-128245 .elementor-element.elementor-element-7bd8417 {
        background: #FFF
      }

      /* End custom CSS */
      /* Start custom CSS for weddingpress-commentkit, class: .elementor-element-ee6f05c */
      .wdp-wrapper {
        font-family: Poppins !important;
        font-size: 13px;
        font-weight: 400;
      }

      input[type=date],
      input[type=email],
      input[type=number],
      input[type=password],
      input[type=search],
      input[type=tel],
      input[type=text],
      input[type=url],
      select,
      textarea {
        color: #5E5E5E !important;
      }

      .wdp-wrapper .wdp-wrap-link {
        display: none
      }

      .wdp-wrapper .wdp-wrap-form {
        border-top: 0px;
      }

      .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type='text'] {
        border-bottom: 1px solid #d5deea !important;
        background: #fbfbfb !important;
      }

      .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type=text] {
        border: 0px;
        border-radius: 0px;
        height: 3em !important;
      }

      .wdp-wrapper .wdp-wrap-form .wdp-container-form textarea.wdp-textarea {
        border-bottom: 1px solid #d5deea !important;
        background: #fbfbfb !important;
      }

      .wdp-wrapper .wdp-wrap-form .wdp-container-form textarea.wdp-textarea {
        border: 0px;
        border-radius: 0px;
        margin-bottom: 5px !important
      }

      .wdp-wrapper .wdp-wrap-form .wdp-container-form.wdp-no-login select.wdp-select {
        border-bottom: 1px solid #d5deea !important;
      }

      .wdp-wrapper .wdp-wrap-form .wdp-container-form select.wdp-select {
        border: 0px;
        border-radius: 0px;
        background: #fbfbfb !important;
        height: 3em !important;
      }

      .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type='submit'],
      .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type='button'].wdp-form-btn {
        font-size: 14px !important;
        letter-spacing: 2px;
        text-transform: uppercase;
        font-weight: 200;
        color: #FFF;
        background: #5E5E5E;
        border-radius: 0px;
      }

      .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type='submit']:hover,
      .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type='button'].wdp-form-btn:hover {
        font-size: 14px !important;
        letter-spacing: 3px;
        text-transform: uppercase;
        font-weight: 400;
        color: #FFF;
        background: #5E5E5E;
        border-radius: 0px;
      }

      .wdp-comment-avatar,
      .wdp-post-author,
      .wdp-comment-time {
        display: none !important;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment {
        border-radius: 0px
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment {
        border: 0px;
        border-bottom: 1px solid #d5deea !important;
      }

      .wdp-wrapper .wdp-comment-status p.wdp-ajax-success {
        font-family: Poppins !important;
        font-size: 11px;
        color: #5E5E5E;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-info a.wdp-commenter-name {
        color: #5E5E5E !important;
        font-weight: 500 !important;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-info a.wdp-commenter-name:hover {
        color: #5E5E5E !important
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-text p {
        color: #5E5E5E !important;
      }

      .wdp-wrap-radio {
        color: #5E5E5E !important;
      }

      .wdp-wrap-radio {
        border-radius: 0px !important;
        border: 1px solid #d5deea !important;
      }

      .wdpo-loading:before {
        color: #d5deea !important;
      }

      .wdp-wrapper .wdp-comment-status {
        border-top: 0px solid #d5deea !important;
      }

      ul.wdp-container-comments {
        min-height: 0px;
        max-height: 400px !important;
        overflow: auto;
      }

      /* End custom CSS */
      /* Start custom CSS for html, class: .elementor-element-fc13e0d */
      .elementor-128245 .elementor-element.elementor-element-fc13e0d {
        display: none
      }

      /* End custom CSS */
      /* Start custom CSS for icon, class: .elementor-element-688989c */
      .b4014e33-99de-4bff-a040-79cf4526fd82 {
        fill: #5E5E5E !important;
      }

      .bcc92e79-7f24-41ba-9405-bfa8b895eaa3 {
        fill: #5E5E5E !important;
      }

      .a29fadad-87cd-45dd-b870-cd004368b1e6 {
        fill: #5E5E5E !important;
      }

      .ff452229-05c2-4ff9-b426-bfc8edf0f91c {
        fill: #5E5E5E !important;
      }

      .a4e688da-be50-4782-9a68-990a44b428bd {
        fill: #5E5E5E !important;
      }

      .a3b16fa2-f15d-406b-8ae9-5a2a2eb27037 {
        fill: #5E5E5E !important;
      }

      /* End custom CSS */
      /* Start custom CSS for weddingpress-audio, class: .elementor-element-e32d3eb */
      #mute-sound .elementor-icon {
        -webkit-animation: rotating 12s linear infinite;
        -moz-animation: rotating 12s linear infinite;
        -ms-animation: rotating 12s linear infinite;
        -o-animation: rotating 12s linear infinite;
        animation: rotating 12s linear infinite;
      }

      @keyframes rotating {
        from {
          -ms-transform: rotate(0deg);
          -moz-transform: rotate(0deg);
          -webkit-transform: rotate(0deg);
          -o-transform: rotate(0deg);
          transform: rotate(0deg);
        }

        to {
          -ms-transform: rotate(360deg);
          -moz-transform: rotate(360deg);
          -webkit-transform: rotate(360deg);
          -o-transform: rotate(360deg);
          transform: rotate(360deg);
        }
      }

      /* End custom CSS */
      /* Start custom CSS for section, class: .elementor-element-a9399bf */
      .elementor-128245 .elementor-element.elementor-element-a9399bf {
        display: none;
      }

      /* End custom CSS */
      /* Start custom CSS */
      @media only screen and (min-width: 360px) {
        body.elementor-page-128245 {
          max-width: 415px;
        }
      }

      .elementor-column>.elementor-widget-wrap>.elementor-element.elementor-absolute {
        padding: initial !important;
      }

      ::-webkit-scrollbar {
        width: 3px !important;
      }

      ul.wdp-container-comments::-webkit-scrollbar-track {
        background: #f1f1f1 !important;
        border-radius: 10px !important;
      }

      ul.wdp-container-comments::-webkit-scrollbar-thumb {
        background: #888 !important;
        border-radius: 10px !important;
      }

      ul.wdp-container-comments::-webkit-scrollbar-thumb:hover {
        background: #555 !important;
        border-radius: 5px !important;
      }

      input,
      textarea,
      button,
      select,
      a {
        -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
      }

      /* End custom CSS */
      /* Start Custom Fonts CSS */
      @font-face {
        font-family: 'Analogue';
        font-style: normal;
        font-weight: normal;
        font-display: auto;
        src: url('wp-content/uploads/2022/07/Analogue.eot');
        src: url('wp-content/uploads/2022/07/Analogue.eot?#iefix') format('embedded-opentype'),
          url('wp-content/uploads/2022/07/Analogue.woff2') format('woff2'),
          url('wp-content/uploads/2022/07/Analogue.woff') format('woff'),
          url('wp-content/uploads/2022/07/Analogue.ttf') format('truetype'),
          url('wp-content/uploads/2022/07/Analogue.svg#Analogue') format('svg');
      }

      /* End Custom Fonts CSS */
      /* Start Custom Fonts CSS */
      @font-face {
        font-family: 'Power';
        font-style: normal;
        font-weight: normal;
        font-display: auto;
        src: url('wp-content/uploads/2022/07/power.eot');
        src: url('wp-content/uploads/2022/07/power.eot?#iefix') format('embedded-opentype'),
          url('wp-content/uploads/2022/07/power.woff2') format('woff2'),
          url('wp-content/uploads/2022/07/power.woff') format('woff'),
          url('wp-content/uploads/2022/07/power.ttf') format('truetype'),
          url('wp-content/uploads/2022/07/power.svg#Power') format('svg');
      }

      /* End Custom Fonts CSS */
      /* Start Custom Fonts CSS */
      @font-face {
        font-family: 'Creattion';
        font-style: normal;
        font-weight: normal;
        font-display: auto;
        src: url('wp-content/uploads/2022/01/Creattion.eot');
        src: url('wp-content/uploads/2022/01/Creattion.eot?#iefix') format('embedded-opentype'),
          url('wp-content/uploads/2022/01/Creattion.woff2') format('woff2'),
          url('wp-content/uploads/2022/01/Creattion.woff') format('woff'),
          url('wp-content/uploads/2022/01/Creattion.ttf') format('truetype'),
          url('wp-content/uploads/2022/01/Creattion.svg#Creattion') format('svg');
      }

      /* End Custom Fonts CSS */
      .elementor-bc-flex-widget .elementor-50158 .elementor-element.elementor-element-131bef9.elementor-column .elementor-column-wrap {
        align-items: center;
      }

      .elementor-50158 .elementor-element.elementor-element-131bef9.elementor-column.elementor-element[data-element_type="column"]>.elementor-column-wrap.elementor-element-populated>.elementor-widget-wrap {
        align-content: center;
        align-items: center;
      }

      .elementor-50158 .elementor-element.elementor-element-131bef9.elementor-column>.elementor-column-wrap>.elementor-widget-wrap {
        justify-content: center;
      }

      .elementor-50158 .elementor-element.elementor-element-131bef9>.elementor-column-wrap>.elementor-widget-wrap>.elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
        margin-bottom: 10px;
      }

      .elementor-50158 .elementor-element.elementor-element-131bef9>.elementor-element-populated>.elementor-widget-wrap {
        padding: 3em 2em 3em 2em;
      }

      .elementor-50158 .elementor-element.elementor-element-093b077>.elementor-widget-container {
        margin: -2em 0em 1em 0em;
      }

      .elementor-50158 .elementor-element.elementor-element-093b077 {
        width: 100px;
        max-width: 100px;
      }

      .elementor-50158 .elementor-element.elementor-element-7075053 {
        text-align: center;
      }

      .elementor-50158 .elementor-element.elementor-element-7075053 .elementor-heading-title {
        color: #232323;
        font-family: "Nunito Sans", Sans-serif;
        font-size: 13px;
        font-weight: normal;
        line-height: 1.5em;
        letter-spacing: 1px;
      }

      .elementor-50158 .elementor-element.elementor-element-7075053>.elementor-widget-container {
        margin: 0em 0em 1em 0em;
      }

      .elementor-50158 .elementor-element.elementor-element-6623270 .elementor-button {
        font-family: "Nunito Sans", Sans-serif;
        font-size: 12px;
        font-weight: normal;
        text-transform: uppercase;
        letter-spacing: 2px;
        fill: #FFFFFF;
        color: #FFFFFF;
        background-color: #232323;
        border-radius: 0px 0px 0px 0px;
        padding: 8px 10px 8px 10px;
      }

      .elementor-50158 .elementor-element.elementor-element-6623270 {
        width: auto;
        max-width: auto;
      }

      #elementor-popup-modal-50158 .dialog-message {
        width: 380px;
        height: auto;
      }

      #elementor-popup-modal-50158 {
        justify-content: center;
        align-items: center;
        pointer-events: all;
        background-color: rgba(0, 0, 0, .8);
      }

      #elementor-popup-modal-50158 .dialog-widget-content {
        animation-duration: 0.5s;
        border-radius: 8px 8px 8px 8px;
        box-shadow: 2px 8px 23px 3px rgba(0, 0, 0, 0.2);
      }

      @media(max-width:767px) {
        #elementor-popup-modal-50158 .dialog-message {
          width: 85vw;
        }
      }

      /* Start custom CSS */
      #elementor-popup-modal-50158 {
        backdrop-filter: blur(3px);
        -webkit-backdrop-filter: blur(3px);
      }

      /* End custom CSS */
      @-webkit-keyframes ha_fadeIn {
        0% {
          opacity: 0
        }

        to {
          opacity: 1
        }
      }

      @keyframes ha_fadeIn {
        0% {
          opacity: 0
        }

        to {
          opacity: 1
        }
      }

      .ha_fadeIn {
        -webkit-animation-name: ha_fadeIn;
        animation-name: ha_fadeIn
      }

      @-webkit-keyframes ha_zoomIn {
        0% {
          opacity: 0;
          -webkit-transform: scale3d(.3, .3, .3);
          transform: scale3d(.3, .3, .3)
        }

        50% {
          opacity: 1
        }
      }

      @keyframes ha_zoomIn {
        0% {
          opacity: 0;
          -webkit-transform: scale3d(.3, .3, .3);
          transform: scale3d(.3, .3, .3)
        }

        50% {
          opacity: 1
        }
      }

      .ha_zoomIn {
        -webkit-animation-name: ha_zoomIn;
        animation-name: ha_zoomIn
      }

      @-webkit-keyframes ha_rollIn {
        0% {
          opacity: 0;
          -webkit-transform: translate3d(-100%, 0, 0) rotate3d(0, 0, 1, -120deg);
          transform: translate3d(-100%, 0, 0) rotate3d(0, 0, 1, -120deg)
        }

        to {
          opacity: 1
        }
      }

      @keyframes ha_rollIn {
        0% {
          opacity: 0;
          -webkit-transform: translate3d(-100%, 0, 0) rotate3d(0, 0, 1, -120deg);
          transform: translate3d(-100%, 0, 0) rotate3d(0, 0, 1, -120deg)
        }

        to {
          opacity: 1
        }
      }

      .ha_rollIn {
        -webkit-animation-name: ha_rollIn;
        animation-name: ha_rollIn
      }

      @-webkit-keyframes ha_bounce {

        0%,
        20%,
        53%,
        to {
          -webkit-animation-timing-function: cubic-bezier(.215, .61, .355, 1);
          animation-timing-function: cubic-bezier(.215, .61, .355, 1)
        }

        40%,
        43% {
          -webkit-transform: translate3d(0, -30px, 0) scaleY(1.1);
          transform: translate3d(0, -30px, 0) scaleY(1.1);
          -webkit-animation-timing-function: cubic-bezier(.755, .05, .855, .06);
          animation-timing-function: cubic-bezier(.755, .05, .855, .06)
        }

        70% {
          -webkit-transform: translate3d(0, -15px, 0) scaleY(1.05);
          transform: translate3d(0, -15px, 0) scaleY(1.05);
          -webkit-animation-timing-function: cubic-bezier(.755, .05, .855, .06);
          animation-timing-function: cubic-bezier(.755, .05, .855, .06)
        }

        80% {
          -webkit-transition-timing-function: cubic-bezier(.215, .61, .355, 1);
          transition-timing-function: cubic-bezier(.215, .61, .355, 1);
          -webkit-transform: translate3d(0, 0, 0) scaleY(.95);
          transform: translate3d(0, 0, 0) scaleY(.95)
        }

        90% {
          -webkit-transform: translate3d(0, -4px, 0) scaleY(1.02);
          transform: translate3d(0, -4px, 0) scaleY(1.02)
        }
      }

      @keyframes ha_bounce {

        0%,
        20%,
        53%,
        to {
          -webkit-animation-timing-function: cubic-bezier(.215, .61, .355, 1);
          animation-timing-function: cubic-bezier(.215, .61, .355, 1)
        }

        40%,
        43% {
          -webkit-transform: translate3d(0, -30px, 0) scaleY(1.1);
          transform: translate3d(0, -30px, 0) scaleY(1.1);
          -webkit-animation-timing-function: cubic-bezier(.755, .05, .855, .06);
          animation-timing-function: cubic-bezier(.755, .05, .855, .06)
        }

        70% {
          -webkit-transform: translate3d(0, -15px, 0) scaleY(1.05);
          transform: translate3d(0, -15px, 0) scaleY(1.05);
          -webkit-animation-timing-function: cubic-bezier(.755, .05, .855, .06);
          animation-timing-function: cubic-bezier(.755, .05, .855, .06)
        }

        80% {
          -webkit-transition-timing-function: cubic-bezier(.215, .61, .355, 1);
          transition-timing-function: cubic-bezier(.215, .61, .355, 1);
          -webkit-transform: translate3d(0, 0, 0) scaleY(.95);
          transform: translate3d(0, 0, 0) scaleY(.95)
        }

        90% {
          -webkit-transform: translate3d(0, -4px, 0) scaleY(1.02);
          transform: translate3d(0, -4px, 0) scaleY(1.02)
        }
      }

      .ha_bounce {
        -webkit-transform-origin: center bottom;
        -ms-transform-origin: center bottom;
        transform-origin: center bottom;
        -webkit-animation-name: ha_bounce;
        animation-name: ha_bounce
      }

      @-webkit-keyframes ha_bounceIn {

        0%,
        20%,
        40%,
        60%,
        80%,
        to {
          -webkit-animation-timing-function: cubic-bezier(.215, .61, .355, 1);
          animation-timing-function: cubic-bezier(.215, .61, .355, 1)
        }

        0% {
          opacity: 0;
          -webkit-transform: scale3d(.3, .3, .3);
          transform: scale3d(.3, .3, .3)
        }

        20% {
          -webkit-transform: scale3d(1.1, 1.1, 1.1);
          transform: scale3d(1.1, 1.1, 1.1)
        }

        40% {
          -webkit-transform: scale3d(.9, .9, .9);
          transform: scale3d(.9, .9, .9)
        }

        60% {
          opacity: 1;
          -webkit-transform: scale3d(1.03, 1.03, 1.03);
          transform: scale3d(1.03, 1.03, 1.03)
        }

        80% {
          -webkit-transform: scale3d(.97, .97, .97);
          transform: scale3d(.97, .97, .97)
        }

        to {
          opacity: 1
        }
      }

      @keyframes ha_bounceIn {

        0%,
        20%,
        40%,
        60%,
        80%,
        to {
          -webkit-animation-timing-function: cubic-bezier(.215, .61, .355, 1);
          animation-timing-function: cubic-bezier(.215, .61, .355, 1)
        }

        0% {
          opacity: 0;
          -webkit-transform: scale3d(.3, .3, .3);
          transform: scale3d(.3, .3, .3)
        }

        20% {
          -webkit-transform: scale3d(1.1, 1.1, 1.1);
          transform: scale3d(1.1, 1.1, 1.1)
        }

        40% {
          -webkit-transform: scale3d(.9, .9, .9);
          transform: scale3d(.9, .9, .9)
        }

        60% {
          opacity: 1;
          -webkit-transform: scale3d(1.03, 1.03, 1.03);
          transform: scale3d(1.03, 1.03, 1.03)
        }

        80% {
          -webkit-transform: scale3d(.97, .97, .97);
          transform: scale3d(.97, .97, .97)
        }

        to {
          opacity: 1
        }
      }

      .ha_bounceIn {
        -webkit-animation-name: ha_bounceIn;
        animation-name: ha_bounceIn;
        -webkit-animation-duration: calc(1s*.75);
        -webkit-animation-duration: calc(var(--animate-duration)*.75);
        animation-duration: calc(1s*.75);
        animation-duration: calc(var(--animate-duration)*.75)
      }

      @-webkit-keyframes ha_flipInX {
        0% {
          opacity: 0;
          -webkit-transform: perspective(400px) rotate3d(1, 0, 0, 90deg);
          transform: perspective(400px) rotate3d(1, 0, 0, 90deg);
          -webkit-animation-timing-function: ease-in;
          animation-timing-function: ease-in
        }

        40% {
          -webkit-transform: perspective(400px) rotate3d(1, 0, 0, -20deg);
          transform: perspective(400px) rotate3d(1, 0, 0, -20deg);
          -webkit-animation-timing-function: ease-in;
          animation-timing-function: ease-in
        }

        60% {
          opacity: 1;
          -webkit-transform: perspective(400px) rotate3d(1, 0, 0, 10deg);
          transform: perspective(400px) rotate3d(1, 0, 0, 10deg)
        }

        80% {
          -webkit-transform: perspective(400px) rotate3d(1, 0, 0, -5deg);
          transform: perspective(400px) rotate3d(1, 0, 0, -5deg)
        }
      }

      @keyframes ha_flipInX {
        0% {
          opacity: 0;
          -webkit-transform: perspective(400px) rotate3d(1, 0, 0, 90deg);
          transform: perspective(400px) rotate3d(1, 0, 0, 90deg);
          -webkit-animation-timing-function: ease-in;
          animation-timing-function: ease-in
        }

        40% {
          -webkit-transform: perspective(400px) rotate3d(1, 0, 0, -20deg);
          transform: perspective(400px) rotate3d(1, 0, 0, -20deg);
          -webkit-animation-timing-function: ease-in;
          animation-timing-function: ease-in
        }

        60% {
          opacity: 1;
          -webkit-transform: perspective(400px) rotate3d(1, 0, 0, 10deg);
          transform: perspective(400px) rotate3d(1, 0, 0, 10deg)
        }

        80% {
          -webkit-transform: perspective(400px) rotate3d(1, 0, 0, -5deg);
          transform: perspective(400px) rotate3d(1, 0, 0, -5deg)
        }
      }

      .ha_flipInX,
      .ha_flipInY {
        -webkit-animation-name: ha_flipInX;
        animation-name: ha_flipInX;
        -webkit-backface-visibility: visible !important;
        backface-visibility: visible !important
      }

      @-webkit-keyframes ha_flipInY {
        0% {
          opacity: 0;
          -webkit-transform: perspective(400px) rotate3d(0, 1, 0, 90deg);
          transform: perspective(400px) rotate3d(0, 1, 0, 90deg);
          -webkit-animation-timing-function: ease-in;
          animation-timing-function: ease-in
        }

        40% {
          -webkit-transform: perspective(400px) rotate3d(0, 1, 0, -20deg);
          transform: perspective(400px) rotate3d(0, 1, 0, -20deg);
          -webkit-animation-timing-function: ease-in;
          animation-timing-function: ease-in
        }

        60% {
          opacity: 1;
          -webkit-transform: perspective(400px) rotate3d(0, 1, 0, 10deg);
          transform: perspective(400px) rotate3d(0, 1, 0, 10deg)
        }

        80% {
          -webkit-transform: perspective(400px) rotate3d(0, 1, 0, -5deg);
          transform: perspective(400px) rotate3d(0, 1, 0, -5deg)
        }
      }

      @keyframes ha_flipInY {
        0% {
          opacity: 0;
          -webkit-transform: perspective(400px) rotate3d(0, 1, 0, 90deg);
          transform: perspective(400px) rotate3d(0, 1, 0, 90deg);
          -webkit-animation-timing-function: ease-in;
          animation-timing-function: ease-in
        }

        40% {
          -webkit-transform: perspective(400px) rotate3d(0, 1, 0, -20deg);
          transform: perspective(400px) rotate3d(0, 1, 0, -20deg);
          -webkit-animation-timing-function: ease-in;
          animation-timing-function: ease-in
        }

        60% {
          opacity: 1;
          -webkit-transform: perspective(400px) rotate3d(0, 1, 0, 10deg);
          transform: perspective(400px) rotate3d(0, 1, 0, 10deg)
        }

        80% {
          -webkit-transform: perspective(400px) rotate3d(0, 1, 0, -5deg);
          transform: perspective(400px) rotate3d(0, 1, 0, -5deg)
        }
      }

      .ha_flipInY {
        -webkit-animation-name: ha_flipInY;
        animation-name: ha_flipInY
      }

      @-webkit-keyframes ha_swing {
        20% {
          -webkit-transform: rotate3d(0, 0, 1, 15deg);
          transform: rotate3d(0, 0, 1, 15deg)
        }

        40% {
          -webkit-transform: rotate3d(0, 0, 1, -10deg);
          transform: rotate3d(0, 0, 1, -10deg)
        }

        60% {
          -webkit-transform: rotate3d(0, 0, 1, 5deg);
          transform: rotate3d(0, 0, 1, 5deg)
        }

        80% {
          -webkit-transform: rotate3d(0, 0, 1, -5deg);
          transform: rotate3d(0, 0, 1, -5deg)
        }
      }

      @keyframes ha_swing {
        20% {
          -webkit-transform: rotate3d(0, 0, 1, 15deg);
          transform: rotate3d(0, 0, 1, 15deg)
        }

        40% {
          -webkit-transform: rotate3d(0, 0, 1, -10deg);
          transform: rotate3d(0, 0, 1, -10deg)
        }

        60% {
          -webkit-transform: rotate3d(0, 0, 1, 5deg);
          transform: rotate3d(0, 0, 1, 5deg)
        }

        80% {
          -webkit-transform: rotate3d(0, 0, 1, -5deg);
          transform: rotate3d(0, 0, 1, -5deg)
        }
      }

      .ha_swing {
        -webkit-transform-origin: top center;
        -ms-transform-origin: top center;
        transform-origin: top center;
        -webkit-animation-name: ha_swing;
        animation-name: ha_swing
      }

      @-webkit-keyframes ha_slideInDown {
        0% {
          visibility: visible;
          -webkit-transform: translate3d(0, -100%, 0);
          transform: translate3d(0, -100%, 0)
        }
      }

      @keyframes ha_slideInDown {
        0% {
          visibility: visible;
          -webkit-transform: translate3d(0, -100%, 0);
          transform: translate3d(0, -100%, 0)
        }
      }

      .ha_slideInDown {
        -webkit-animation-name: ha_slideInDown;
        animation-name: ha_slideInDown
      }

      @-webkit-keyframes ha_slideInUp {
        0% {
          visibility: visible;
          -webkit-transform: translate3d(0, 100%, 0);
          transform: translate3d(0, 100%, 0)
        }
      }

      @keyframes ha_slideInUp {
        0% {
          visibility: visible;
          -webkit-transform: translate3d(0, 100%, 0);
          transform: translate3d(0, 100%, 0)
        }
      }

      .ha_slideInUp {
        -webkit-animation-name: ha_slideInUp;
        animation-name: ha_slideInUp
      }

      @-webkit-keyframes ha_slideInLeft {
        0% {
          visibility: visible;
          -webkit-transform: translate3d(-100%, 0, 0);
          transform: translate3d(-100%, 0, 0)
        }
      }

      @keyframes ha_slideInLeft {
        0% {
          visibility: visible;
          -webkit-transform: translate3d(-100%, 0, 0);
          transform: translate3d(-100%, 0, 0)
        }
      }

      .ha_slideInLeft {
        -webkit-animation-name: ha_slideInLeft;
        animation-name: ha_slideInLeft
      }

      @-webkit-keyframes ha_slideInRight {
        0% {
          visibility: visible;
          -webkit-transform: translate3d(100%, 0, 0);
          transform: translate3d(100%, 0, 0)
        }
      }

      @keyframes ha_slideInRight {
        0% {
          visibility: visible;
          -webkit-transform: translate3d(100%, 0, 0);
          transform: translate3d(100%, 0, 0)
        }
      }

      .ha_slideInRight {
        -webkit-animation-name: ha_slideInRight;
        animation-name: ha_slideInRight
      }

      .ha-css-transform-yes {
        -webkit-transition-duration: var(--ha-tfx-transition-duration, .2s);
        transition-duration: var(--ha-tfx-transition-duration, .2s);
        -webkit-transition-property: -webkit-transform;
        transition-property: transform;
        transition-property: transform, -webkit-transform;
        -webkit-transform: translate(var(--ha-tfx-translate-x, 0), var(--ha-tfx-translate-y, 0)) scale(var(--ha-tfx-scale-x, 1), var(--ha-tfx-scale-y, 1)) skew(var(--ha-tfx-skew-x, 0), var(--ha-tfx-skew-y, 0)) rotateX(var(--ha-tfx-rotate-x, 0)) rotateY(var(--ha-tfx-rotate-y, 0)) rotateZ(var(--ha-tfx-rotate-z, 0));
        transform: translate(var(--ha-tfx-translate-x, 0), var(--ha-tfx-translate-y, 0)) scale(var(--ha-tfx-scale-x, 1), var(--ha-tfx-scale-y, 1)) skew(var(--ha-tfx-skew-x, 0), var(--ha-tfx-skew-y, 0)) rotateX(var(--ha-tfx-rotate-x, 0)) rotateY(var(--ha-tfx-rotate-y, 0)) rotateZ(var(--ha-tfx-rotate-z, 0))
      }

      .ha-css-transform-yes:hover {
        -webkit-transform: translate(var(--ha-tfx-translate-x-hover, var(--ha-tfx-translate-x, 0)), var(--ha-tfx-translate-y-hover, var(--ha-tfx-translate-y, 0))) scale(var(--ha-tfx-scale-x-hover, var(--ha-tfx-scale-x, 1)), var(--ha-tfx-scale-y-hover, var(--ha-tfx-scale-y, 1))) skew(var(--ha-tfx-skew-x-hover, var(--ha-tfx-skew-x, 0)), var(--ha-tfx-skew-y-hover, var(--ha-tfx-skew-y, 0))) rotateX(var(--ha-tfx-rotate-x-hover, var(--ha-tfx-rotate-x, 0))) rotateY(var(--ha-tfx-rotate-y-hover, var(--ha-tfx-rotate-y, 0))) rotateZ(var(--ha-tfx-rotate-z-hover, var(--ha-tfx-rotate-z, 0)));
        transform: translate(var(--ha-tfx-translate-x-hover, var(--ha-tfx-translate-x, 0)), var(--ha-tfx-translate-y-hover, var(--ha-tfx-translate-y, 0))) scale(var(--ha-tfx-scale-x-hover, var(--ha-tfx-scale-x, 1)), var(--ha-tfx-scale-y-hover, var(--ha-tfx-scale-y, 1))) skew(var(--ha-tfx-skew-x-hover, var(--ha-tfx-skew-x, 0)), var(--ha-tfx-skew-y-hover, var(--ha-tfx-skew-y, 0))) rotateX(var(--ha-tfx-rotate-x-hover, var(--ha-tfx-rotate-x, 0))) rotateY(var(--ha-tfx-rotate-y-hover, var(--ha-tfx-rotate-y, 0))) rotateZ(var(--ha-tfx-rotate-z-hover, var(--ha-tfx-rotate-z, 0)))
      }

      .happy-addon>.elementor-widget-container {
        word-wrap: break-word;
        overflow-wrap: break-word;
        box-sizing: border-box
      }

      .happy-addon>.elementor-widget-container * {
        box-sizing: border-box
      }

      .happy-addon p:empty {
        display: none
      }

      .happy-addon .elementor-inline-editing {
        min-height: auto !important
      }

      .happy-addon-pro img {
        max-width: 100%;
        height: auto;
        -o-object-fit: cover;
        object-fit: cover
      }

      .ha-screen-reader-text {
        position: absolute;
        overflow: hidden;
        clip: rect(1px, 1px, 1px, 1px);
        margin: -1px;
        padding: 0;
        width: 1px;
        height: 1px;
        border: 0;
        word-wrap: normal !important;
        -webkit-clip-path: inset(50%);
        clip-path: inset(50%)
      }

      .ha-has-bg-overlay>.elementor-widget-container {
        position: relative;
        z-index: 1
      }

      .ha-has-bg-overlay>.elementor-widget-container:before {
        position: absolute;
        top: 0;
        left: 0;
        z-index: -1;
        width: 100%;
        height: 100%;
        content: ""
      }

      .ha-popup--is-enabled .ha-js-popup,
      .ha-popup--is-enabled .ha-js-popup img {
        cursor: -webkit-zoom-in !important;
        cursor: zoom-in !important
      }

      .mfp-wrap .mfp-arrow,
      .mfp-wrap .mfp-close {
        background-color: transparent
      }

      .mfp-wrap .mfp-arrow:focus,
      .mfp-wrap .mfp-close:focus {
        outline-width: thin
      }

      .ha-advanced-tooltip-enable {
        position: relative;
        cursor: pointer;
        --ha-tooltip-arrow-color: #000;
        --ha-tooltip-arrow-distance: 0
      }

      .ha-advanced-tooltip-enable .ha-advanced-tooltip-content {
        position: absolute;
        z-index: 999;
        display: none;
        padding: 5px 0;
        width: 120px;
        height: auto;
        border-radius: 6px;
        background-color: #000;
        color: #fff;
        text-align: center;
        opacity: 0
      }

      .ha-advanced-tooltip-enable .ha-advanced-tooltip-content::after {
        position: absolute;
        border-width: 5px;
        border-style: solid;
        content: ""
      }

      .ha-advanced-tooltip-enable .ha-advanced-tooltip-content.no-arrow::after {
        visibility: hidden
      }

      .ha-advanced-tooltip-enable .ha-advanced-tooltip-content.show {
        display: inline-block;
        opacity: 1
      }

      .ha-advanced-tooltip-enable.ha-advanced-tooltip-top .ha-advanced-tooltip-content,
      body[data-elementor-device-mode=tablet] .ha-advanced-tooltip-enable.ha-advanced-tooltip-tablet-top .ha-advanced-tooltip-content {
        top: unset;
        right: 0;
        bottom: calc(101% + var(--ha-tooltip-arrow-distance));
        left: 0;
        margin: 0 auto
      }

      .ha-advanced-tooltip-enable.ha-advanced-tooltip-top .ha-advanced-tooltip-content::after,
      body[data-elementor-device-mode=tablet] .ha-advanced-tooltip-enable.ha-advanced-tooltip-tablet-top .ha-advanced-tooltip-content::after {
        top: 100%;
        right: unset;
        bottom: unset;
        left: 50%;
        border-color: var(--ha-tooltip-arrow-color) transparent transparent;
        -webkit-transform: translateX(-50%);
        -ms-transform: translateX(-50%);
        transform: translateX(-50%)
      }

      .ha-advanced-tooltip-enable.ha-advanced-tooltip-bottom .ha-advanced-tooltip-content,
      body[data-elementor-device-mode=tablet] .ha-advanced-tooltip-enable.ha-advanced-tooltip-tablet-bottom .ha-advanced-tooltip-content {
        top: calc(101% + var(--ha-tooltip-arrow-distance));
        right: 0;
        bottom: unset;
        left: 0;
        margin: 0 auto
      }

      .ha-advanced-tooltip-enable.ha-advanced-tooltip-bottom .ha-advanced-tooltip-content::after,
      body[data-elementor-device-mode=tablet] .ha-advanced-tooltip-enable.ha-advanced-tooltip-tablet-bottom .ha-advanced-tooltip-content::after {
        top: unset;
        right: unset;
        bottom: 100%;
        left: 50%;
        border-color: transparent transparent var(--ha-tooltip-arrow-color);
        -webkit-transform: translateX(-50%);
        -ms-transform: translateX(-50%);
        transform: translateX(-50%)
      }

      .ha-advanced-tooltip-enable.ha-advanced-tooltip-left .ha-advanced-tooltip-content,
      body[data-elementor-device-mode=tablet] .ha-advanced-tooltip-enable.ha-advanced-tooltip-tablet-left .ha-advanced-tooltip-content {
        top: 50%;
        right: calc(101% + var(--ha-tooltip-arrow-distance));
        bottom: unset;
        left: unset;
        -webkit-transform: translateY(-50%);
        -ms-transform: translateY(-50%);
        transform: translateY(-50%)
      }

      .ha-advanced-tooltip-enable.ha-advanced-tooltip-left .ha-advanced-tooltip-content::after,
      body[data-elementor-device-mode=tablet] .ha-advanced-tooltip-enable.ha-advanced-tooltip-tablet-left .ha-advanced-tooltip-content::after {
        top: 50%;
        right: unset;
        bottom: unset;
        left: 100%;
        border-color: transparent transparent transparent var(--ha-tooltip-arrow-color);
        -webkit-transform: translateY(-50%);
        -ms-transform: translateY(-50%);
        transform: translateY(-50%)
      }

      .ha-advanced-tooltip-enable.ha-advanced-tooltip-right .ha-advanced-tooltip-content,
      body[data-elementor-device-mode=tablet] .ha-advanced-tooltip-enable.ha-advanced-tooltip-tablet-right .ha-advanced-tooltip-content {
        top: 50%;
        right: unset;
        bottom: unset;
        left: calc(101% + var(--ha-tooltip-arrow-distance));
        -webkit-transform: translateY(-50%);
        -ms-transform: translateY(-50%);
        transform: translateY(-50%)
      }

      .ha-advanced-tooltip-enable.ha-advanced-tooltip-right .ha-advanced-tooltip-content::after,
      body[data-elementor-device-mode=tablet] .ha-advanced-tooltip-enable.ha-advanced-tooltip-tablet-right .ha-advanced-tooltip-content::after {
        top: 50%;
        right: 100%;
        bottom: unset;
        left: unset;
        border-color: transparent var(--ha-tooltip-arrow-color) transparent transparent;
        -webkit-transform: translateY(-50%);
        -ms-transform: translateY(-50%);
        transform: translateY(-50%)
      }

      body[data-elementor-device-mode=mobile] .ha-advanced-tooltip-enable.ha-advanced-tooltip-mobile-top .ha-advanced-tooltip-content {
        top: unset;
        right: 0;
        bottom: calc(101% + var(--ha-tooltip-arrow-distance));
        left: 0;
        margin: 0 auto
      }

      body[data-elementor-device-mode=mobile] .ha-advanced-tooltip-enable.ha-advanced-tooltip-mobile-top .ha-advanced-tooltip-content::after {
        top: 100%;
        right: unset;
        bottom: unset;
        left: 50%;
        border-color: var(--ha-tooltip-arrow-color) transparent transparent;
        -webkit-transform: translateX(-50%);
        -ms-transform: translateX(-50%);
        transform: translateX(-50%)
      }

      body[data-elementor-device-mode=mobile] .ha-advanced-tooltip-enable.ha-advanced-tooltip-mobile-bottom .ha-advanced-tooltip-content {
        top: calc(101% + var(--ha-tooltip-arrow-distance));
        right: 0;
        bottom: unset;
        left: 0;
        margin: 0 auto
      }

      body[data-elementor-device-mode=mobile] .ha-advanced-tooltip-enable.ha-advanced-tooltip-mobile-bottom .ha-advanced-tooltip-content::after {
        top: unset;
        right: unset;
        bottom: 100%;
        left: 50%;
        border-color: transparent transparent var(--ha-tooltip-arrow-color);
        -webkit-transform: translateX(-50%);
        -ms-transform: translateX(-50%);
        transform: translateX(-50%)
      }

      body[data-elementor-device-mode=mobile] .ha-advanced-tooltip-enable.ha-advanced-tooltip-mobile-left .ha-advanced-tooltip-content {
        top: 50%;
        right: calc(101% + var(--ha-tooltip-arrow-distance));
        bottom: unset;
        left: unset;
        -webkit-transform: translateY(-50%);
        -ms-transform: translateY(-50%);
        transform: translateY(-50%)
      }

      body[data-elementor-device-mode=mobile] .ha-advanced-tooltip-enable.ha-advanced-tooltip-mobile-left .ha-advanced-tooltip-content::after {
        top: 50%;
        right: unset;
        bottom: unset;
        left: 100%;
        border-color: transparent transparent transparent var(--ha-tooltip-arrow-color);
        -webkit-transform: translateY(-50%);
        -ms-transform: translateY(-50%);
        transform: translateY(-50%)
      }

      body[data-elementor-device-mode=mobile] .ha-advanced-tooltip-enable.ha-advanced-tooltip-mobile-right .ha-advanced-tooltip-content {
        top: 50%;
        right: unset;
        bottom: unset;
        left: calc(101% + var(--ha-tooltip-arrow-distance));
        -webkit-transform: translateY(-50%);
        -ms-transform: translateY(-50%);
        transform: translateY(-50%)
      }

      body[data-elementor-device-mode=mobile] .ha-advanced-tooltip-enable.ha-advanced-tooltip-mobile-right .ha-advanced-tooltip-content::after {
        top: 50%;
        right: 100%;
        bottom: unset;
        left: unset;
        border-color: transparent var(--ha-tooltip-arrow-color) transparent transparent;
        -webkit-transform: translateY(-50%);
        -ms-transform: translateY(-50%);
        transform: translateY(-50%)
      }

      .elementor-616 .elementor-element.elementor-element-213c45d8>.elementor-container {
        max-width: 500px;
        min-height: 450px;
      }

      .elementor-616 .elementor-element.elementor-element-175d12ff.elementor-column>.elementor-column-wrap>.elementor-widget-wrap {
        justify-content: center;
      }

      .elementor-616 .elementor-element.elementor-element-175d12ff>.elementor-element-populated {
        transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
      }

      .elementor-616 .elementor-element.elementor-element-175d12ff>.elementor-element-populated>.elementor-background-overlay {
        transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
      }

      .elementor-616 .elementor-element.elementor-element-d8834be {
        text-align: center;
      }

      .elementor-616 .elementor-element.elementor-element-d8834be .elementor-heading-title {
        color: #333333;
        font-family: "Lato", Sans-serif;
        font-size: 16px;
        font-weight: 600;
      }

      .elementor-616 .elementor-element.elementor-element-d8834be>.elementor-widget-container {
        margin: 0em 0em 0.5em 0em;
      }

      .elementor-616 .elementor-element.elementor-element-3ce7eba0 {
        text-align: center;
      }

      .elementor-616 .elementor-element.elementor-element-3ce7eba0 .dce-barcode-svg #elements {
        fill: #333333 !important;
      }

      .elementor-616 .elementor-element.elementor-element-3ce7eba0 .dce-barcode-html>div {
        background-color: #333333 !important;
      }

      .elementor-616 .elementor-element.elementor-element-3ce7eba0 .dce-barcode {
        width: 50%;
        max-width: 50%;
      }

      .elementor-616 .elementor-element.elementor-element-3ce7eba0>.elementor-widget-container {
        margin: 0em 0em 1em 0em;
      }

      .elementor-616 .elementor-element.elementor-element-d89ac53 .elementor-heading-title {
        color: #333333;
        font-family: "Lato", Sans-serif;
        font-weight: 600;
        font-style: italic;
      }

      .elementor-616 .elementor-element.elementor-element-d89ac53>.elementor-widget-container {
        margin: 0em 0em 1em 0em;
      }

      .elementor-bc-flex-widget .elementor-616 .elementor-element.elementor-element-af203c5.elementor-column .elementor-column-wrap {
        align-items: center;
      }

      .elementor-616 .elementor-element.elementor-element-af203c5.elementor-column.elementor-element[data-element_type="column"]>.elementor-column-wrap.elementor-element-populated>.elementor-widget-wrap {
        align-content: center;
        align-items: center;
      }

      .elementor-616 .elementor-element.elementor-element-af203c5.elementor-column>.elementor-column-wrap>.elementor-widget-wrap {
        justify-content: center;
      }

      .elementor-616 .elementor-element.elementor-element-c717ebe .elementor-icon-wrapper {
        text-align: center;
      }

      .elementor-616 .elementor-element.elementor-element-c717ebe.elementor-view-stacked .elementor-icon {
        background-color: #FFFFFF;
        color: #333333;
      }

      .elementor-616 .elementor-element.elementor-element-c717ebe.elementor-view-framed .elementor-icon,
      .elementor-616 .elementor-element.elementor-element-c717ebe.elementor-view-default .elementor-icon {
        color: #FFFFFF;
        border-color: #FFFFFF;
      }

      .elementor-616 .elementor-element.elementor-element-c717ebe.elementor-view-framed .elementor-icon,
      .elementor-616 .elementor-element.elementor-element-c717ebe.elementor-view-default .elementor-icon svg {
        fill: #FFFFFF;
      }

      .elementor-616 .elementor-element.elementor-element-c717ebe.elementor-view-framed .elementor-icon {
        background-color: #333333;
      }

      .elementor-616 .elementor-element.elementor-element-c717ebe.elementor-view-stacked .elementor-icon svg {
        fill: #333333;
      }

      .elementor-616 .elementor-element.elementor-element-c717ebe .elementor-icon {
        font-size: 12px;
        padding: 6px;
        border-width: 0px 0px 0px 0px;
      }

      .elementor-616 .elementor-element.elementor-element-c717ebe .elementor-icon i,
      .elementor-616 .elementor-element.elementor-element-c717ebe .elementor-icon svg {
        transform: rotate(0deg);
      }

      .elementor-616 .elementor-element.elementor-element-c717ebe>.elementor-widget-container {
        margin: 0px 0px -6px 0px;
      }

      .elementor-616 .elementor-element.elementor-element-c717ebe {
        width: auto;
        max-width: auto;
      }

      .elementor-616 .elementor-element.elementor-element-8f2b67f {
        text-align: center;
        width: auto;
        max-width: auto;
      }

      .elementor-616 .elementor-element.elementor-element-8f2b67f .elementor-heading-title {
        color: #333333;
        font-family: "Nunito Sans", Sans-serif;
        font-size: 1.2em;
        font-weight: 600;
        letter-spacing: 2px;
      }

      .elementor-616 .elementor-element.elementor-element-8f2b67f>.elementor-widget-container {
        margin: 0em 0em 0em 0.5em;
      }

      #elementor-popup-modal-616 .dialog-message {
        width: 400px;
        height: auto;
      }

      #elementor-popup-modal-616 {
        justify-content: center;
        align-items: center;
        pointer-events: all;
        background-color: rgba(0, 0, 0, .8);
      }

      #elementor-popup-modal-616 .dialog-close-button {
        display: flex;
        background-color: #333333;
        font-size: 15px;
      }

      #elementor-popup-modal-616 .dialog-widget-content {
        animation-duration: 0.8s;
        border-radius: 4px 4px 4px 4px;
        box-shadow: 2px 8px 23px 3px rgba(0, 0, 0, 0.2);
      }

      #elementor-popup-modal-616 .dialog-close-button i {
        color: #FFFFFF;
      }

      #elementor-popup-modal-616 .dialog-close-button svg {
        fill: #FFFFFF;
      }

      #elementor-popup-modal-616 .dialog-close-button:hover i {
        color: #FFFFFF;
      }

      #elementor-popup-modal-616 .dialog-close-button:hover {
        background-color: #333333;
      }

      @media(max-width:767px) {
        .elementor-616 .elementor-element.elementor-element-213c45d8>.elementor-container {
          max-width: 320px;
          min-height: 400px;
        }

        .elementor-616 .elementor-element.elementor-element-175d12ff.elementor-column>.elementor-column-wrap>.elementor-widget-wrap {
          justify-content: center;
        }

        .elementor-616 .elementor-element.elementor-element-d8834be .elementor-heading-title {
          font-size: 16px;
        }

        .elementor-616 .elementor-element.elementor-element-d89ac53 {
          text-align: center;
        }

        .elementor-616 .elementor-element.elementor-element-d89ac53 .elementor-heading-title {
          font-size: 15px;
        }

        .elementor-616 .elementor-element.elementor-element-d89ac53>.elementor-widget-container {
          margin: 0px 0px 15px 0px;
        }

        .elementor-616 .elementor-element.elementor-element-8f2b67f {
          text-align: center;
        }

        .elementor-616 .elementor-element.elementor-element-8f2b67f .elementor-heading-title {
          line-height: 1.4em;
        }

        #elementor-popup-modal-616 .dialog-message {
          width: 320px;
          padding: 0px 0px 0px 0px;
        }

        #elementor-popup-modal-616 .dialog-widget-content {
          margin: 0px 0px 0px 0px;
        }
      }

      /* Start custom CSS */
      .dialog-close-button {
        padding: 4px;
        border-radius: 100%
      }

      /* End custom CSS */
    </style>
    <link rel='stylesheet' id='dashicons-css' href='wp-includes/css/dashicons.min.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='google-fonts-1-css' href='https://fonts.googleapis.com/css?family=Roboto%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRoboto+Slab%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CPoppins%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CNunito+Sans%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CLato%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&#038;display=auto&#038;ver=6.1.1' type='text/css' media='all' />
    <script type='text/javascript' id='jquery-core-js-extra'>
      // var pp = {
      //   "ajax_url": "https:\/\/asd.com\/wp-admin\/admin-ajax.php"
      // };
    </script>
    <script type='text/javascript' src='wp-includes/js/jquery/jquery.min.js?ver=3.6.1' id='jquery-core-js'></script>
    <script type='text/javascript' src='wp-includes/js/jquery/jquery-migrate.min.js?ver=3.3.2' id='jquery-migrate-js'></script>
    <script defer type='text/javascript' src='wp-content/cache/autoptimize/autoptimize_single_49cea0a781874a962879c2caca9bc322.js?ver=1.0.0' id='euis-js'></script>
    <script defer type='text/javascript' src='wp-content/plugins/elementor/assets/lib/font-awesome/js/v4-shims.min.js?ver=3.5.6' id='font-awesome-4-shim-js'></script>
    <link rel="https://api.w.org/" href="wp-json/" />
    <link rel="alternate" type="application/json" href="wp-json/wp/v2/posts/128245" />
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="xmlrpc.php?rsd" />
    <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="wp-includes/wlwmanifest.xml" />
    <meta name="generator" content="WordPress 6.1.1" />
    <link rel='shortlink' href='?p=128245' />
    <link rel="alternate" type="application/json+oembed" href="wp-json/oembed/1.0/embed?url=https%3A%2F%2Finviteams.com%2Firfan-vita%2F" />
    <link rel="alternate" type="text/xml+oembed" href="wp-json/oembed/1.0/embed?url=https%3A%2F%2Finviteams.com%2Firfan-vita%2F&#038;format=xml" />
    <script defer src="data:text/javascript;base64,KGZ1bmN0aW9uKHcsZCxzLGwsaSl7d1tsXT13W2xdfHxbXTt3W2xdLnB1c2goeydndG0uc3RhcnQnOg0KbmV3IERhdGUoKS5nZXRUaW1lKCksZXZlbnQ6J2d0bS5qcyd9KTt2YXIgZj1kLmdldEVsZW1lbnRzQnlUYWdOYW1lKHMpWzBdLA0Kaj1kLmNyZWF0ZUVsZW1lbnQocyksZGw9bCE9J2RhdGFMYXllcic/JyZsPScrbDonJztqLmFzeW5jPXRydWU7ai5zcmM9DQonaHR0cHM6Ly93d3cuZ29vZ2xldGFnbWFuYWdlci5jb20vZ3RtLmpzP2lkPScraStkbDtmLnBhcmVudE5vZGUuaW5zZXJ0QmVmb3JlKGosZik7DQp9KSh3aW5kb3csZG9jdW1lbnQsJ3NjcmlwdCcsJ2RhdGFMYXllcicsJ0dUTS1QUE5OTExIJyk7"></script>
    <meta name="color-scheme" content="light dark">
    <script defer src="data:text/javascript;base64,CiAgICAgICAgalF1ZXJ5KGZ1bmN0aW9uKCQpIHsKICAgICAgICAgICAgJCgiW25hbWU9J3ZpZXdwb3J0J10iKS5hdHRyKCdjb250ZW50JywgJ3dpZHRoPWRldmljZS13aWR0aCwgaW5pdGlhbC1zY2FsZT0xLjAsIG1heGltdW0tc2NhbGU9MS4wLCB1c2VyLXNjYWxhYmxlPW5vJyk7CiAgICAgICAgfSk7CiAgICA="></script>
    <link rel="icon" href="wp-content/uploads/2022/07/favicon-sage-150x150.png" sizes="32x32" />
    <link rel="icon" href="wp-content/uploads/2022/07/favicon-sage.png" sizes="192x192" />
    <link rel="apple-touch-icon" href="wp-content/uploads/2022/07/favicon-sage.png" />
    <meta name="msapplication-TileImage" content="wp-content/uploads/2022/07/favicon-sage.png" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover" />
  </head>
  <body data-rsssl=1 class="post-template post-template-elementor_canvas single single-post postid-128245 single-format-standard elementor-dce elementor-default elementor-template-canvas elementor-kit-5 elementor-page elementor-page-128245">
    <noscript>
      <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PPNNLLH" height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;">
      <defs>
        <filter id="wp-duotone-dark-grayscale">
          <feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
          <feComponentTransfer color-interpolation-filters="sRGB">
            <feFuncR type="table" tableValues="0 0.49803921568627" />
            <feFuncG type="table" tableValues="0 0.49803921568627" />
            <feFuncB type="table" tableValues="0 0.49803921568627" />
            <feFuncA type="table" tableValues="1 1" />
          </feComponentTransfer>
          <feComposite in2="SourceGraphic" operator="in" />
        </filter>
      </defs>
    </svg>
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;">
      <defs>
        <filter id="wp-duotone-grayscale">
          <feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
          <feComponentTransfer color-interpolation-filters="sRGB">
            <feFuncR type="table" tableValues="0 1" />
            <feFuncG type="table" tableValues="0 1" />
            <feFuncB type="table" tableValues="0 1" />
            <feFuncA type="table" tableValues="1 1" />
          </feComponentTransfer>
          <feComposite in2="SourceGraphic" operator="in" />
        </filter>
      </defs>
    </svg>
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;">
      <defs>
        <filter id="wp-duotone-purple-yellow">
          <feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
          <feComponentTransfer color-interpolation-filters="sRGB">
            <feFuncR type="table" tableValues="0.54901960784314 0.98823529411765" />
            <feFuncG type="table" tableValues="0 1" />
            <feFuncB type="table" tableValues="0.71764705882353 0.25490196078431" />
            <feFuncA type="table" tableValues="1 1" />
          </feComponentTransfer>
          <feComposite in2="SourceGraphic" operator="in" />
        </filter>
      </defs>
    </svg>
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;">
      <defs>
        <filter id="wp-duotone-blue-red">
          <feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
          <feComponentTransfer color-interpolation-filters="sRGB">
            <feFuncR type="table" tableValues="0 1" />
            <feFuncG type="table" tableValues="0 0.27843137254902" />
            <feFuncB type="table" tableValues="0.5921568627451 0.27843137254902" />
            <feFuncA type="table" tableValues="1 1" />
          </feComponentTransfer>
          <feComposite in2="SourceGraphic" operator="in" />
        </filter>
      </defs>
    </svg>
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;">
      <defs>
        <filter id="wp-duotone-midnight">
          <feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
          <feComponentTransfer color-interpolation-filters="sRGB">
            <feFuncR type="table" tableValues="0 0" />
            <feFuncG type="table" tableValues="0 0.64705882352941" />
            <feFuncB type="table" tableValues="0 1" />
            <feFuncA type="table" tableValues="1 1" />
          </feComponentTransfer>
          <feComposite in2="SourceGraphic" operator="in" />
        </filter>
      </defs>
    </svg>
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;">
      <defs>
        <filter id="wp-duotone-magenta-yellow">
          <feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
          <feComponentTransfer color-interpolation-filters="sRGB">
            <feFuncR type="table" tableValues="0.78039215686275 1" />
            <feFuncG type="table" tableValues="0 0.94901960784314" />
            <feFuncB type="table" tableValues="0.35294117647059 0.47058823529412" />
            <feFuncA type="table" tableValues="1 1" />
          </feComponentTransfer>
          <feComposite in2="SourceGraphic" operator="in" />
        </filter>
      </defs>
    </svg>
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;">
      <defs>
        <filter id="wp-duotone-purple-green">
          <feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
          <feComponentTransfer color-interpolation-filters="sRGB">
            <feFuncR type="table" tableValues="0.65098039215686 0.40392156862745" />
            <feFuncG type="table" tableValues="0 1" />
            <feFuncB type="table" tableValues="0.44705882352941 0.4" />
            <feFuncA type="table" tableValues="1 1" />
          </feComponentTransfer>
          <feComposite in2="SourceGraphic" operator="in" />
        </filter>
      </defs>
    </svg>
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;">
      <defs>
        <filter id="wp-duotone-blue-orange">
          <feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
          <feComponentTransfer color-interpolation-filters="sRGB">
            <feFuncR type="table" tableValues="0.098039215686275 1" />
            <feFuncG type="table" tableValues="0 0.66274509803922" />
            <feFuncB type="table" tableValues="0.84705882352941 0.41960784313725" />
            <feFuncA type="table" tableValues="1 1" />
          </feComponentTransfer>
          <feComposite in2="SourceGraphic" operator="in" />
        </filter>
      </defs>
    </svg>
    <div data-elementor-type="wp-post" data-elementor-id="128245" data-post-id="128245" data-obj-id="128245" class="elementor elementor-128245 dce-elementor-post-128245" data-elementor-settings="[]">
      <div class="elementor-inner">
        <div class="elementor-section-wrap">
          <section data-dce-background-overlay-color="#00000000" class="elementor-section elementor-top-section elementor-element elementor-element-513f728 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="513f728" data-element_type="section" id="s0" data-settings="{&quot;background_background&quot;:&quot;slideshow&quot;,&quot;background_slideshow_slide_duration&quot;:500,&quot;background_slideshow_gallery&quot;:[{&quot;id&quot;:128310,&quot;url&quot;:&quot;wp-content\/uploads\/2023\/01\/3.jpg&quot;}],&quot;sticky&quot;:&quot;top&quot;,&quot;background_slideshow_loop&quot;:&quot;yes&quot;,&quot;background_slideshow_slide_transition&quot;:&quot;fade&quot;,&quot;background_slideshow_transition_duration&quot;:500,&quot;_ha_eqh_enable&quot;:false,&quot;sticky_on&quot;:[&quot;desktop&quot;,&quot;tablet&quot;,&quot;mobile&quot;],&quot;sticky_offset&quot;:0,&quot;sticky_effects_offset&quot;:0}">
            <div class="elementor-background-overlay"></div>
            <div class="elementor-container elementor-column-gap-default">
              <div class="elementor-row">
                <div data-dce-background-overlay-color="#00000000" class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-9da6ff9 wdp-sticky-section-no" data-id="9da6ff9" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                  <div class="elementor-column-wrap elementor-element-populated">
                    <div class="elementor-background-overlay"></div>
                    <div class="elementor-widget-wrap">
                      <section class="elementor-section elementor-inner-section elementor-element elementor-element-ed84c9e elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="ed84c9e" data-element_type="section" data-settings="{&quot;_ha_eqh_enable&quot;:false}">
                        <div class="elementor-container elementor-column-gap-default">
                          <div class="elementor-row">
                            <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-e84aeaa wdp-sticky-section-no" data-id="e84aeaa" data-element_type="column">
                              <div class="elementor-column-wrap elementor-element-populated">
                                <div class="elementor-widget-wrap">
                                  <div class="elementor-element elementor-element-6cafba5 animated-slow elementor-widget__width-auto wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-text-path" data-id="6cafba5" data-element_type="widget" data-settings="{&quot;text&quot;:&quot;THE WEDDING OF&quot;,&quot;align&quot;:&quot;center&quot;,&quot;start_point&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:10,&quot;sizes&quot;:[]},&quot;_animation&quot;:&quot;zoomIn&quot;,&quot;link&quot;:{&quot;url&quot;:&quot;&quot;,&quot;is_external&quot;:&quot;&quot;,&quot;nofollow&quot;:&quot;&quot;,&quot;custom_attributes&quot;:&quot;&quot;}}" data-widget_type="text-path.default">
                                    <div class="elementor-widget-container">
                                      <div class="e-text-path" data-text="THE WEDDING OF" data-url="wp-content/plugins/elementor/assets/svg-paths/circle.svg"></div>
                                    </div>
                                  </div>
                                  <div class="elementor-element elementor-element-2a8b858 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="2a8b858" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
                                    <div class="elementor-widget-container">
                                      <h2 class="elementor-heading-title elementor-size-default">Abdan & Uke</h2>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </section>
                      <div class="elementor-element elementor-element-7d4475a animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="7d4475a" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInDown&quot;,&quot;_animation_delay&quot;:400}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">DEAR</p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-dcfcf52 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="dcfcf52" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInDown&quot;,&quot;_animation_delay&quot;:800}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default"><?= ucfirst(strtolower($requestName)) ?></p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-447df93 elementor-align-center animated-slow elementor-widget__width-auto wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-button" data-id="447df93" data-element_type="widget" id="b_o" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;,&quot;_animation_delay&quot;:1200}" data-widget_type="button.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-button-wrapper">
                            <a class="elementor-button elementor-size-sm elementor-animation-shrink" role="button">
                              <span class="elementor-button-content-wrapper">
                                <span class="elementor-button-icon elementor-align-icon-left">
                                  <i aria-hidden="true" class="fas fa-envelope-open"></i>
                                </span>
                                <span class="elementor-button-text">Buka Undangan</span>
                              </span>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <section data-dce-background-color="#F8F8F8" class="elementor-section elementor-top-section elementor-element elementor-element-34a8077 animated-slow elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no elementor-invisible" data-id="34a8077" data-element_type="section" id="content" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;background_background&quot;:&quot;classic&quot;,&quot;_ha_eqh_enable&quot;:false}">
            <div class="elementor-background-overlay"></div>
            <div class="elementor-container elementor-column-gap-no">
              <div class="elementor-row">
                <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-b5d8cb8 wdp-sticky-section-no" data-id="b5d8cb8" data-element_type="column">
                  <div class="elementor-column-wrap elementor-element-populated">
                    <div class="elementor-widget-wrap">
                      <section data-dce-background-overlay-color="#000000" class="elementor-section elementor-inner-section elementor-element elementor-element-4a0a9f7 elementor-section-height-min-height elementor-section-boxed elementor-section-height-default wdp-sticky-section-no" data-id="4a0a9f7" data-element_type="section" id="cover" data-settings="{&quot;background_background&quot;:&quot;slideshow&quot;,&quot;background_slideshow_gallery&quot;:[{&quot;id&quot;:128330,&quot;url&quot;:&quot;wp-content\/uploads\/2023\/01\/9.jpg&quot;},{&quot;id&quot;:130097,&quot;url&quot;:&quot;wp-content\/uploads\/2023\/01\/4.jpg&quot;}],&quot;background_slideshow_slide_duration&quot;:2000,&quot;background_slideshow_transition_duration&quot;:2000,&quot;background_slideshow_ken_burns&quot;:&quot;yes&quot;,&quot;background_slideshow_ken_burns_zoom_direction&quot;:&quot;out&quot;,&quot;background_slideshow_loop&quot;:&quot;yes&quot;,&quot;background_slideshow_slide_transition&quot;:&quot;fade&quot;,&quot;_ha_eqh_enable&quot;:false}">
                        <div class="elementor-background-overlay"></div>
                        <div class="elementor-container elementor-column-gap-default">
                          <div class="elementor-row">
                            <div data-dce-background-overlay-color="#00000000" class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-3967eea wdp-sticky-section-no" data-id="3967eea" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                              <div class="elementor-column-wrap elementor-element-populated">
                                <div class="elementor-background-overlay"></div>
                                <div class="elementor-widget-wrap">
                                  <section class="elementor-section elementor-inner-section elementor-element elementor-element-c65c10a elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="c65c10a" data-element_type="section" data-settings="{&quot;_ha_eqh_enable&quot;:false}">
                                    <div class="elementor-container elementor-column-gap-default">
                                      <div class="elementor-row">
                                        <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-8104037 wdp-sticky-section-no" data-id="8104037" data-element_type="column">
                                          <div class="elementor-column-wrap elementor-element-populated">
                                            <div class="elementor-widget-wrap">
                                              <div class="elementor-element elementor-element-f73ce84 elementor-widget__width-auto animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="f73ce84" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
                                                <div class="elementor-widget-container">
                                                  <h2 class="elementor-heading-title elementor-size-default">THE <span style="font-weight: 500;font-style: italic;">WEDDING OF</span>
                                                  </h2>
                                                </div>
                                              </div>
                                              <div class="elementor-element elementor-element-6ad7d31 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="6ad7d31" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-widget_type="heading.default">
                                                <div class="elementor-widget-container">
                                                  <h2 class="elementor-heading-title elementor-size-default">ABDAN & UKE</h2>
                                                </div>
                                              </div>
                                              <div class="elementor-element elementor-element-17e43e4 elementor-widget__width-auto animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="17e43e4" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInLeft&quot;}" data-widget_type="heading.default">
                                                <div class="elementor-widget-container">
                                                  <h2 class="elementor-heading-title elementor-size-default">
                                                    <a href="https://www.google.com/calendar/render?action=TEMPLATE&#038;text=Pernikahan%20Irfan%20&#038;%20Vita&#038;details&#038;dates=20230204T070000/20230204T133000&#038;location" target="_blank" rel="noopener">SAVE THE DATE</a>
                                                  </h2>
                                                </div>
                                              </div>
                                              <div class="elementor-element elementor-element-6664a5c elementor-widget__width-auto animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="6664a5c" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInRight&quot;}" data-widget_type="heading.default">
                                                <div class="elementor-widget-container">
                                                  <h2 class="elementor-heading-title elementor-size-default">| 19 . 02 . 2023</h2>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </section>
                                  <div class="elementor-element elementor-element-d3cf127 animated-slow elementor-countdown--label-block wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-countdown" data-id="d3cf127" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-widget_type="countdown.default">
                                    <div class="elementor-widget-container">
                                      <div class="elementor-countdown-wrapper" data-date="1676800800">
                                        <div class="elementor-countdown-item">
                                          <span class="elementor-countdown-digits elementor-countdown-days"></span>
                                          <span class="elementor-countdown-label">Days</span>
                                        </div>
                                        <div class="elementor-countdown-item">
                                          <span class="elementor-countdown-digits elementor-countdown-hours"></span>
                                          <span class="elementor-countdown-label">Hours</span>
                                        </div>
                                        <div class="elementor-countdown-item">
                                          <span class="elementor-countdown-digits elementor-countdown-minutes"></span>
                                          <span class="elementor-countdown-label">Minutes</span>
                                        </div>
                                        <div class="elementor-countdown-item">
                                          <span class="elementor-countdown-digits elementor-countdown-seconds"></span>
                                          <span class="elementor-countdown-label">Seconds</span>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="elementor-element elementor-element-2b2552b elementor-align-center wdp-sticky-section-no elementor-widget elementor-widget-lottie" data-id="2b2552b" data-element_type="widget" data-settings="{&quot;source_json&quot;:{&quot;url&quot;:&quot;wp-content\/uploads\/2022\/12\/96514-scroll-down-off-white.json&quot;,&quot;id&quot;:119784,&quot;alt&quot;:&quot;&quot;,&quot;source&quot;:&quot;library&quot;},&quot;loop&quot;:&quot;yes&quot;,&quot;source&quot;:&quot;media_file&quot;,&quot;caption_source&quot;:&quot;none&quot;,&quot;link_to&quot;:&quot;none&quot;,&quot;trigger&quot;:&quot;arriving_to_viewport&quot;,&quot;viewport&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:{&quot;start&quot;:0,&quot;end&quot;:100}},&quot;play_speed&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:1,&quot;sizes&quot;:[]},&quot;start_point&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:&quot;0&quot;,&quot;sizes&quot;:[]},&quot;end_point&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:&quot;100&quot;,&quot;sizes&quot;:[]},&quot;renderer&quot;:&quot;svg&quot;}" data-widget_type="lottie.default">
                                    <div class="elementor-widget-container">
                                      <div class="e-lottie__container">
                                        <div class="e-lottie__animation"></div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </section>
                      <section data-dce-background-color="#EDEDED" class="elementor-section elementor-inner-section elementor-element elementor-element-663bb43 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="663bb43" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;_ha_eqh_enable&quot;:false}">
                        <div class="elementor-container elementor-column-gap-default">
                          <div class="elementor-row">
                            <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-35e80ce wdp-sticky-section-no" data-id="35e80ce" data-element_type="column">
                              <div class="elementor-column-wrap elementor-element-populated">
                                <div class="elementor-widget-wrap">
                                  <div class="elementor-element elementor-element-0765357 elementor-widget__width-auto elementor-absolute dce_masking-none wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="0765357" data-element_type="widget" data-settings="{&quot;_position&quot;:&quot;absolute&quot;}" data-widget_type="image.default">
                                    <div class="elementor-widget-container">
                                      <div class="elementor-image">
                                        <img decoding="async" width="700" height="540" src="wp-content/uploads/2022/07/img-tree-shadow-01.png" class="attachment-full size-full" alt="" loading="lazy" srcset="wp-content/uploads/2022/07/img-tree-shadow-01.png 700w, wp-content/uploads/2022/07/img-tree-shadow-01-300x231.png 300w" sizes="(max-width: 700px) 100vw, 700px" />
                                      </div>
                                    </div>
                                  </div>
                                  <section class="elementor-section elementor-inner-section elementor-element elementor-element-5eb7525 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no elementor-invisible" data-id="5eb7525" data-element_type="section" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;_ha_eqh_enable&quot;:false}">
                                    <div class="elementor-container elementor-column-gap-default">
                                      <div class="elementor-row">
                                        <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-598fcc0 wdp-sticky-section-no" data-id="598fcc0" data-element_type="column">
                                          <div class="elementor-column-wrap elementor-element-populated">
                                            <div class="elementor-widget-wrap">
                                              <div class="elementor-element elementor-element-13d8394 elementor-widget__width-auto wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="13d8394" data-element_type="widget" data-widget_type="heading.default">
                                                <div class="elementor-widget-container">
                                                  <h2 class="elementor-heading-title elementor-size-default">X</h2>
                                                </div>
                                              </div>
                                              <div class="elementor-element elementor-element-0cd1e16 e-transform elementor-widget__width-auto elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="0cd1e16" data-element_type="widget" data-settings="{&quot;_transform_rotateZ_effect&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:90,&quot;sizes&quot;:[]},&quot;_transform_rotateZ_effect_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;_transform_rotateZ_effect_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]}}" data-widget_type="divider.default">
                                                <div class="elementor-widget-container">
                                                  <div class="elementor-divider">
                                                    <span class="elementor-divider-separator"></span>
                                                  </div>
                                                </div>
                                              </div>
                                              <div class="elementor-element elementor-element-9fb2d43 elementor-widget__width-auto wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="9fb2d43" data-element_type="widget" data-widget_type="heading.default">
                                                <div class="elementor-widget-container">
                                                  <h2 class="elementor-heading-title elementor-size-default">X</h2>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </section>
                                  <div class="elementor-element elementor-element-71f739f animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="71f739f" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                                    <div class="elementor-widget-container">
                                      <h2 class="elementor-heading-title elementor-size-default">HR Abu Ya'la</h2>
                                    </div>
                                  </div>
                                  <div class="elementor-element elementor-element-666d668 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="666d668" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                                    <div class="elementor-widget-container">
                                      <h2 class="elementor-heading-title elementor-size-default">"Menikah adalah ibadah terpanjang dan terlama karena dengan menikah engkau akan menyempurnakan setengah perjalanan agama dan kehidupanmu. Siapa yang menikah maka sungguh ia telah diberi setengahnya ibadah."</h2>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </section>
                      <section class="elementor-section elementor-inner-section elementor-element elementor-element-6b182d4 elementor-section-height-min-height animated-slow elementor-section-boxed elementor-section-height-default wdp-sticky-section-no elementor-invisible" data-id="6b182d4" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;animation&quot;:&quot;fadeIn&quot;,&quot;_ha_eqh_enable&quot;:false}">
                        <div class="elementor-container elementor-column-gap-default">
                          <div class="elementor-row">
                            <div data-dce-background-image-url="wp-content/uploads/2023/01/men.jpg" class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-d46a144 wdp-sticky-section-no" data-id="d46a144" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                              <div class="elementor-column-wrap elementor-element-populated">
                                <div class="elementor-widget-wrap">
                                  <div class="elementor-element elementor-element-ddd5b51 animated-slow elementor-absolute elementor-widget__width-initial wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="ddd5b51" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:400,&quot;_position&quot;:&quot;absolute&quot;}" data-widget_type="heading.default">
                                    <div class="elementor-widget-container">
                                      <h2 class="elementor-heading-title elementor-size-default">STRONGER</h2>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-a5b8dba wdp-sticky-section-no" data-id="a5b8dba" data-element_type="column">
                              <div class="elementor-column-wrap">
                                <div class="elementor-widget-wrap"></div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </section>
                      <section data-dce-background-color="#F8F8F8" class="elementor-section elementor-inner-section elementor-element elementor-element-e44369e elementor-section-height-min-height elementor-section-boxed elementor-section-height-default wdp-sticky-section-no" data-id="e44369e" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;_ha_eqh_enable&quot;:false}">
                        <div class="elementor-container elementor-column-gap-default">
                          <div class="elementor-row">
                            <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-4e05e79 wdp-sticky-section-no" data-id="4e05e79" data-element_type="column">
                              <div class="elementor-column-wrap elementor-element-populated">
                                <div class="elementor-widget-wrap">
                                  <div class="elementor-element elementor-element-35e168a animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="35e168a" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                                    <div class="elementor-widget-container">
                                      <h2 class="elementor-heading-title elementor-size-default">Abdan Ahkamudin</h2>
                                    </div>
                                  </div>
                                  <div class="elementor-element elementor-element-44ca2c9 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="44ca2c9" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                                    <div class="elementor-widget-container">
                                      <h2 class="elementor-heading-title elementor-size-default">Putra Dari :</h2>
                                    </div>
                                  </div>
                                  <div class="elementor-element elementor-element-0156c7b wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="0156c7b" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                                    <div class="elementor-widget-container">
                                      <h2 class="elementor-heading-title elementor-size-default">Bpk.achmad heryansahdin dan Alm.ibu rumanah</h2>
                                    </div>
                                  </div>
                                  <div class="elementor-element elementor-element-56d9e2d elementor-widget__width-auto animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-button" data-id="56d9e2d" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="button.default">
                                    <div class="elementor-widget-container">
                                      <div class="elementor-button-wrapper">
                                        <a href="https://instagram.com/onepiece_abe" class="elementor-button-link elementor-button elementor-size-sm" role="button">
                                          <span class="elementor-button-content-wrapper">
                                            <span class="elementor-button-icon elementor-align-icon-left">
                                              <i aria-hidden="true" class="fab fa-instagram"></i>
                                            </span>
                                            <span class="elementor-button-text">INSTAGRAM</span>
                                          </span>
                                        </a>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </section>
                      <section class="elementor-section elementor-inner-section elementor-element elementor-element-b92dacd elementor-section-height-min-height animated-slow elementor-section-boxed elementor-section-height-default wdp-sticky-section-no elementor-invisible" data-id="b92dacd" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;animation&quot;:&quot;fadeIn&quot;,&quot;_ha_eqh_enable&quot;:false}">
                        <div class="elementor-container elementor-column-gap-default">
                          <div class="elementor-row">
                            <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-bc50505 wdp-sticky-section-no" data-id="bc50505" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                              <div class="elementor-column-wrap">
                                <div class="elementor-widget-wrap"></div>
                              </div>
                            </div>
                            <div data-dce-background-image-url="wp-content/uploads/2023/01/women.jpg" class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-31d0772 wdp-sticky-section-no" data-id="31d0772" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                              <div class="elementor-column-wrap elementor-element-populated">
                                <div class="elementor-widget-wrap">
                                  <div class="elementor-element elementor-element-d475a39 animated-slow elementor-absolute elementor-widget__width-initial wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="d475a39" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:400,&quot;_position&quot;:&quot;absolute&quot;}" data-widget_type="heading.default">
                                    <div class="elementor-widget-container">
                                      <h2 class="elementor-heading-title elementor-size-default">SOFTNESS</h2>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </section>
                      <section data-dce-background-color="#F8F8F8" class="elementor-section elementor-inner-section elementor-element elementor-element-62175bc elementor-section-height-min-height elementor-section-boxed elementor-section-height-default wdp-sticky-section-no" data-id="62175bc" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;gradient&quot;,&quot;_ha_eqh_enable&quot;:false}">
                        <div class="elementor-container elementor-column-gap-default">
                          <div class="elementor-row">
                            <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-9bedd01 wdp-sticky-section-no" data-id="9bedd01" data-element_type="column">
                              <div class="elementor-column-wrap elementor-element-populated">
                                <div class="elementor-widget-wrap">
                                  <div class="elementor-element elementor-element-9dedd2c animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="9dedd2c" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                                    <div class="elementor-widget-container">
                                      <h2 class="elementor-heading-title elementor-size-default">Uke Arumbini</h2>
                                    </div>
                                  </div>
                                  <div class="elementor-element elementor-element-5e0f78b animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="5e0f78b" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                                    <div class="elementor-widget-container">
                                      <h2 class="elementor-heading-title elementor-size-default">Putri Dari :</h2>
                                    </div>
                                  </div>
                                  <div class="elementor-element elementor-element-1cf51fb animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="1cf51fb" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                                    <div class="elementor-widget-container">
                                      <h2 class="elementor-heading-title elementor-size-default">Bpk.Suyan dan Ibu Samsini</h2>
                                    </div>
                                  </div>
                                  <div class="elementor-element elementor-element-b0c8d5e elementor-widget__width-auto animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-button" data-id="b0c8d5e" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="button.default">
                                    <div class="elementor-widget-container">
                                      <div class="elementor-button-wrapper">
                                        <a href="https://instagram.com/ukearum" class="elementor-button-link elementor-button elementor-size-sm" role="button">
                                          <span class="elementor-button-content-wrapper">
                                            <span class="elementor-button-icon elementor-align-icon-left">
                                              <i aria-hidden="true" class="fab fa-instagram"></i>
                                            </span>
                                            <span class="elementor-button-text">INSTAGRAM</span>
                                          </span>
                                        </a>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </section>
                      <section data-dce-background-overlay-color="#F8F8F8" class="elementor-section elementor-inner-section elementor-element elementor-element-c92e288 elementor-section-height-min-height elementor-section-boxed elementor-section-height-default wdp-sticky-section-no" data-id="c92e288" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;slideshow&quot;,&quot;background_slideshow_gallery&quot;:[{&quot;id&quot;:128329,&quot;url&quot;:&quot;wp-content\/uploads\/2023\/01\/3.jpg&quot;},{&quot;id&quot;:128330,&quot;url&quot;:&quot;wp-content\/uploads\/2023\/01\/9.jpg&quot;}],&quot;background_slideshow_slide_duration&quot;:2500,&quot;background_slideshow_slide_transition&quot;:&quot;slide_up&quot;,&quot;background_slideshow_transition_duration&quot;:12000,&quot;background_slideshow_loop&quot;:&quot;yes&quot;,&quot;_ha_eqh_enable&quot;:false}">
                        <div class="elementor-background-overlay"></div>
                        <div class="elementor-container elementor-column-gap-default">
                          <div class="elementor-row">
                            <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-ae0ff4e wdp-sticky-section-no" data-id="ae0ff4e" data-element_type="column">
                              <div class="elementor-column-wrap elementor-element-populated">
                                <div class="elementor-widget-wrap">
                                  <div class="elementor-element elementor-element-64a3ba8 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="64a3ba8" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
                                    <div class="elementor-widget-container">
                                      <h2 class="elementor-heading-title elementor-size-default">Wedding</h2>
                                    </div>
                                  </div>
                                  <div class="elementor-element elementor-element-f03ebad animated-slow elementor-widget-divider--view-line wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-divider" data-id="f03ebad" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInRight&quot;}" data-widget_type="divider.default">
                                    <div class="elementor-widget-container">
                                      <div class="elementor-divider">
                                        <span class="elementor-divider-separator"></span>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="elementor-element elementor-element-e521c8f animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="e521c8f" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                                    <div class="elementor-widget-container">
                                      <h2 class="elementor-heading-title elementor-size-default">Event</h2>
                                    </div>
                                  </div>
                                  <section data-dce-background-color="#FFFFFF" class="elementor-section elementor-inner-section elementor-element elementor-element-d43cd28 animated-slow elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no elementor-invisible" data-id="d43cd28" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;animation&quot;:&quot;fadeInUp&quot;,&quot;_ha_eqh_enable&quot;:false}">
                                    <div class="elementor-container elementor-column-gap-default">
                                      <div class="elementor-row">
                                        <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-44aacf7 wdp-sticky-section-no" data-id="44aacf7" data-element_type="column">
                                          <div class="elementor-column-wrap elementor-element-populated">
                                            <div class="elementor-widget-wrap">
                                              <section class="elementor-section elementor-inner-section elementor-element elementor-element-c8f738e elementor-section-height-min-height elementor-section-boxed elementor-section-height-default wdp-sticky-section-no" data-id="c8f738e" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;_ha_eqh_enable&quot;:false}">
                                                <div class="elementor-container elementor-column-gap-default">
                                                  <div class="elementor-row">
                                                    <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-ab06960 wdp-sticky-section-no" data-id="ab06960" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;slideshow&quot;,&quot;background_slideshow_gallery&quot;:[{&quot;id&quot;:128316,&quot;url&quot;:&quot;wp-content\/uploads\/2023\/01\/left-1.jpg&quot;},{&quot;id&quot;:128317,&quot;url&quot;:&quot;wp-content\/uploads\/2023\/01\/left-2.jpg&quot;}],&quot;background_slideshow_slide_duration&quot;:2000,&quot;background_slideshow_slide_transition&quot;:&quot;slide_left&quot;,&quot;background_slideshow_transition_duration&quot;:4000,&quot;background_slideshow_loop&quot;:&quot;yes&quot;}">
                                                      <div class="elementor-column-wrap">
                                                        <div class="elementor-widget-wrap"></div>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                              </section>
                                              <section class="elementor-section elementor-inner-section elementor-element elementor-element-697a9c5 elementor-section-height-min-height elementor-section-boxed elementor-section-height-default wdp-sticky-section-no" data-id="697a9c5" data-element_type="section" data-settings="{&quot;_ha_eqh_enable&quot;:false}">
                                                <div class="elementor-container elementor-column-gap-default">
                                                  <div class="elementor-row">
                                                    <div data-dce-background-color="#5E5E5E" class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-7f27de5 wdp-sticky-section-no" data-id="7f27de5" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                                                      <div class="elementor-column-wrap elementor-element-populated">
                                                        <div class="elementor-widget-wrap">
                                                          <div class="elementor-element elementor-element-6ac62a0 e-transform elementor-widget__width-auto wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="6ac62a0" data-element_type="widget" data-settings="{&quot;_transform_rotateZ_effect&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:-90,&quot;sizes&quot;:[]},&quot;_transform_rotateZ_effect_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;_transform_rotateZ_effect_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]}}" data-widget_type="heading.default">
                                                            <div class="elementor-widget-container">
                                                              <h2 class="elementor-heading-title elementor-size-default">Akad Nikah</h2>
                                                            </div>
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </div>
                                                    <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-f5a3804 wdp-sticky-section-no" data-id="f5a3804" data-element_type="column">
                                                      <div class="elementor-column-wrap elementor-element-populated">
                                                        <div class="elementor-widget-wrap">
                                                          <section class="elementor-section elementor-inner-section elementor-element elementor-element-c4d6feb elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="c4d6feb" data-element_type="section" data-settings="{&quot;_ha_eqh_enable&quot;:false}">
                                                            <div class="elementor-container elementor-column-gap-default">
                                                              <div class="elementor-row">
                                                                <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-ee784a5 wdp-sticky-section-no" data-id="ee784a5" data-element_type="column">
                                                                  <div class="elementor-column-wrap elementor-element-populated">
                                                                    <div class="elementor-widget-wrap">
                                                                      <div class="elementor-element elementor-element-029d5a9 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="029d5a9" data-element_type="widget" data-widget_type="heading.default">
                                                                        <div class="elementor-widget-container">
                                                                          <h2 class="elementor-heading-title elementor-size-default">19</h2>
                                                                        </div>
                                                                      </div>
                                                                    </div>
                                                                  </div>
                                                                </div>
                                                                <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-91caa13 wdp-sticky-section-no" data-id="91caa13" data-element_type="column">
                                                                  <div class="elementor-column-wrap elementor-element-populated">
                                                                    <div class="elementor-widget-wrap">
                                                                      <div class="elementor-element elementor-element-0760287 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="0760287" data-element_type="widget" data-widget_type="heading.default">
                                                                        <div class="elementor-widget-container">
                                                                          <h2 class="elementor-heading-title elementor-size-default">Minggu</h2>
                                                                        </div>
                                                                      </div>
                                                                      <div class="elementor-element elementor-element-4fb147c wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="4fb147c" data-element_type="widget" data-widget_type="heading.default">
                                                                        <div class="elementor-widget-container">
                                                                          <h2 class="elementor-heading-title elementor-size-default">Februari</h2>
                                                                        </div>
                                                                      </div>
                                                                      <div class="elementor-element elementor-element-376027b wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="376027b" data-element_type="widget" data-widget_type="heading.default">
                                                                        <div class="elementor-widget-container">
                                                                          <h2 class="elementor-heading-title elementor-size-default">2023</h2>
                                                                        </div>
                                                                      </div>
                                                                    </div>
                                                                  </div>
                                                                </div>
                                                              </div>
                                                            </div>
                                                          </section>
                                                          <div class="elementor-element elementor-element-56c487c elementor-icon-list--layout-traditional elementor-list-item-link-full_width wdp-sticky-section-no elementor-widget elementor-widget-icon-list" data-id="56c487c" data-element_type="widget" data-widget_type="icon-list.default">
                                                            <div class="elementor-widget-container">
                                                              <ul class="elementor-icon-list-items">
                                                                <li class="elementor-icon-list-item">
                                                                  <span class="elementor-icon-list-icon">
                                                                    <i aria-hidden="true" class="fas fa-clock"></i>
                                                                  </span>
                                                                  <span class="elementor-icon-list-text">09.00 - 10.00 WIB</span>
                                                                </li>
                                                              </ul>
                                                            </div>
                                                          </div>
                                                          <div class="elementor-element elementor-element-926d8b2 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="926d8b2" data-element_type="widget" data-widget_type="heading.default">
                                                            <div class="elementor-widget-container">
                                                              <h2 class="elementor-heading-title elementor-size-default">Lokasi Acara</h2>
                                                            </div>
                                                          </div>
                                                          <div class="elementor-element elementor-element-65eec21 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="65eec21" data-element_type="widget" data-widget_type="heading.default">
                                                            <div class="elementor-widget-container">
                                                              <h2 class="elementor-heading-title elementor-size-default">
                                                                <b>Rumah mempelai puteri <br>
                                                                </b>Desa Kalisube , <br>Banyumas, <br>Jawa Tengah
                                                              </h2>
                                                            </div>
                                                          </div>
                                                          <div class="elementor-element elementor-element-2fb5f40 elementor-widget__width-auto wdp-sticky-section-no elementor-widget elementor-widget-button" data-id="2fb5f40" data-element_type="widget" data-widget_type="button.default">
                                                            <div class="elementor-widget-container">
                                                              <div class="elementor-button-wrapper">
                                                                <a href="https://goo.gl/maps/vb7k5WhEnekS2rVYA" class="elementor-button-link elementor-button elementor-size-sm" role="button">
                                                                  <span class="elementor-button-content-wrapper">
                                                                    <span class="elementor-button-text">GOOGLE MAPS</span>
                                                                  </span>
                                                                </a>
                                                              </div>
                                                            </div>
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                              </section>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </section>
                                  <section data-dce-background-color="#FFFFFF" class="elementor-section elementor-inner-section elementor-element elementor-element-feeb831 animated-slow elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no elementor-invisible" data-id="feeb831" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;animation&quot;:&quot;fadeInUp&quot;,&quot;_ha_eqh_enable&quot;:false}">
                                    <div class="elementor-container elementor-column-gap-default">
                                      <div class="elementor-row">
                                        <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-8bae223 wdp-sticky-section-no" data-id="8bae223" data-element_type="column">
                                          <div class="elementor-column-wrap elementor-element-populated">
                                            <div class="elementor-widget-wrap">
                                              <section class="elementor-section elementor-inner-section elementor-element elementor-element-d89b3fe elementor-section-height-min-height elementor-section-boxed elementor-section-height-default wdp-sticky-section-no" data-id="d89b3fe" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;_ha_eqh_enable&quot;:false}">
                                                <div class="elementor-container elementor-column-gap-default">
                                                  <div class="elementor-row">
                                                    <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-629e426 wdp-sticky-section-no" data-id="629e426" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;slideshow&quot;,&quot;background_slideshow_gallery&quot;:[{&quot;id&quot;:128311,&quot;url&quot;:&quot;wp-content\/uploads\/2023\/01\/left-2.jpg&quot;},{&quot;id&quot;:128312,&quot;url&quot;:&quot;wp-content\/uploads\/2023\/01\/left-1.jpg&quot;}],&quot;background_slideshow_slide_duration&quot;:2000,&quot;background_slideshow_slide_transition&quot;:&quot;slide_right&quot;,&quot;background_slideshow_transition_duration&quot;:4000,&quot;background_slideshow_loop&quot;:&quot;yes&quot;}">
                                                      <div class="elementor-column-wrap">
                                                        <div class="elementor-widget-wrap"></div>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                              </section>
                                              <section class="elementor-section elementor-inner-section elementor-element elementor-element-7323c99 elementor-section-height-min-height elementor-section-boxed elementor-section-height-default wdp-sticky-section-no" data-id="7323c99" data-element_type="section" data-settings="{&quot;_ha_eqh_enable&quot;:false}">
                                                <div class="elementor-container elementor-column-gap-default">
                                                  <div class="elementor-row">
                                                    <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-37f8b28 wdp-sticky-section-no" data-id="37f8b28" data-element_type="column">
                                                      <div class="elementor-column-wrap elementor-element-populated">
                                                        <div class="elementor-widget-wrap">
                                                          <section class="elementor-section elementor-inner-section elementor-element elementor-element-d7c932d elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="d7c932d" data-element_type="section" data-settings="{&quot;_ha_eqh_enable&quot;:false}">
                                                            <div class="elementor-container elementor-column-gap-default">
                                                              <div class="elementor-row">
                                                                <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-c9ad365 wdp-sticky-section-no" data-id="c9ad365" data-element_type="column">
                                                                  <div class="elementor-column-wrap elementor-element-populated">
                                                                    <div class="elementor-widget-wrap">
                                                                      <div class="elementor-element elementor-element-1ba1140 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="1ba1140" data-element_type="widget" data-widget_type="heading.default">
                                                                        <div class="elementor-widget-container">
                                                                          <h3 class="elementor-heading-title elementor-size-default">18<br>19</h3>
                                                                        </div>
                                                                      </div>
                                                                    </div>
                                                                  </div>
                                                                </div>
                                                                <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-e041a66 wdp-sticky-section-no" data-id="e041a66" data-element_type="column">
                                                                  <div class="elementor-column-wrap elementor-element-populated">
                                                                    <div class="elementor-widget-wrap">
                                                                      <div class="elementor-element elementor-element-fe77200 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="fe77200" data-element_type="widget" data-widget_type="heading.default">
                                                                        <div class="elementor-widget-container">
                                                                          <h2 class="elementor-heading-title elementor-size-default">Sabtu - Minggu</h2>
                                                                        </div>
                                                                      </div>
                                                                      <div class="elementor-element elementor-element-9ec9fd0 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="9ec9fd0" data-element_type="widget" data-widget_type="heading.default">
                                                                        <div class="elementor-widget-container">
                                                                          <h2 class="elementor-heading-title elementor-size-default">Februari</h2>
                                                                        </div>
                                                                      </div>
                                                                      <div class="elementor-element elementor-element-b878d5b wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="b878d5b" data-element_type="widget" data-widget_type="heading.default">
                                                                        <div class="elementor-widget-container">
                                                                          <h2 class="elementor-heading-title elementor-size-default">2023</h2>
                                                                        </div>
                                                                      </div>
                                                                    </div>
                                                                  </div>
                                                                </div>
                                                              </div>
                                                            </div>
                                                          </section>
                                                          <div class="elementor-element elementor-element-047d9c4 elementor-align-left elementor-icon-list--layout-traditional elementor-list-item-link-full_width wdp-sticky-section-no elementor-widget elementor-widget-icon-list" data-id="047d9c4" data-element_type="widget" data-widget_type="icon-list.default">
                                                            <div class="elementor-widget-container">
                                                              <ul class="elementor-icon-list-items">
                                                                <li class="elementor-icon-list-item">
                                                                  <span class="elementor-icon-list-icon">
                                                                    <i aria-hidden="true" class="fas fa-clock"></i>
                                                                  </span>
                                                                  <span class="elementor-icon-list-text">10.00 - Selesai WIB</span>
                                                                </li>
                                                              </ul>
                                                            </div>
                                                          </div>
                                                          <div class="elementor-element elementor-element-d3d0c6d wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="d3d0c6d" data-element_type="widget" data-widget_type="heading.default">
                                                            <div class="elementor-widget-container">
                                                              <h2 class="elementor-heading-title elementor-size-default">Lokasi Acara</h2>
                                                            </div>
                                                          </div>
                                                          <div class="elementor-element elementor-element-5815811 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="5815811" data-element_type="widget" data-widget_type="heading.default">
                                                            <div class="elementor-widget-container">
                                                              <h2 class="elementor-heading-title elementor-size-default">
                                                                <b>Rumah mempelai puteri <br>
                                                                </b>Desa Kalisube , <br>Banyumas, <br>Jawa Tengah
                                                              </h2>
                                                            </div>
                                                          </div>
                                                          <div class="elementor-element elementor-element-ac48de4 elementor-widget__width-auto wdp-sticky-section-no elementor-widget elementor-widget-button" data-id="ac48de4" data-element_type="widget" data-widget_type="button.default">
                                                            <div class="elementor-widget-container">
                                                              <div class="elementor-button-wrapper">
                                                                <a href="https://goo.gl/maps/vb7k5WhEnekS2rVYA" class="elementor-button-link elementor-button elementor-size-sm" role="button">
                                                                  <span class="elementor-button-content-wrapper">
                                                                    <span class="elementor-button-text">GOOGLE MAPS</span>
                                                                  </span>
                                                                </a>
                                                              </div>
                                                            </div>
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </div>
                                                    <div data-dce-background-color="#5E5E5E" class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-49e38d9 wdp-sticky-section-no" data-id="49e38d9" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                                                      <div class="elementor-column-wrap elementor-element-populated">
                                                        <div class="elementor-widget-wrap">
                                                          <div class="elementor-element elementor-element-862a1a3 e-transform elementor-widget__width-auto wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="862a1a3" data-element_type="widget" data-settings="{&quot;_transform_rotateZ_effect&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:90,&quot;sizes&quot;:[]},&quot;_transform_rotateZ_effect_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;_transform_rotateZ_effect_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]}}" data-widget_type="heading.default">
                                                            <div class="elementor-widget-container">
                                                              <h2 class="elementor-heading-title elementor-size-default">Resepsi</h2>
                                                            </div>
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                              </section>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </section>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </section>
                      <section data-dce-background-color="#F8F8F8" class="elementor-section elementor-inner-section elementor-element elementor-element-7fa2887 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="7fa2887" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;_ha_eqh_enable&quot;:false}">
                        <div class="elementor-container elementor-column-gap-default">
                          <div class="elementor-row">
                            <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-d84e2f0 wdp-sticky-section-no" data-id="d84e2f0" data-element_type="column">
                              <div class="elementor-column-wrap elementor-element-populated">
                                <div class="elementor-widget-wrap">
                                  <div class="elementor-element elementor-element-cf26391 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="cf26391" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
                                    <div class="elementor-widget-container">
                                      <h2 class="elementor-heading-title elementor-size-default">Gallery</h2>
                                    </div>
                                  </div>
                                  <div class="elementor-element elementor-element-b4d998c animated-slow elementor-widget-divider--view-line wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-divider" data-id="b4d998c" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInLeft&quot;}" data-widget_type="divider.default">
                                    <div class="elementor-widget-container">
                                      <div class="elementor-divider">
                                        <span class="elementor-divider-separator"></span>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="elementor-element elementor-element-68857af animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="68857af" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                                    <div class="elementor-widget-container">
                                      <h2 class="elementor-heading-title elementor-size-default">Our Moment</h2>
                                    </div>
                                  </div>
                                  <div class="elementor-element elementor-element-208dc63 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-gallery" data-id="208dc63" data-element_type="widget" id="galeri" data-settings="{&quot;gallery_layout&quot;:&quot;justified&quot;,&quot;ideal_row_height&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:250,&quot;sizes&quot;:[]},&quot;ideal_row_height_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:220,&quot;sizes&quot;:[]},&quot;gap&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:4,&quot;sizes&quot;:[]},&quot;gap_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:4,&quot;sizes&quot;:[]},&quot;image_hover_animation&quot;:&quot;grow&quot;,&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;lazyload&quot;:&quot;yes&quot;,&quot;ideal_row_height_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:150,&quot;sizes&quot;:[]},&quot;gap_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:10,&quot;sizes&quot;:[]},&quot;link_to&quot;:&quot;file&quot;,&quot;overlay_background&quot;:&quot;yes&quot;,&quot;content_hover_animation&quot;:&quot;fade-in&quot;}" data-widget_type="gallery.default">
                                    <div class="elementor-widget-container">
                                      <div class="elementor-gallery__container">
                                        <a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="wp-content/uploads/2023/01/3.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-208dc63" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6MTI4MzEwLCJ1cmwiOiJodHRwczpcL1wvaW52aXRlYW1zLmNvbVwvd3AtY29udGVudFwvdXBsb2Fkc1wvMjAyM1wvMDFcL2ltZ19pcmZhbi12aXRhLTEuanBnIiwic2xpZGVzaG93IjoiYWxsLTIwOGRjNjMifQ%3D%3D">
                                          <div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="wp-content/uploads/2023/01/small/3.jpg" data-width="1024" data-height="1533" alt=""></div>
                                          <div class="elementor-gallery-item__overlay"></div>
                                        </a>
                                        <a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="wp-content/uploads/2023/01/10.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-208dc63" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6MTI4MzIwLCJ1cmwiOiJodHRwczpcL1wvaW52aXRlYW1zLmNvbVwvd3AtY29udGVudFwvdXBsb2Fkc1wvMjAyM1wvMDFcL2ltZ19pcmZhbi12aXRhLTExLmpwZyIsInNsaWRlc2hvdyI6ImFsbC0yMDhkYzYzIn0%3D">
                                          <div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="wp-content/uploads/2023/01/small/10.jpg" data-width="1150" data-height="768" alt=""></div>
                                          <div class="elementor-gallery-item__overlay"></div>
                                        </a>
                                        <a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="wp-content/uploads/2023/01/4.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-208dc63" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6MTI4MzI5LCJ1cmwiOiJodHRwczpcL1wvaW52aXRlYW1zLmNvbVwvd3AtY29udGVudFwvdXBsb2Fkc1wvMjAyM1wvMDFcL2ltZ19pcmZhbi12aXRhLTE3LmpwZyIsInNsaWRlc2hvdyI6ImFsbC0yMDhkYzYzIn0%3D">
                                          <div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="wp-content/uploads/2023/01/small/4.jpg" data-width="1024" data-height="1533" alt=""></div>
                                          <div class="elementor-gallery-item__overlay"></div>
                                        </a>
                                        <a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="wp-content/uploads/2023/01/5.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-208dc63" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6MTI4MzEzLCJ1cmwiOiJodHRwczpcL1wvaW52aXRlYW1zLmNvbVwvd3AtY29udGVudFwvdXBsb2Fkc1wvMjAyM1wvMDFcL2ltZ19pcmZhbi12aXRhLTQuanBnIiwic2xpZGVzaG93IjoiYWxsLTIwOGRjNjMifQ%3D%3D">
                                          <div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="wp-content/uploads/2023/01/small/5.jpg" data-width="1150" data-height="768" alt=""></div>
                                          <div class="elementor-gallery-item__overlay"></div>
                                        </a>
                                        <a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="wp-content/uploads/2023/01/15.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-208dc63" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6MTI4MzI0LCJ1cmwiOiJodHRwczpcL1wvaW52aXRlYW1zLmNvbVwvd3AtY29udGVudFwvdXBsb2Fkc1wvMjAyM1wvMDFcL2ltZ19pcmZhbi12aXRhLTE0LmpwZyIsInNsaWRlc2hvdyI6ImFsbC0yMDhkYzYzIn0%3D">
                                          <div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="wp-content/uploads/2023/01/small/15.jpg" data-width="1150" data-height="768" alt=""></div>
                                          <div class="elementor-gallery-item__overlay"></div>
                                        </a>
                                        <a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="wp-content/uploads/2023/01/7.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-208dc63" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6MTI4MzIyLCJ1cmwiOiJodHRwczpcL1wvaW52aXRlYW1zLmNvbVwvd3AtY29udGVudFwvdXBsb2Fkc1wvMjAyM1wvMDFcL2ltZ19pcmZhbi12aXRhLTEyLmpwZyIsInNsaWRlc2hvdyI6ImFsbC0yMDhkYzYzIn0%3D">
                                          <div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="wp-content/uploads/2023/01/small/7.jpg" data-width="1024" data-height="1534" alt=""></div>
                                          <div class="elementor-gallery-item__overlay"></div>
                                        </a>
                                        <a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="wp-content/uploads/2023/01/8.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-208dc63" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6MTI4MzMxLCJ1cmwiOiJodHRwczpcL1wvaW52aXRlYW1zLmNvbVwvd3AtY29udGVudFwvdXBsb2Fkc1wvMjAyM1wvMDFcL2ltZ19pcmZhbi12aXRhLTE5LmpwZyIsInNsaWRlc2hvdyI6ImFsbC0yMDhkYzYzIn0%3D">
                                          <div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="wp-content/uploads/2023/01/small/8.jpg" data-width="1150" data-height="768" alt=""></div>
                                          <div class="elementor-gallery-item__overlay"></div>
                                        </a>
                                        <a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="wp-content/uploads/2023/01/9.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-208dc63" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6MTI4MzE5LCJ1cmwiOiJodHRwczpcL1wvaW52aXRlYW1zLmNvbVwvd3AtY29udGVudFwvdXBsb2Fkc1wvMjAyM1wvMDFcL2ltZ19pcmZhbi12aXRhLTEwLmpwZyIsInNsaWRlc2hvdyI6ImFsbC0yMDhkYzYzIn0%3D">
                                          <div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="wp-content/uploads/2023/01/small/9.jpg" data-width="1150" data-height="768" alt=""></div>
                                          <div class="elementor-gallery-item__overlay"></div>
                                        </a>
                                        <a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="wp-content/uploads/2023/01/10.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-208dc63" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6MTI4MzE4LCJ1cmwiOiJodHRwczpcL1wvaW52aXRlYW1zLmNvbVwvd3AtY29udGVudFwvdXBsb2Fkc1wvMjAyM1wvMDFcL2ltZ19pcmZhbi12aXRhLTkuanBnIiwic2xpZGVzaG93IjoiYWxsLTIwOGRjNjMifQ%3D%3D">
                                          <div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="wp-content/uploads/2023/01/small/10.jpg" data-width="1150" data-height="768" alt=""></div>
                                          <div class="elementor-gallery-item__overlay"></div>
                                        </a>
                                        <a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="wp-content/uploads/2023/01/14.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-208dc63" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6MTI4MzIyLCJ1cmwiOiJodHRwczpcL1wvaW52aXRlYW1zLmNvbVwvd3AtY29udGVudFwvdXBsb2Fkc1wvMjAyM1wvMDFcL2ltZ19pcmZhbi12aXRhLTEyLmpwZyIsInNsaWRlc2hvdyI6ImFsbC0yMDhkYzYzIn0%3D">
                                          <div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="wp-content/uploads/2023/01/small/14.jpg" data-width="1024" data-height="1534" alt=""></div>
                                          <div class="elementor-gallery-item__overlay"></div>
                                        </a>
                                        <a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="wp-content/uploads/2023/01/13.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-208dc63" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6MTI4MzMxLCJ1cmwiOiJodHRwczpcL1wvaW52aXRlYW1zLmNvbVwvd3AtY29udGVudFwvdXBsb2Fkc1wvMjAyM1wvMDFcL2ltZ19pcmZhbi12aXRhLTE5LmpwZyIsInNsaWRlc2hvdyI6ImFsbC0yMDhkYzYzIn0%3D">
                                          <div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="wp-content/uploads/2023/01/small/13.jpg" data-width="1150" data-height="768" alt=""></div>
                                          <div class="elementor-gallery-item__overlay"></div>
                                        </a>
                                        <a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="wp-content/uploads/2023/01/11.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-208dc63" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6MTI4MzE4LCJ1cmwiOiJodHRwczpcL1wvaW52aXRlYW1zLmNvbVwvd3AtY29udGVudFwvdXBsb2Fkc1wvMjAyM1wvMDFcL2ltZ19pcmZhbi12aXRhLTkuanBnIiwic2xpZGVzaG93IjoiYWxsLTIwOGRjNjMifQ%3D%3D">
                                          <div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="wp-content/uploads/2023/01/small/11.jpg" data-width="1150" data-height="768" alt=""></div>
                                          <div class="elementor-gallery-item__overlay"></div>
                                        </a>
                                        <a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="wp-content/uploads/2023/01/12.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-208dc63" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6MTI4MzE4LCJ1cmwiOiJodHRwczpcL1wvaW52aXRlYW1zLmNvbVwvd3AtY29udGVudFwvdXBsb2Fkc1wvMjAyM1wvMDFcL2ltZ19pcmZhbi12aXRhLTkuanBnIiwic2xpZGVzaG93IjoiYWxsLTIwOGRjNjMifQ%3D%3D">
                                          <div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="wp-content/uploads/2023/01/small/12.jpg" data-width="1150" data-height="768" alt=""></div>
                                          <div class="elementor-gallery-item__overlay"></div>
                                        </a>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </section>
                      <section data-dce-background-color="#F8F8F8" class="elementor-section elementor-inner-section elementor-element elementor-element-69c9018 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="69c9018" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;gradient&quot;,&quot;_ha_eqh_enable&quot;:false}">
                        <div class="elementor-container elementor-column-gap-default">
                          <div class="elementor-row">
                            <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-de87ad8 wdp-sticky-section-no" data-id="de87ad8" data-element_type="column">
                              <div class="elementor-column-wrap elementor-element-populated">
                                <div class="elementor-widget-wrap">
                                  <div class="elementor-element elementor-element-aeb2e16 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="aeb2e16" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
                                    <div class="elementor-widget-container">
                                      <h2 class="elementor-heading-title elementor-size-default">Wedding Gift</h2>
                                    </div>
                                  </div>
                                  <div class="elementor-element elementor-element-141ff40 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="141ff40" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                                    <div class="elementor-widget-container">
                                      <h2 class="elementor-heading-title elementor-size-default">Bagi bapak/ibu/saudara/i yang ingin mengirimkan hadiah pernikahan dapat melalui virtual account atau e-wallet di bawah ini:</h2>
                                    </div>
                                  </div>
                                  <div class="elementor-element elementor-element-1db3065 animated-slow elementor-align-center wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-button" data-id="1db3065" data-element_type="widget" id="btn_gift" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="button.default">
                                    <div class="elementor-widget-container">
                                      <div class="elementor-button-wrapper">
                                        <a class="elementor-button elementor-size-sm" role="button">
                                          <span class="elementor-button-content-wrapper">
                                            <span class="elementor-button-icon elementor-align-icon-right">
                                              <i aria-hidden="true" class="hm hm-arrow-right1"></i>
                                            </span>
                                            <span class="elementor-button-text">KLIK DISINI</span>
                                          </span>
                                        </a>
                                      </div>
                                    </div>
                                  </div>
                                  <section class="elementor-section elementor-inner-section elementor-element elementor-element-4b39e42 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="4b39e42" data-element_type="section" id="sec_gift" data-settings="{&quot;_ha_eqh_enable&quot;:false}">
                                    <div class="elementor-container elementor-column-gap-default">
                                      <div class="elementor-row">
                                        <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-0fd6034 wdp-sticky-section-no" data-id="0fd6034" data-element_type="column">
                                          <div class="elementor-column-wrap elementor-element-populated">
                                            <div class="elementor-widget-wrap">
                                              <section data-dce-background-color="#FFFFFF" class="elementor-section elementor-inner-section elementor-element elementor-element-055ee47 animated-slow elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no elementor-invisible" data-id="055ee47" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;gradient&quot;,&quot;animation&quot;:&quot;fadeIn&quot;,&quot;_ha_eqh_enable&quot;:false}">
                                                <div class="elementor-container elementor-column-gap-default">
                                                  <div class="elementor-row">
                                                    <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-ff8e7fd wdp-sticky-section-no" data-id="ff8e7fd" data-element_type="column">
                                                      <div class="elementor-column-wrap elementor-element-populated">
                                                        <div class="elementor-widget-wrap">
                                                          <div class="elementor-element elementor-element-897e4f3 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="897e4f3" data-element_type="widget" data-widget_type="heading.default">
                                                            <div class="elementor-widget-container">
                                                              <h2 class="elementor-heading-title elementor-size-default">MANDIRI</h2>
                                                            </div>
                                                          </div>
                                                          <div class="elementor-element elementor-element-c6ce70c wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="c6ce70c" data-element_type="widget" data-widget_type="heading.default">
                                                            <div class="elementor-widget-container">
                                                              <h2 class="elementor-heading-title elementor-size-default">9000014960992</h2>
                                                            </div>
                                                          </div>
                                                          <div class="elementor-element elementor-element-ca01162 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="ca01162" data-element_type="widget" data-widget_type="heading.default">
                                                            <div class="elementor-widget-container">
                                                              <h2 class="elementor-heading-title elementor-size-default">An : Abdan Ahkamudin</h2>
                                                            </div>
                                                          </div>
                                                          <div class="elementor-element elementor-element-1ca71e5 elementor-align-center elementor-widget__width-auto wdp-sticky-section-no elementor-widget elementor-widget-dce-copy-to-clipboard" data-id="1ca71e5" data-element_type="widget" data-widget_type="dce-copy-to-clipboard.default">
                                                            <div class="elementor-widget-container">
                                                              <div class="dce-clipboard-wrapper dce-clipboard-wrapper-text">
                                                                <div>
                                                                  <button class="elementor-button elementor-size-sm" type="button" id="dce-clipboard-btn-1ca71e5-128245" data-clipboard-target="#dce-clipboard-value-1ca71e5-128245">
                                                                    <span class="elementor-button-content-wrapper dce-flexbox">
                                                                      <span class="elementor-button-icon elementor-align-icon-left">
                                                                        <i aria-hidden="true" class="far fa-copy"></i>
                                                                      </span>
                                                                      <span class="elementor-button-text">Salin No. Rek</span>
                                                                    </span>
                                                                  </button>
                                                                </div>
                                                                <input class="elementor-size-sm dce-clipboard-value elementor-field-textual dce-offscreen dce-form-control" id="dce-clipboard-value-1ca71e5-128245" aria-hidden="true" type="text" value="9000014960992">
                                                              </div>
                                                              <script defer src="data:text/javascript;base64,CgkJCQkJCQkJCWpRdWVyeShmdW5jdGlvbiAoKSB7CgkJCQl2YXIgY2xpcGJvYXJkXzFjYTcxZTVfMTI4MjQ1ID0gbmV3IENsaXBib2FyZEpTKCcjZGNlLWNsaXBib2FyZC1idG4tMWNhNzFlNS0xMjgyNDUnKTsKCQkJCWNsaXBib2FyZF8xY2E3MWU1XzEyODI0NS5vbignc3VjY2VzcycsIGZ1bmN0aW9uIChlKSB7CgkJCQkJalF1ZXJ5KCcjZGNlLWNsaXBib2FyZC1idG4tMWNhNzFlNS0xMjgyNDUnKS5odG1sKCdUZXJzYWxpbicpOwoJCQkJCXJldHVybiBmYWxzZTsKCQkJCX0pOwoJCQkJY2xpcGJvYXJkXzFjYTcxZTVfMTI4MjQ1Lm9uKCdlcnJvcicsIGZ1bmN0aW9uIChlKSB7CgkJCQkJY29uc29sZS5sb2coZSk7CgkJCQl9KTsKCQkJfSk7CgkJCQkJ"></script>
                                                            </div>
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                              </section>
                                              <section data-dce-background-color="#FFFFFF" class="elementor-section elementor-inner-section elementor-element elementor-element-7cc92ba animated-slow elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no elementor-invisible" data-id="7cc92ba" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;gradient&quot;,&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:400,&quot;_ha_eqh_enable&quot;:false}">
                                                <div class="elementor-container elementor-column-gap-default">
                                                  <div class="elementor-row">
                                                    <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-937a061 wdp-sticky-section-no" data-id="937a061" data-element_type="column">
                                                      <div class="elementor-column-wrap elementor-element-populated">
                                                        <div class="elementor-widget-wrap">
                                                          <div class="elementor-element elementor-element-c268313 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="c268313" data-element_type="widget" data-widget_type="heading.default">
                                                            <div class="elementor-widget-container">
                                                              <h2 class="elementor-heading-title elementor-size-default">BCA</h2>
                                                            </div>
                                                          </div>
                                                          <div class="elementor-element elementor-element-d216bd0 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="d216bd0" data-element_type="widget" data-widget_type="heading.default">
                                                            <div class="elementor-widget-container">
                                                              <h2 class="elementor-heading-title elementor-size-default">1110154531</h2>
                                                            </div>
                                                          </div>
                                                          <div class="elementor-element elementor-element-c46f73a wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="c46f73a" data-element_type="widget" data-widget_type="heading.default">
                                                            <div class="elementor-widget-container">
                                                              <h2 class="elementor-heading-title elementor-size-default">An : Uke Arumbini</h2>
                                                            </div>
                                                          </div>
                                                          <div class="elementor-element elementor-element-009e162 elementor-align-center elementor-widget__width-auto wdp-sticky-section-no elementor-widget elementor-widget-dce-copy-to-clipboard" data-id="009e162" data-element_type="widget" data-widget_type="dce-copy-to-clipboard.default">
                                                            <div class="elementor-widget-container">
                                                              <div class="dce-clipboard-wrapper dce-clipboard-wrapper-text">
                                                                <div>
                                                                  <button class="elementor-button elementor-size-sm" type="button" id="dce-clipboard-btn-009e162-128245" data-clipboard-target="#dce-clipboard-value-009e162-128245">
                                                                    <span class="elementor-button-content-wrapper dce-flexbox">
                                                                      <span class="elementor-button-icon elementor-align-icon-left">
                                                                        <i aria-hidden="true" class="far fa-copy"></i>
                                                                      </span>
                                                                      <span class="elementor-button-text">Salin No. Rek</span>
                                                                    </span>
                                                                  </button>
                                                                </div>
                                                                <input class="elementor-size-sm dce-clipboard-value elementor-field-textual dce-offscreen dce-form-control" id="dce-clipboard-value-009e162-128245" aria-hidden="true" type="text" value="1110154531">
                                                              </div>
                                                              <script defer src="data:text/javascript;base64,CgkJCQkJCQkJCWpRdWVyeShmdW5jdGlvbiAoKSB7CgkJCQl2YXIgY2xpcGJvYXJkXzAwOWUxNjJfMTI4MjQ1ID0gbmV3IENsaXBib2FyZEpTKCcjZGNlLWNsaXBib2FyZC1idG4tMDA5ZTE2Mi0xMjgyNDUnKTsKCQkJCWNsaXBib2FyZF8wMDllMTYyXzEyODI0NS5vbignc3VjY2VzcycsIGZ1bmN0aW9uIChlKSB7CgkJCQkJalF1ZXJ5KCcjZGNlLWNsaXBib2FyZC1idG4tMDA5ZTE2Mi0xMjgyNDUnKS5odG1sKCdUZXJzYWxpbicpOwoJCQkJCXJldHVybiBmYWxzZTsKCQkJCX0pOwoJCQkJY2xpcGJvYXJkXzAwOWUxNjJfMTI4MjQ1Lm9uKCdlcnJvcicsIGZ1bmN0aW9uIChlKSB7CgkJCQkJY29uc29sZS5sb2coZSk7CgkJCQl9KTsKCQkJfSk7CgkJCQkJ"></script>
                                                            </div>
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                              </section>
                                              <!-- <section data-dce-background-color="#FFFFFF" class="elementor-section elementor-inner-section elementor-element elementor-element-dd4de0e animated-slow elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no elementor-invisible" data-id="dd4de0e" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;gradient&quot;,&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:800,&quot;_ha_eqh_enable&quot;:false}">
                                                <div class="elementor-container elementor-column-gap-default">
                                                  <div class="elementor-row">
                                                    <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-5b01b78 wdp-sticky-section-no" data-id="5b01b78" data-element_type="column">
                                                      <div class="elementor-column-wrap elementor-element-populated">
                                                        <div class="elementor-widget-wrap">
                                                          <div class="elementor-element elementor-element-c086915 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="c086915" data-element_type="widget" data-widget_type="heading.default">
                                                            <div class="elementor-widget-container">
                                                              <h2 class="elementor-heading-title elementor-size-default">GIFT</h2>
                                                            </div>
                                                          </div>
                                                          <div class="elementor-element elementor-element-09b92b2 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="09b92b2" data-element_type="widget" data-widget_type="heading.default">
                                                            <div class="elementor-widget-container">
                                                              <h2 class="elementor-heading-title elementor-size-default">Penerima: <br>Vitaloka </h2>
                                                            </div>
                                                          </div>
                                                          <div class="elementor-element elementor-element-bca796e wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="bca796e" data-element_type="widget" data-widget_type="heading.default">
                                                            <div class="elementor-widget-container">
                                                              <h2 class="elementor-heading-title elementor-size-default">Jl . Ky Warmidzi No. 31 Sapuro Indah Kec. Pekalongan Barat - Jawa Tengah Kode Pos : 51112</h2>
                                                            </div>
                                                          </div>
                                                          <div class="elementor-element elementor-element-8813067 elementor-align-center elementor-widget__width-auto wdp-sticky-section-no elementor-widget elementor-widget-dce-copy-to-clipboard" data-id="8813067" data-element_type="widget" data-widget_type="dce-copy-to-clipboard.default">
                                                            <div class="elementor-widget-container">
                                                              <div class="dce-clipboard-wrapper dce-clipboard-wrapper-textarea">
                                                                <button class="elementor-button elementor-size-sm" type="button" id="dce-clipboard-btn-8813067-128245" data-clipboard-target="#dce-clipboard-value-8813067-128245">
                                                                  <span class="elementor-button-content-wrapper dce-flexbox">
                                                                    <span class="elementor-button-icon elementor-align-icon-left">
                                                                      <i aria-hidden="true" class="far fa-copy"></i>
                                                                    </span>
                                                                    <span class="elementor-button-text">Salin Alamat</span>
                                                                  </span>
                                                                </button>
                                                                <textarea class="elementor-size-sm dce-clipboard-value elementor-field-textual dce-offscreen dce-block" id="dce-clipboard-value-8813067-128245" aria-hidden="true">Jl . Ky Warmidzi No. 31 Sapuro Indah Kec. Pekalongan Barat - Jawa Tengah
Kode Pos : 51112</textarea>
                                                              </div>
                                                              <script defer src="data:text/javascript;base64,CgkJCQkJCQkJCWpRdWVyeShmdW5jdGlvbiAoKSB7CgkJCQl2YXIgY2xpcGJvYXJkXzg4MTMwNjdfMTI4MjQ1ID0gbmV3IENsaXBib2FyZEpTKCcjZGNlLWNsaXBib2FyZC1idG4tODgxMzA2Ny0xMjgyNDUnKTsKCQkJCWNsaXBib2FyZF84ODEzMDY3XzEyODI0NS5vbignc3VjY2VzcycsIGZ1bmN0aW9uIChlKSB7CgkJCQkJalF1ZXJ5KCcjZGNlLWNsaXBib2FyZC1idG4tODgxMzA2Ny0xMjgyNDUnKS5odG1sKCdUZXJzYWxpbicpOwoJCQkJCXJldHVybiBmYWxzZTsKCQkJCX0pOwoJCQkJY2xpcGJvYXJkXzg4MTMwNjdfMTI4MjQ1Lm9uKCdlcnJvcicsIGZ1bmN0aW9uIChlKSB7CgkJCQkJY29uc29sZS5sb2coZSk7CgkJCQl9KTsKCQkJfSk7CgkJCQkJ"></script>
                                                            </div>
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                              </section> -->
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </section>
                                  <div class="elementor-element elementor-element-795e8b5 wdp-sticky-section-no elementor-widget elementor-widget-html" data-id="795e8b5" data-element_type="widget" data-widget_type="html.default">
                                    <div class="elementor-widget-container">
                                      <script defer src="data:text/javascript;base64,DQp2YXIgYnRuX2dpZnQgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnYnRuX2dpZnQnKTsNCnZhciBzZWNfZ2lmdCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdzZWNfZ2lmdCcpOw0KDQpidG5fZ2lmdC5vbmNsaWNrID0gZnVuY3Rpb24oKXsgICANCiAgICBpZiAoc2VjX2dpZnQuc3R5bGUuZGlzcGxheSA9PT0gImJsb2NrIikgew0KICAgICAgICAkKCIjc2VjX2dpZnQiKS5hbmltYXRlKHsgb3BhY2l0eTogMH0sIDEwMDAsICdlYXNlSW5PdXRDdWJpYycpOw0KCSAgICAkKCIjc2VjX2dpZnQiKS5oaWRlKDEwMDApOw0KICAgIH0gDQogICAgZWxzZXsNCiAgICAgICAgJCgiI3NlY19naWZ0Iikuc2hvdyg1MDApOw0KICAgICAgICAkKCIjc2VjX2dpZnQiKS5hbmltYXRlKHsgb3BhY2l0eTogMX0sIDEwMDAsICdlYXNlSW5PdXRDdWJpYycpOw0KICAgIH0NCn07DQo="></script>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </section>
                      <section data-dce-background-overlay-color="#5E5E5E" data-dce-background-image-url="wp-content/uploads/2023/01/left-2.jpg" class="elementor-section elementor-inner-section elementor-element elementor-element-7bd8417 elementor-section-height-min-height elementor-section-boxed elementor-section-height-default wdp-sticky-section-no" data-id="7bd8417" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;_ha_eqh_enable&quot;:false}">
                        <div class="elementor-background-overlay"></div>
                        <div class="elementor-container elementor-column-gap-default">
                          <div class="elementor-row">
                            <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-aecd59c wdp-sticky-section-no" data-id="aecd59c" data-element_type="column">
                              <div class="elementor-column-wrap elementor-element-populated">
                                <div class="elementor-widget-wrap">
                                  <div class="elementor-element elementor-element-6d805ac animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="6d805ac" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
                                    <div class="elementor-widget-container">
                                      <h2 class="elementor-heading-title elementor-size-default">Ucapan</h2>
                                    </div>
                                  </div>
                                  <div class="elementor-element elementor-element-48307fa animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="48307fa" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                                    <div class="elementor-widget-container">
                                      <h2 class="elementor-heading-title elementor-size-default">Wishes</h2>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </section>
                      <section data-dce-background-color="#FFFFFF" class="elementor-section elementor-inner-section elementor-element elementor-element-ea4c586 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="ea4c586" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;_ha_eqh_enable&quot;:false}">
                        <div class="elementor-container elementor-column-gap-default">
                          <div class="elementor-row">
                            <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-eff1217 wdp-sticky-section-no" data-id="eff1217" data-element_type="column">
                              <div class="elementor-column-wrap elementor-element-populated">
                                <div class="elementor-widget-wrap">
                                  <div class="elementor-element elementor-element-2c2adda elementor-widget__width-auto elementor-absolute dce_masking-none wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="2c2adda" data-element_type="widget" data-settings="{&quot;_position&quot;:&quot;absolute&quot;}" data-widget_type="image.default">
                                    <div class="elementor-widget-container">
                                      <div class="elementor-image">
                                        <img decoding="async" width="700" height="540" src="wp-content/uploads/2022/07/img-tree-shadow-01.png" class="attachment-full size-full" alt="" loading="lazy" srcset="wp-content/uploads/2022/07/img-tree-shadow-01.png 700w, wp-content/uploads/2022/07/img-tree-shadow-01-300x231.png 300w" sizes="(max-width: 700px) 100vw, 700px" />
                                      </div>
                                    </div>
                                  </div>
                                  <div class="elementor-element elementor-element-ee6f05c animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-weddingpress-commentkit" data-id="ee6f05c" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-widget_type="weddingpress-commentkit.default">
                                    <div class="elementor-widget-container">
                                      <div class='wdp-wrapper wdp-custom wdp-border' style='overflow: hidden;'>
                                        <div class='wdp-wrap-link'>
                                          <a id='wdp-link-128245' class='wdp-link wdp-icon-link wdp-icon-link-true auto-load-true' href='?post_id=128245&amp;comments=15&amp;get=0&amp;order=DESC' title='15 Ucapan'>
                                            <i aria-hidden='true' class='fas fa-comments'></i>
                                            <span>15</span> Ucapan </a>
                                        </div>
                                        <div id='wdp-wrap-commnent-128245' class='wdp-wrap-comments' style='display:none;'>
                                          <div id='wdp-wrap-form-128245' class='wdp-wrap-form wdp-clearfix'>
                                            <div id='wdp-container-form-128245' class='wdp-container-form wdp-no-login'>
                                              <div id='respond-128245' class='respond wdp-clearfix'>
                                                <form action='save_json.php' method='post' id='commentform-128245'>
                                                  <p class="comment-form-author wdp-field-1">
                                                    <input id="author" name="author" type="text" aria-required="true" class="wdp-input" placeholder="Nama Anda" value="<?= ucfirst(strtolower($requestName)) ?>" required nofocus />
                                                    <span class="wdp-required">*</span>
                                                    <span class="wdp-error-info wdp-error-info-name">Nama dilarang mengandung symbol!</span>
                                                  </p>
                                                  <div class="wdp-wrap-text">
                                                    <p class="comment-form-hubungan wdp-hubungan">
                                                      <input id="hubungan" name="hubungan" type="text" aria-required="true" class="wdp-input" placeholder="Hubungan (Saudara, Teman Kerja, Teman Kuliah, dll)" required maxlength="20" />
                                                    </p>
                                                  </div>
                                                  <div class="wdp-wrap-textarea">
                                                    <textarea class="waci_comment wdp-textarea autosize-textarea" name="comment" id="comment" aria-required="true" placeholder="Berikan Ucapan & Doa untuk Kedua Mempelai" rows="3"></textarea>
                                                    <span class="wdp-required">*</span>
                                                    <span class="wdp-error-info wdp-error-info-text">Minimal 2 karakter.</span>
                                                  </div>
                                                  <div class="wdp-wrap-select">
                                                    <select class="waci_comment wdp-select" name="konfirmasi" id="konfirmasi">
                                                      <option value="Tidak Konfirmasi">Pilih Konfirmasi Kehadiran</option>> </option>
                                                      <option value="Akan Hadir">Akan Hadir</option>
                                                      <option value="Tidak Hadir">Tidak Hadir</option>
                                                      <option value="Masih Ragu">Masih Ragu</option>
                                                    </select>
                                                    <span class="wdp-required">*</span>
                                                    <span class="wdp-error-info wdp-error-info-confirm">Silahkan pilih konfirmasi kehadiran</span>
                                                  </div>
                                                  <div class='wdp-wrap-submit wdp-clearfix'>
                                                    <p class='form-submit'>
                                                      <span class="wdp-hide">Do not change these fields following</span>
                                                      <input type="text" class="wdp-hide" name="name" value="username">
                                                      <input type="text" class="wdp-hide" name="nombre" value="">
                                                      <input type="text" class="wdp-hide" name="form-wdp" value="">
                                                      <input type="button" class="wdp-form-btn wdp-cancel-btn" value="Batal">
                                                      <!-- <input name='submit' id='submit-128245' value='➤ Kirim'' type=' submit' /> -->
                                                      <input name='submit' type='submit' id="tombolSubmit" class="elementor-button-text"></input>
                                                      <input type='hidden' name='commentpress' value='true' />
                                                      <input type='hidden' name='comment_post_ID' value='128245' id='comment_post_ID' />
                                                      <input type='hidden' name='comment_parent' id='comment_parent' value='0' />
                                                    </p>
                                                  </div>
                                                </form>
                                              </div>
                                            </div>
                                          </div>
                                          <div id='wdp-comment-status-128245' class='wdp-comment-status'></div>
                                          <ul class='wdp-container-comments wdp-order-DESC  wdp-has-15-comments wdp-multiple-comments' data-order='DESC' id="wedding_comment" style="display: block !important;">
                                           <?= 
                                            $commentnya;
                                           ?>
                                          </ul>
                                          <div class='wdp-holder-128245 wdp-holder'></div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="elementor-element elementor-element-fc13e0d wdp-sticky-section-no elementor-widget elementor-widget-html" data-id="fc13e0d" data-element_type="widget" data-widget_type="html.default">
                                    <div class="elementor-widget-container">
                                      <!-- <script defer src="data:text/javascript;base64,DQoNCiAgICAvKk5hbWEgVGFtdSovDQogICAgdmFyIHRleHRfYXV0aCA9ICJOYW1hIEthbXUiOw0KICAgIA0KICAgIC8qVGV4dCBFcnJvciovDQogICAgdmFyIHRleHRfZXJyID0gIlRlcmphZGkga2VzYWxhaGFuISI7DQogICAgDQogICAgLypUZXh0IEh1YnVuZ2FuKi8NCiAgICB2YXIgdGV4dF9odWJ1bmdhbiA9ICJIdWJ1bmdhbiAoU2F1ZGFyYSwgVGVtYW4gS2VyamEsIFRlbWFuIEt1bGlhaCwgZGxsKSINCiAgICANCiAgICAvKlVjYXBhbiovDQogICAgdmFyIHRleHRfdWNwbiA9ICJCZXJpa2FuIFVjYXBhbiAmIERvYSB0ZXJiYWlrIHVudHVrIEtlZHVhIE1lbXBlbGFpIg0KICAgIA0KICAgIC8qVGV4dCBCdXR0b24qLw0KICAgIHZhciB0ZXh0X2J0biA9ICJTZW5kIOKGkiI7DQogICAgDQogICAgLyprb25maXJtYXNpIEtlaGFkaXJhbiovDQogICAgdmFyIHRleHRfb3B0MSA9ICJQaWxpaCBLb25maXJtYXNpIEtlaGFkaXJhbiI7DQogICAgdmFyIHRleHRfb3B0MiA9ICJIYWRpciI7DQogICAgdmFyIHRleHRfb3B0MyA9ICJUaWRhayBIYWRpciI7DQogICAgdmFyIHRleHRfb3B0NCA9ICJNYXNpaCBSYWd1IjsNCiAgICANCiAgICAvKlRleHQgSnVtbGFoIFRhbXUqLw0KICAgIHZhciB0eHRfam1saF90YW11ID0gIjxiPkp1bWxhaCBUYW11IEhhZGlyOjwvYj4iOw0KICAgIA0KICAgIC8qVGV4dCBUYW11Ki8NCiAgICB2YXIgdHh0X3RhbXUgPSAiVGFtdSI7DQogICAgDQogICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoImF1dGhvciIpLnBsYWNlaG9sZGVyID0gdGV4dF9hdXRoOw0KICAgIGRvY3VtZW50LmdldEVsZW1lbnRzQnlDbGFzc05hbWUoJ3dkcC1lcnJvci1pbmZvLW5hbWUnKVswXS5pbm5lckhUTUwgPSB0ZXh0X2VycjsNCiAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgiaHVidW5nYW4iKS5wbGFjZWhvbGRlciA9IHRleHRfaHVidW5nYW47DQogICAgZG9jdW1lbnQuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZSgnd2FjaV9jb21tZW50JylbMF0ucGxhY2Vob2xkZXIgPSB0ZXh0X3VjcG47DQogICAgZG9jdW1lbnQuZ2V0RWxlbWVudHNCeU5hbWUoJ3N1Ym1pdCcpWzBdLnZhbHVlID0gdGV4dF9idG47DQogICAgZG9jdW1lbnQuZ2V0RWxlbWVudHNCeU5hbWUoJ2tvbmZpcm1hc2knKVswXS5vcHRpb25zWzBdLmlubmVySFRNTCA9IHRleHRfb3B0MTsNCiAgICBkb2N1bWVudC5nZXRFbGVtZW50c0J5TmFtZSgna29uZmlybWFzaScpWzBdLm9wdGlvbnNbMV0uaW5uZXJIVE1MID0gdGV4dF9vcHQyOw0KICAgIGRvY3VtZW50LmdldEVsZW1lbnRzQnlOYW1lKCdrb25maXJtYXNpJylbMF0ub3B0aW9uc1syXS5pbm5lckhUTUwgPSB0ZXh0X29wdDM7DQogICAgZG9jdW1lbnQuZ2V0RWxlbWVudHNCeU5hbWUoJ2tvbmZpcm1hc2knKVswXS5vcHRpb25zWzNdLmlubmVySFRNTCA9IHRleHRfb3B0NDsNCiAgICBkb2N1bWVudC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKCd0eHRfam1saF90YW11JylbMF0uaW5uZXJIVE1MID0gdHh0X2ptbGhfdGFtdTsNCiAgICBkb2N1bWVudC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKCd0eHRfdGFtdScpWzBdLmlubmVySFRNTCA9IHR4dF90YW11Ow0KICAgIGRvY3VtZW50LmdldEVsZW1lbnRzQnlDbGFzc05hbWUoJ3R4dF90YW11JylbMV0uaW5uZXJIVE1MID0gdHh0X3RhbXU7DQogICAgZG9jdW1lbnQuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZSgndHh0X3RhbXUnKVsyXS5pbm5lckhUTUwgPSB0eHRfdGFtdTsNCiAgICANCg=="></script> -->
                                    </div>
                                  </div>
                                  <div class="elementor-element elementor-element-9e4f3a3 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="9e4f3a3" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
                                    <div class="elementor-widget-container">
                                      <h2 class="elementor-heading-title elementor-size-default">Protocol Covid-19</h2>
                                    </div>
                                  </div>
                                  <div class="elementor-element elementor-element-1cf38ae animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="1cf38ae" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                                    <div class="elementor-widget-container">
                                      <h2 class="elementor-heading-title elementor-size-default">Kami menghimbau para tamu undangan agar tetap memperhatikan protokol kesehatan.</h2>
                                    </div>
                                  </div>
                                  <section class="elementor-section elementor-inner-section elementor-element elementor-element-ca73dc9 animated-slow elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no elementor-invisible" data-id="ca73dc9" data-element_type="section" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;_ha_eqh_enable&quot;:false}">
                                    <div class="elementor-container elementor-column-gap-default">
                                      <div class="elementor-row">
                                        <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-1e3fbf7 wdp-sticky-section-no" data-id="1e3fbf7" data-element_type="column">
                                          <div class="elementor-column-wrap elementor-element-populated">
                                            <div class="elementor-widget-wrap">
                                              <div class="elementor-element elementor-element-688989c elementor-view-stacked elementor-shape-square wdp-sticky-section-no elementor-widget elementor-widget-icon" data-id="688989c" data-element_type="widget" data-widget_type="icon.default">
                                                <div class="elementor-widget-container">
                                                  <div class="elementor-icon-wrapper">
                                                    <div class="elementor-icon">
                                                      <svg xmlns="http://www.w3.org/2000/svg" id="fc335d76-f6f6-4d53-998a-33504ccd4493" data-name="Layer 1" viewBox="0 0 150 150">
                                                        <defs>
                                                          <style>
                                                            .b4014e33-99de-4bff-a040-79cf4526fd82 {
                                                              fill: #333;
                                                              fill-rule: evenodd;
                                                            }
                                                          </style>
                                                        </defs>
                                                        <title>protocol-2</title>
                                                        <path class="b4014e33-99de-4bff-a040-79cf4526fd82" d="M43.76,13.15c2.63,0,16.45,21.18,16.45,30.28a16.45,16.45,0,0,1-32.9,0c0-9.1,14-30.28,16.45-30.28Zm6.3-4.72a2.68,2.68,0,0,1-4-.53,2.79,2.79,0,0,1,.35-4c.17-.18,10.67-8.75,31.5.17l.17.18h.18s5.25,3.5,19.07-.35A3,3,0,0,1,101,6a2.92,2.92,0,0,1-2.1,3.67c-2.63.7-5.08,1.23-7.18,1.58V32.75h6.65a4,4,0,0,1,4,4v9.1H70.19v-9.1a4,4,0,0,1,4-4h6.13V11.05a12.67,12.67,0,0,1-4.9-1.57c-17.5-7.35-25.2-1.05-25.38-1.05Zm54.25,41a35.84,35.84,0,0,1,18.38,31.15v.17H49.89v-.17A35.85,35.85,0,0,1,68.26,49.38Zm18.38,34.3v43.57H49.89V83.68Zm0,46.37V142.3a7.62,7.62,0,0,1-7.53,7.7H57.41a7.73,7.73,0,0,1-7.52-7.7V130.05Zm-81-79.45V44.3h-6.3V39.93h6.3V33.8h4.2v6.13h6.3V44.3h-6.3v6.3Z"></path>
                                                      </svg>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                              <div class="elementor-element elementor-element-bf769a1 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="bf769a1" data-element_type="widget" data-widget_type="heading.default">
                                                <div class="elementor-widget-container">
                                                  <h2 class="elementor-heading-title elementor-size-default">Memakai <br> Hand Sanitizer </h2>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-02f10ac wdp-sticky-section-no" data-id="02f10ac" data-element_type="column">
                                          <div class="elementor-column-wrap elementor-element-populated">
                                            <div class="elementor-widget-wrap">
                                              <div class="elementor-element elementor-element-cd9aff3 elementor-view-stacked elementor-shape-square wdp-sticky-section-no elementor-widget elementor-widget-icon" data-id="cd9aff3" data-element_type="widget" data-widget_type="icon.default">
                                                <div class="elementor-widget-container">
                                                  <div class="elementor-icon-wrapper">
                                                    <div class="elementor-icon">
                                                      <svg xmlns="http://www.w3.org/2000/svg" id="b0fe75c6-692e-441c-ae95-117c49c47f25" data-name="Layer 1" viewBox="0 0 150 150">
                                                        <defs>
                                                          <style>
                                                            .ff452229-05c2-4ff9-b426-bfc8edf0f91c {
                                                              fill: #333;
                                                            }
                                                          </style>
                                                        </defs>
                                                        <title>protocol-5</title>
                                                        <path class="ff452229-05c2-4ff9-b426-bfc8edf0f91c" d="M15.94,60.3a1.18,1.18,0,0,1-.28-1.7,1.13,1.13,0,0,1,1.55-.28c.15,0,4.81,3.82,32.52-15.55h0c0-.14,12-9.05,24.32-9.48h1.27c12.3.43,24.31,9.34,24.31,9.48h0c27.71,19.37,32.52,15.55,32.52,15.55a1.32,1.32,0,0,1,1.7.28,1.35,1.35,0,0,1-.43,1.7s-6.08,4.8-35.2-15.7h0S86.49,36,74.75,35.7C63,36,51.14,44.6,51.14,44.6h0C22,65.1,15.94,60.3,15.94,60.3Zm96.56,43.12c2.69,3.67,6.36,5.09,10.32,4s8.62-4.52,14-9.89a49.55,49.55,0,0,0,11.59-17.81c2.12-6.23,2.26-11.88-.43-15.27-4.52-6.08-13.29-2-15.26-1C124,65,102.18,51.25,93.13,44.89a32.46,32.46,0,0,0-17.39-5.52H74.19a31.63,31.63,0,0,0-17.25,5.52C50.15,49.55,36.3,58.46,26.12,62l-5.94,1.42a10.5,10.5,0,0,1-3,0c-2-1-10.74-5.09-15.26,1-2.55,3.39-2.55,9-.29,15.27C3.64,85.6,7.74,92,13.11,97.48c5.52,5.37,10,8.76,14,9.89s7.63-.28,10.32-4a58.88,58.88,0,0,0,35.91,13.29h3.25c13-.43,25.87-4.81,35.91-13.29ZM73.76,91.26h2.41c9.19.42,14.7,5.23,18.66,8.9h0c-6.93-4.8-13.15-6.64-18.66-6.78h0l-.43-.14H74.19l-.43.14h0c-5.37.14-11.73,2-18.52,6.78h0c3.82-3.67,9.33-8.48,18.52-8.9ZM74.47,60h1c15.13.29,24.17,8.2,30.39,14h0c-11-7.78-21.2-10.46-30-10.74H74.05c-8.77.28-18.81,3-29.83,10.74h0c6.08-5.8,15.12-13.71,30.25-14ZM74.19,77.4h1.55c12.3.29,19.65,6.79,24.74,11.45h0c-9-6.36-17.39-8.62-24.6-8.76V80H74.19v.14c-7.35.14-15.7,2.4-24.74,8.76h0c5.09-4.66,12.44-11.16,24.74-11.45ZM19.33,75.14l.29.71.14.42v.14l.14.15V77h0l.14.42.14.14v.29l.14.28h0l.15.42.14.15V79l.14.28v.14h0l.56,1.13.14.43h0l.15.42.14.28v.15l.14.42H22l.14.42.43.85.14.43v.14l.14.28.14.28.14.14v.15l.43.7.14.29.14.14.14.42h0l.28.57v.14l.14.28.29.28v.14l.28.43v.14l.28.28.14.29v.14l.29.42v.14L26,90l.14.28.14.14.28.42h0l.29.43.14.28.14.14.28.43v.14l.29.42h0L28,93v.15l.28.28v.14l.42.42.15.29.28.42h0l.14.14.28.43h.14l.29.42.14.28h0l.42.43.14.14h0l.15.14.28.42h0l.28.29.28.42h0a8.59,8.59,0,0,0,1.13,1.13V99l.43.43.14.14h0l.42.42.15.14.14.15.42.42h0l.14.14.29.28.14.15.14.14.42.42c-2,2.83-4.8,4.1-7.77,3.25-3.54-1-7.78-4.24-13-9.33a46.15,46.15,0,0,1-10.89-17C2,73.44,1.8,68.64,3.78,66c3.68-5,11.6-.85,12.59-.28l1,.85h0v.14h0v.14h0v.14h0l.14.14h0v.14h0v.15h0v.14h0v.14h0v.14h.14v.14h0v.14h0v.14h0v.15h0v.14h.14v.14h0v.14h0v.14h0v.14h0v.14h.14v.15h0v.14Zm113.24-8.62,1-.85c1-.57,8.91-4.67,12.59.28,2,2.69,1.83,7.49,0,12.87a46.62,46.62,0,0,1-11,17c-5.09,5.09-9.47,8.34-12.87,9.33-3.11.85-5.93-.42-7.91-3.25,13.29-12.16,17.1-28.56,18.23-35.34Z"></path>
                                                      </svg>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                              <div class="elementor-element elementor-element-ec6e767 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="ec6e767" data-element_type="widget" data-widget_type="heading.default">
                                                <div class="elementor-widget-container">
                                                  <h2 class="elementor-heading-title elementor-size-default">Memakai <br> Masker </h2>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-96a2fe0 wdp-sticky-section-no" data-id="96a2fe0" data-element_type="column">
                                          <div class="elementor-column-wrap elementor-element-populated">
                                            <div class="elementor-widget-wrap">
                                              <div class="elementor-element elementor-element-00cd332 elementor-view-stacked elementor-shape-square wdp-sticky-section-no elementor-widget elementor-widget-icon" data-id="00cd332" data-element_type="widget" data-widget_type="icon.default">
                                                <div class="elementor-widget-container">
                                                  <div class="elementor-icon-wrapper">
                                                    <div class="elementor-icon">
                                                      <svg xmlns="http://www.w3.org/2000/svg" id="bc1ba8c6-6118-4909-9342-e897e4cbe163" data-name="Layer 1" viewBox="0 0 150 150">
                                                        <defs>
                                                          <style>
                                                            .a29fadad-87cd-45dd-b870-cd004368b1e6 {
                                                              fill: #333;
                                                              fill-rule: evenodd;
                                                            }
                                                          </style>
                                                        </defs>
                                                        <title>protocol-3</title>
                                                        <path class="a29fadad-87cd-45dd-b870-cd004368b1e6" d="M72.27,133.17H61.85a7,7,0,0,1,1.19-2.9,14.17,14.17,0,0,1,3.25-3.08c1-.85,1.71-1.36,2-1.71a4.32,4.32,0,0,0,.34-1.19,1.33,1.33,0,0,0-.34-.86,1.49,1.49,0,0,0-1-.51,1.54,1.54,0,0,0-1.19.51,3.76,3.76,0,0,0-.52,1.37l-3.41-.17a6.66,6.66,0,0,1,.68-2.39,3.69,3.69,0,0,1,1.54-1.2,6,6,0,0,1,2.73-.51,6.87,6.87,0,0,1,2.91.51,2.73,2.73,0,0,1,1.53,1.2,4.05,4.05,0,0,1,.52,2.05,3.33,3.33,0,0,1-.69,2A6,6,0,0,1,69,128.56c-.68.51-1.2.85-1.37,1-.17.35-.51.52-.85.86h5.47ZM135.82,56C144.36,62.44,150,73.55,150,86.19v.68c-21.18,10.77-42.37,10.25-63.55,0v-.68c0-12.3,5.46-23.06,13.66-29.56A25.81,25.81,0,0,0,135.82,56ZM31.09,16.83A21.1,21.1,0,1,1,10.08,38a21.07,21.07,0,0,1,21-21.18ZM49.2,56c8.72,6.49,14.35,17.6,14.35,30.24v.68C42.37,97.64,21.18,97.12,0,86.87v-.68c0-12.3,5.47-23.06,13.67-29.56a25.55,25.55,0,0,0,17.42,6.84A24.74,24.74,0,0,0,49.2,56Zm68.34-39.12A21.1,21.1,0,1,1,96.53,38a21.06,21.06,0,0,1,21-21.18Zm-94,94.47,15.2-7.51v4.61h72.61v-4.61l15,7.51-15,7.52v-4.61H38.78v4.61Zm50.74,9.57h5l1.88,7.52,2-7.52h5v12.3H85.08v-9.39l-2.39,9.39H79.78l-2.39-9.39v9.39H74.32Z"></path>
                                                      </svg>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                              <div class="elementor-element elementor-element-f475063 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="f475063" data-element_type="widget" data-widget_type="heading.default">
                                                <div class="elementor-widget-container">
                                                  <h2 class="elementor-heading-title elementor-size-default">Menjaga <br> Jarak </h2>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </section>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </section>
                      <section data-dce-background-overlay-color="#FFFFFF" class="elementor-section elementor-inner-section elementor-element elementor-element-8ab1d9a elementor-section-height-min-height elementor-section-boxed elementor-section-height-default wdp-sticky-section-no" data-id="8ab1d9a" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;slideshow&quot;,&quot;background_slideshow_gallery&quot;:[{&quot;id&quot;:128313,&quot;url&quot;:&quot;wp-content\/uploads\/2023\/01\/10.jpg&quot;},{&quot;id&quot;:128314,&quot;url&quot;:&quot;wp-content\/uploads\/2023\/01\/7.jpg&quot;},{&quot;id&quot;:128315,&quot;url&quot;:&quot;wp-content\/uploads\/2023\/01\/3.jpg&quot;}],&quot;background_slideshow_slide_duration&quot;:1000,&quot;background_slideshow_transition_duration&quot;:5000,&quot;background_slideshow_loop&quot;:&quot;yes&quot;,&quot;background_slideshow_slide_transition&quot;:&quot;fade&quot;,&quot;_ha_eqh_enable&quot;:false}">
                        <div class="elementor-background-overlay"></div>
                        <div class="elementor-container elementor-column-gap-default">
                          <div class="elementor-row">
                            <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-6a60023 wdp-sticky-section-no" data-id="6a60023" data-element_type="column">
                              <div class="elementor-column-wrap elementor-element-populated">
                                <div class="elementor-widget-wrap">
                                  <div class="elementor-element elementor-element-b73e346 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="b73e346" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
                                    <div class="elementor-widget-container">
                                      <h2 class="elementor-heading-title elementor-size-default">Terimakasih</h2>
                                    </div>
                                  </div>
                                  <div class="elementor-element elementor-element-7b50b4d animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="7b50b4d" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                                    <div class="elementor-widget-container">
                                      <h2 class="elementor-heading-title elementor-size-default">Merupakan suatu kebahagiaan dan kehormatan bagi kami, apabila Bapak/Ibu/Saudara/i, berkenan hadir dan memberikan do’a restu kepada kami.</h2>
                                    </div>
                                  </div>
                                  <div class="elementor-element elementor-element-066c6bf animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="066c6bf" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                                    <div class="elementor-widget-container">
                                      <h2 class="elementor-heading-title elementor-size-default">KAMI YANG BERBAHAGIA</h2>
                                    </div>
                                  </div>
                                  <div class="elementor-element elementor-element-6fb722a animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="6fb722a" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                                    <div class="elementor-widget-container">
                                      <h2 class="elementor-heading-title elementor-size-default">Abdan & Uke</h2>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </section>
                      <section class="elementor-section elementor-inner-section elementor-element elementor-element-89ac2e9 elementor-section-content-middle wdp-sticky-section-yes elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-positon-bottom" data-id="89ac2e9" data-element_type="section" data-settings="{&quot;sticky&quot;:&quot;bottom&quot;,&quot;_ha_eqh_enable&quot;:false,&quot;sticky_on&quot;:[&quot;desktop&quot;,&quot;tablet&quot;,&quot;mobile&quot;],&quot;sticky_offset&quot;:0,&quot;sticky_effects_offset&quot;:0}">
                        <div class="elementor-container elementor-column-gap-no">
                          <div class="elementor-row">
                            <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-59a0679 wdp-sticky-section-no" data-id="59a0679" data-element_type="column">
                              <div class="elementor-column-wrap elementor-element-populated">
                                <div class="elementor-widget-wrap">
                                  <div class="elementor-element elementor-element-e32d3eb elementor-view-stacked animated-slow elementor-widget__width-auto btn_musik elementor-shape-circle wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-weddingpress-audio" data-id="e32d3eb" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;,&quot;_animation_delay&quot;:2000}" data-widget_type="weddingpress-audio.default">
                                    <div class="elementor-widget-container">
                                      <script defer src="data:text/javascript;base64,CgkJCXZhciBzZXR0aW5nQXV0b3BsYXkgPSAneWVzJzsKCQkJd2luZG93LnNldHRpbmdBdXRvcGxheSA9IHNldHRpbmdBdXRvcGxheSA9PT0gJ2Rpc2FibGUnID8gZmFsc2UgOiB0cnVlOwoJCQ=="></script>
                                      <div id="audio-container" class="audio-box">
                                        <audio id="song" src="wp-content/uploads/2023/01/music/<?= $music ?>" loop>
                                          <source src="wp-content/uploads/2023/01/music/<?= $music ?>" type="audio/mp3">
                                        </audio>
                                        <!-- <div class="elementor-icon-wrapper" id="unmute-sound" style="display: none;">
                                          <div class="elementor-icon elementor-animation-shrink">
                                            <svg xmlns="http://www.w3.org/2000/svg" id="f41a8ff0-6f57-4a85-8843-68da76e5cd5b" data-name="Layer 1" viewBox="0 0 50 50">
                                              <title>disk-icon-logo-inv</title>
                                              <path d="M34.5,8.5c.08-.37.2-.82.29-1.18h0c-.25.28-.55.64-.81.9l-.93,1,1.19.64Z" transform="translate(0.05)"></path>
                                              <path d="M25,0A25,25,0,1,0,50,25.06,25.07,25.07,0,0,0,25,0ZM42,11.68,40.74,13.4c-.44.58-.92,1.2-1.4,1.75v0c.67-.3,1.43-.63,2.09-.89l2-.73L44.55,15l-4.62,4-.83-1.06,1.8-1.51c.56-.47,1.24-1,1.94-1.58v0c-.73.32-1.58.66-2.29.91l-2.33.88-.67-.85,1.47-2c.44-.6,1-1.3,1.54-1.9v0c-.67.48-1.43,1-2.06,1.47l-1.9,1.34-.79-1,5.07-3.44ZM34.52,6l1.6.86-1.23,6.27-1.31-.71.33-1.6L32.37,10l-1.13,1.18L30,10.49ZM26.17,3.85l3.73.56-.16,1.11-2.36-.35-.19,1.25,2.22.34-.16,1.11L27,7.54,26.81,9l2.48.37-.16,1.12-3.86-.58ZM25,14.06a11,11,0,0,1,10.88,11A10.87,10.87,0,0,1,25,35.94a10.94,10.94,0,1,1,0-21.88ZM23.35,3.9l.18,1.15-1.65.25.76,4.89-1.37.21-.76-4.88-1.62.25-.18-1.14ZM16.2,5.67l2.5,5.57-1.27.56-2.5-5.56Zm-7,4.63,2.31,1.37c.65.38,1.27.76,1.9,1.21l0,0c-.4-.64-.75-1.29-1.08-1.94L11.13,8.49l1.06-1,2.7,5.85-1.18,1.1L8.1,11.33ZM4.62,16.65l2.57-.06a18,18,0,0,1,2.32.12v0a23.88,23.88,0,0,1-2.3-1.11l-1.57-.84.6-1.12,5.38,2.9-.69,1.27-2.69,0a21.57,21.57,0,0,1-2.4-.09v0c.73.35,1.49.74,2.37,1.21l1.62.87L9.23,21,3.85,18.07ZM3.07,21.41l6,1.14-.26,1.36-6-1.14ZM25,44.46A19.38,19.38,0,0,1,5.57,25.74l1,0a18.44,18.44,0,0,0,36.86,0l1,0A19.57,19.57,0,0,1,25,44.46Zm20.77-22a2.8,2.8,0,0,0,0-1.31c-.15-.57-.49-.78-.8-.7s-.45.47-.54,1.31c-.11,1.14-.52,1.78-1.36,2-1,.26-2-.28-2.46-1.9a3.91,3.91,0,0,1-.08-1.77l1.18,0a3.52,3.52,0,0,0,0,1.58c.16.61.5.87.89.77s.5-.44.57-1.21c.08-1.07.45-1.85,1.31-2.08s2,.37,2.37,1.76A3.58,3.58,0,0,1,47,22.5Z" transform="translate(0.05)"></path>
                                              <path d="M29.74,20.31h.1a1.21,1.21,0,1,0-.1,0Z" transform="translate(0.05)"></path>
                                              <path d="M28.25,23.61h.05c0,1.57.05,3.12.1,4.4l.15,4.11h2.36V21.41H27.17L25,26.77h.75Z" transform="translate(0.05)"></path>
                                              <path d="M20.21,20.31a1.22,1.22,0,0,0,0-2.43h0A1.22,1.22,0,0,0,19,19.09h0A1.22,1.22,0,0,0,20.21,20.31Z" transform="translate(0.05)"></path>
                                              <path d="M19.09,32.12h2.25l.18-4.06c.08-1.34.13-3,.2-4.45h0l2.11,3.16H25L23,21.41H19.08Z" transform="translate(0.05)"></path>
                                            </svg>
                                          </div>
                                        </div> -->
                                        <!-- <div class="elementor-icon-wrapper" id="mute-sound" style="display: none;">
                                          <div class="elementor-icon elementor-animation-shrink">
                                            <svg xmlns="http://www.w3.org/2000/svg" id="f41a8ff0-6f57-4a85-8843-68da76e5cd5b" data-name="Layer 1" viewBox="0 0 50 50">
                                              <title>disk-icon-logo-inv</title>
                                              <path d="M34.5,8.5c.08-.37.2-.82.29-1.18h0c-.25.28-.55.64-.81.9l-.93,1,1.19.64Z" transform="translate(0.05)"></path>
                                              <path d="M25,0A25,25,0,1,0,50,25.06,25.07,25.07,0,0,0,25,0ZM42,11.68,40.74,13.4c-.44.58-.92,1.2-1.4,1.75v0c.67-.3,1.43-.63,2.09-.89l2-.73L44.55,15l-4.62,4-.83-1.06,1.8-1.51c.56-.47,1.24-1,1.94-1.58v0c-.73.32-1.58.66-2.29.91l-2.33.88-.67-.85,1.47-2c.44-.6,1-1.3,1.54-1.9v0c-.67.48-1.43,1-2.06,1.47l-1.9,1.34-.79-1,5.07-3.44ZM34.52,6l1.6.86-1.23,6.27-1.31-.71.33-1.6L32.37,10l-1.13,1.18L30,10.49ZM26.17,3.85l3.73.56-.16,1.11-2.36-.35-.19,1.25,2.22.34-.16,1.11L27,7.54,26.81,9l2.48.37-.16,1.12-3.86-.58ZM25,14.06a11,11,0,0,1,10.88,11A10.87,10.87,0,0,1,25,35.94a10.94,10.94,0,1,1,0-21.88ZM23.35,3.9l.18,1.15-1.65.25.76,4.89-1.37.21-.76-4.88-1.62.25-.18-1.14ZM16.2,5.67l2.5,5.57-1.27.56-2.5-5.56Zm-7,4.63,2.31,1.37c.65.38,1.27.76,1.9,1.21l0,0c-.4-.64-.75-1.29-1.08-1.94L11.13,8.49l1.06-1,2.7,5.85-1.18,1.1L8.1,11.33ZM4.62,16.65l2.57-.06a18,18,0,0,1,2.32.12v0a23.88,23.88,0,0,1-2.3-1.11l-1.57-.84.6-1.12,5.38,2.9-.69,1.27-2.69,0a21.57,21.57,0,0,1-2.4-.09v0c.73.35,1.49.74,2.37,1.21l1.62.87L9.23,21,3.85,18.07ZM3.07,21.41l6,1.14-.26,1.36-6-1.14ZM25,44.46A19.38,19.38,0,0,1,5.57,25.74l1,0a18.44,18.44,0,0,0,36.86,0l1,0A19.57,19.57,0,0,1,25,44.46Zm20.77-22a2.8,2.8,0,0,0,0-1.31c-.15-.57-.49-.78-.8-.7s-.45.47-.54,1.31c-.11,1.14-.52,1.78-1.36,2-1,.26-2-.28-2.46-1.9a3.91,3.91,0,0,1-.08-1.77l1.18,0a3.52,3.52,0,0,0,0,1.58c.16.61.5.87.89.77s.5-.44.57-1.21c.08-1.07.45-1.85,1.31-2.08s2,.37,2.37,1.76A3.58,3.58,0,0,1,47,22.5Z" transform="translate(0.05)"></path>
                                              <path d="M29.74,20.31h.1a1.21,1.21,0,1,0-.1,0Z" transform="translate(0.05)"></path>
                                              <path d="M28.25,23.61h.05c0,1.57.05,3.12.1,4.4l.15,4.11h2.36V21.41H27.17L25,26.77h.75Z" transform="translate(0.05)"></path>
                                              <path d="M20.21,20.31a1.22,1.22,0,0,0,0-2.43h0A1.22,1.22,0,0,0,19,19.09h0A1.22,1.22,0,0,0,20.21,20.31Z" transform="translate(0.05)"></path>
                                              <path d="M19.09,32.12h2.25l.18-4.06c.08-1.34.13-3,.2-4.45h0l2.11,3.16H25L23,21.41H19.08Z" transform="translate(0.05)"></path>
                                            </svg>
                                          </div>
                                        </div> -->
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </section>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <section class="elementor-section elementor-top-section elementor-element elementor-element-a9399bf elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="a9399bf" data-element_type="section" data-settings="{&quot;_ha_eqh_enable&quot;:false}">
            <div class="elementor-container elementor-column-gap-default">
              <div class="elementor-row">
                <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-2f117e4 wdp-sticky-section-no" data-id="2f117e4" data-element_type="column">
                  <div class="elementor-column-wrap elementor-element-populated">
                    <div class="elementor-widget-wrap">
                      <div class="elementor-element elementor-element-5130abf wdp-sticky-section-no elementor-widget elementor-widget-html" data-id="5130abf" data-element_type="widget" data-widget_type="html.default">
                        <div class="elementor-widget-container">
                          <style>
                            /*Tombon*/
                            .content .elementor-button {
                              color: #fff !important;
                              background-color: #7D674C !important
                            }

                            .content .elementor-button:hover {
                              background-color: #534432 !important
                            }

                            /*Text H2*/
                            .content h2 {
                              font-family: "Analogue", Times, serif !important;
                              color: #7D674C !important;
                            }

                            /*Tanggal Event*/
                            .content-date h2 {
                              font-family: "Analogue", Times, serif !important;
                              color: #7D674C !important;
                            }

                            /*Text P*/
                            .content p {
                              color: #7D674C !important;
                            }
                          </style>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-a5d8c36 wdp-sticky-section-no elementor-widget elementor-widget-html" data-id="a5d8c36" data-element_type="widget" data-widget_type="html.default">
                        <div class="elementor-widget-container">
                          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
                          <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
                          <script defer src="data:text/javascript;base64,DQp2YXIgczAgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnczAnKTsNCnZhciBiX28gPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnYl9vJyk7DQp2YXIgY29udGVudCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCJjb250ZW50Iik7DQp2YXIgeCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCJzb25nIik7DQp2YXIgdmlkZW9faWQgPSBkb2N1bWVudC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKCJlbGVtZW50b3ItdmlkZW8iKTsNCnZhciB4ID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoInNvbmciKTsNCnZhciBlbGVtID0gZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50Ow0KZGlzYWJsZVNjcm9sbGluZygpOw0KZG9jdW1lbnQuYm9keS5zdHlsZS5oZWlndGg9IjEwMHZoIg0KDQpiX28ub25jbGljayA9IGZ1bmN0aW9uKCl7ICAgDQogICAgZW5hYmxlU2Nyb2xsaW5nKCk7DQogICAgcGxheUF1ZGlvKCk7DQogICAgY2xvc2VDb3ZlcigpOw0KICAgIHdpbmRvdy5zY3JvbGxUbygwLCAwKTsNCn07DQoNCnZpZGVvX2lkWzBdLm9ucGxheSA9IGZ1bmN0aW9uKCl7IA0KICBwYXVzZUF1ZGlvKCkNCn07IA0KDQp2aWRlb19pZFswXS5vbnBhdXNlID0gZnVuY3Rpb24oKXsgDQogIHBsYXlBdWRpbygpDQp9Ow0KDQpmdW5jdGlvbiBwbGF5QXVkaW8oKSB7IA0KICB4LnBsYXkoKTsgDQp9IA0KDQpmdW5jdGlvbiBwYXVzZUF1ZGlvKCkgeyANCiAgeC5wYXVzZSgpOyANCn0NCg0KZnVuY3Rpb24gb3BlbkZ1bGxzY3JlZW4oKSB7DQogIGlmIChlbGVtLnJlcXVlc3RGdWxsc2NyZWVuKSB7DQogICAgZWxlbS5yZXF1ZXN0RnVsbHNjcmVlbigpOw0KICB9IGVsc2UgaWYgKGVsZW0ud2Via2l0UmVxdWVzdEZ1bGxzY3JlZW4pIHsgLyogU2FmYXJpICovDQogICAgZWxlbS53ZWJraXRSZXF1ZXN0RnVsbHNjcmVlbigpOw0KICB9IGVsc2UgaWYgKGVsZW0ubXNSZXF1ZXN0RnVsbHNjcmVlbikgeyAvKiBJRTExICovDQogICAgZWxlbS5tc1JlcXVlc3RGdWxsc2NyZWVuKCk7DQogIH0NCn0NCg0KZnVuY3Rpb24gZGlzYWJsZVNjcm9sbGluZygpeyB2YXIgeD13aW5kb3cuc2Nyb2xsWDsgdmFyIHk9d2luZG93LnNjcm9sbFk7IHdpbmRvdy5vbnNjcm9sbD1mdW5jdGlvbigpe3dpbmRvdy5zY3JvbGxUbyh4LCB5KTt9OyB9DQoNCmZ1bmN0aW9uIGVuYWJsZVNjcm9sbGluZygpeyB3aW5kb3cub25zY3JvbGw9ZnVuY3Rpb24oKXt9OyB9DQoNCmZ1bmN0aW9uIGNsb3NlQ292ZXIoKSB7DQoJJCgiI3MwIikuYW5pbWF0ZSh7IG9wYWNpdHk6IDAsIHRvcDogIi0xMDAlIn0sIDI1MDAsICdlYXNlSW5PdXRDdWJpYycpOw0KCSQoIiNzMCIpLmhpZGUoMjUwMCk7DQp9DQo="></script>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
    <script defer src="data:text/javascript;base64,CmxldCB2aCA9IHdpbmRvdy5pbm5lckhlaWdodCAqIDAuMDE7CmRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5zdHlsZS5zZXRQcm9wZXJ0eSgnLS12aCcsIGAke3ZofXB4YCk7Cgp3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcigncmVzaXplJywgKCkgPT4gewogIGxldCB2aCA9IHdpbmRvdy5pbm5lckhlaWdodCAqIDAuMDE7CiAgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LnN0eWxlLnNldFByb3BlcnR5KCctLXZoJywgYCR7dmh9cHhgKTsKfSk7CgkK"></script>
    <div data-elementor-type="popup" data-elementor-id="50158" data-post-id="128245" data-obj-id="128245" class="elementor elementor-50158 dce-elementor-post-128245 elementor-location-popup" data-elementor-settings="{&quot;entrance_animation&quot;:&quot;fadeIn&quot;,&quot;exit_animation&quot;:&quot;fadeIn&quot;,&quot;entrance_animation_duration&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:0.5,&quot;sizes&quot;:[]},&quot;triggers&quot;:{&quot;page_load&quot;:&quot;yes&quot;,&quot;page_load_delay&quot;:0},&quot;timing&quot;:{&quot;times_times&quot;:1,&quot;times&quot;:&quot;yes&quot;}}">
      <div class="elementor-section-wrap">
        <section class="elementor-section elementor-top-section elementor-element elementor-element-2533689 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="2533689" data-element_type="section" data-settings="{&quot;_ha_eqh_enable&quot;:false}">
          <div class="elementor-container elementor-column-gap-default">
            <div class="elementor-row">
              <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-131bef9 wdp-sticky-section-no" data-id="131bef9" data-element_type="column">
                <div class="elementor-column-wrap elementor-element-populated">
                  <div class="elementor-widget-wrap">
                    <div class="elementor-element elementor-element-093b077 elementor-aspect-ratio-11 elementor-widget__width-initial dce_masking-none wdp-sticky-section-no elementor-widget elementor-widget-video" data-id="093b077" data-element_type="widget" data-settings="{&quot;video_type&quot;:&quot;hosted&quot;,&quot;autoplay&quot;:&quot;yes&quot;,&quot;play_on_mobile&quot;:&quot;yes&quot;,&quot;mute&quot;:&quot;yes&quot;,&quot;loop&quot;:&quot;yes&quot;,&quot;aspect_ratio&quot;:&quot;11&quot;}" data-widget_type="video.default">
                      <div class="elementor-widget-container">
                        <div class="e-hosted-video elementor-wrapper elementor-fit-aspect-ratio elementor-open-inline">
                          <video class="elementor-video" src="wp-content/uploads/2022/08/81949-toggle-dark-mode-light-mode-themes.mp4" autoplay="" loop="" muted="muted" playsinline="" controlsList="nodownload"></video>
                        </div>
                      </div>
                    </div>
                    <div class="elementor-element elementor-element-7075053 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="7075053" data-element_type="widget" data-widget_type="heading.default">
                      <div class="elementor-widget-container">
                        <h2 class="elementor-heading-title elementor-size-default">Untuk mendapatkan tampilan terbaik <b>nonaktifkan dark mode / mode gelap</b>
                          <br>dan gunakan <b>chrome</b> atau <b>safari</b>.
                        </h2>
                      </div>
                    </div>
                    <div class="elementor-element elementor-element-6623270 elementor-align-center elementor-widget__width-auto wdp-sticky-section-no elementor-widget elementor-widget-button" data-id="6623270" data-element_type="widget" data-widget_type="button.default">
                      <div class="elementor-widget-container">
                        <div class="elementor-button-wrapper">
                          <a href="#elementor-action%3Aaction%3Dpopup%3Aclose%26settings%3DeyJkb19ub3Rfc2hvd19hZ2FpbiI6InllcyJ9" class="elementor-button-link elementor-button elementor-size-sm elementor-animation-shrink" role="button">
                            <span class="elementor-button-content-wrapper">
                              <span class="elementor-button-text">Saya Mengerti</span>
                            </span>
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
    <div data-elementor-type="popup" data-elementor-id="616" data-post-id="128245" data-obj-id="128245" class="elementor elementor-616 dce-elementor-post-128245 elementor-location-popup" data-elementor-settings="{&quot;entrance_animation&quot;:&quot;fadeIn&quot;,&quot;exit_animation&quot;:&quot;fadeIn&quot;,&quot;prevent_scroll&quot;:&quot;yes&quot;,&quot;entrance_animation_duration&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:0.8,&quot;sizes&quot;:[]},&quot;triggers&quot;:[],&quot;timing&quot;:{&quot;devices&quot;:&quot;yes&quot;,&quot;devices_devices&quot;:[&quot;desktop&quot;,&quot;tablet&quot;,&quot;mobile&quot;]}}">
      <div class="elementor-section-wrap">
        <section class="elementor-section elementor-top-section elementor-element elementor-element-213c45d8 elementor-section-height-min-height elementor-section-boxed elementor-section-height-default elementor-section-items-middle wdp-sticky-section-no" data-id="213c45d8" data-element_type="section" data-settings="{&quot;_ha_eqh_enable&quot;:false}">
          <div class="elementor-container elementor-column-gap-default">
            <div class="elementor-row">
              <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-175d12ff wdp-sticky-section-no" data-id="175d12ff" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                <div class="elementor-column-wrap elementor-element-populated">
                  <div class="elementor-widget-wrap">
                    <div class="elementor-element elementor-element-d8834be wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="d8834be" data-element_type="widget" data-widget_type="heading.default">
                      <div class="elementor-widget-container">
                        <h2 class="elementor-heading-title elementor-size-default">- Abdan Uke -</h2>
                      </div>
                    </div>
                    <div class="elementor-element elementor-element-3ce7eba0 wdp-sticky-section-no elementor-widget elementor-widget-dce_barcode" data-id="3ce7eba0" data-element_type="widget" data-widget_type="dce_barcode.default">
                      <div class="elementor-widget-container">
                        <img class="dce-barcode dce-barcode-png" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAANIAAADSAQMAAAAFVwwBAAAABlBMVEX///8zMzM4VIyRAAAAAXRSTlMAQObYZgAAAAlwSFlzAAAOxAAADsQBlSsOGwAAAN1JREFUWIXtmMENwCAMAy0xACOxOiN1ACQaEhJA7QYmjxbl+rJiA0Wf1QDkhl6zdyqoWQWsrSUsVuRsiFRNLhduSHjZZEuzyw5WntQvCxYeK9Lb3cbNIpPdaH95zcdWLaN9i4+ZXCaS28sHiptJ4shbHiN7xGjNejpnzExqdsZqfCXty1Qz3aa+iUPNjuuB9Sp8r6Jlh3rwjRypA9RsjlD3k54NlJ4DuVlVwdbdCSkCiJvFndI0e3yqcNkmV9FZmh67TGfJ06j//J/gY5vHhl5xBAQ3W5lsmuV18CNmL3BWpCSno4lRAAAAAElFTkSuQmCC">
                      </div>
                    </div>
                    <div class="elementor-element elementor-element-d89ac53 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="d89ac53" data-element_type="widget" data-widget_type="heading.default">
                      <div class="elementor-widget-container">
                        <h6 class="elementor-heading-title elementor-size-default">
                          <center>Scan QR Code untuk check-in di lokasi.</center>
                        </h6>
                      </div>
                    </div>
                    <section class="elementor-section elementor-inner-section elementor-element elementor-element-648a52e elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="648a52e" data-element_type="section" data-settings="{&quot;_ha_eqh_enable&quot;:false}">
                      <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                          <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-af203c5 wdp-sticky-section-no" data-id="af203c5" data-element_type="column">
                            <div class="elementor-column-wrap elementor-element-populated">
                              <div class="elementor-widget-wrap">
                                <div class="elementor-element elementor-element-c717ebe elementor-view-framed elementor-widget__width-auto elementor-shape-circle wdp-sticky-section-no elementor-widget elementor-widget-icon" data-id="c717ebe" data-element_type="widget" data-widget_type="icon.default">
                                  <div class="elementor-widget-container">
                                    <div class="elementor-icon-wrapper">
                                      <div class="elementor-icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" id="e6452766-98c1-4deb-a691-b255b3232226" data-name="Layer 1" viewBox="0 0 50 59.56">
                                          <defs>
                                            <style>
                                              .af59355c-1891-45e0-9269-0f4994d98bb7 {
                                                fill-rule: evenodd;
                                              }
                                            </style>
                                          </defs>
                                          <title>logo-icon</title>
                                          <path class="af59355c-1891-45e0-9269-0f4994d98bb7" d="M351.22,350.73a5.08,5.08,0,1,1,0,10.15,5.08,5.08,0,0,1,0-10.15Zm-20,37.19-8.15-22.44H306.65l.06,44.81h9.39l.77-17c.35-5.61.53-12.52.82-18.6h.12l8.86,13.23Zm15.05,22.37h9.86l-.06-44.81H340.48l-9,22.44h3.13L345,374.69h.23c0,6.56.18,13.05.42,18.42Zm-34.88-59.56a5.08,5.08,0,1,1-5.08,5.07,5.07,5.07,0,0,1,5.08-5.07Z" transform="translate(-306.3 -350.73)"></path>
                                        </svg>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="elementor-element elementor-element-8f2b67f animated-slow elementor-widget__width-auto wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="8f2b67f" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;none&quot;}" data-widget_type="heading.default">
                                  <div class="elementor-widget-container">
                                    <p class="elementor-heading-title elementor-size-default">wedding</p>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </section>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
    <script defer id="wdp_js_script-js-extra" src="data:text/javascript;base64,Ci8qIDwhW0NEQVRBWyAqLwp2YXIgV0RQX1dQID0geyJhamF4dXJsIjoiaHR0cHM6XC9cL2ludml0ZWFtcy5jb21cL3dwLWFkbWluXC9hZG1pbi1hamF4LnBocCIsIndkcE5vbmNlIjoiODk5YmI2MGZlYyIsImpwYWdlcyI6InRydWUiLCJqUGFnZXNOdW0iOiIxMDAwIiwidGV4dENvdW50ZXIiOiJ0cnVlIiwidGV4dENvdW50ZXJOdW0iOiI1MDAiLCJ3aWR0aFdyYXAiOiIiLCJhdXRvTG9hZCI6InRydWUiLCJ0aGFua3NDb21tZW50IjoiVGVyaW1hIGthc2loIGF0YXMgdWNhcGFuICYgZG9hbnlhISIsInRoYW5rc1JlcGx5Q29tbWVudCI6IlRlcmltYSBrYXNpaCBhdGFzIGJhbGFzYW5ueWEhIiwiZHVwbGljYXRlQ29tbWVudCI6IllvdSBtaWdodCBoYXZlIGxlZnQgb25lIG9mIHRoZSBmaWVsZHMgYmxhbmssIG9yIGR1cGxpY2F0ZSBjb21tZW50cyIsImluc2VydEltYWdlIjoiSW5zZXJ0IGltYWdlIiwiaW5zZXJ0VmlkZW8iOiJJbnNlcnQgdmlkZW8iLCJpbnNlcnRMaW5rIjoiSW5zZXJ0IGxpbmsiLCJjaGVja1ZpZGVvIjoiQ2hlY2sgdmlkZW8iLCJhY2NlcHQiOiJBY2NlcHQiLCJjYW5jZWwiOiJDYW5jZWwiLCJyZXBseSI6IkJhbGFzIiwidGV4dFdyaXRlQ29tbWVudCI6IlR1bGlzIFVjYXBhbiAmIERvYSIsImNsYXNzUG9wdWxhckNvbW1lbnQiOiJ3ZHAtcG9wdWxhci1jb21tZW50IiwidGV4dFVybEltYWdlIjoiVXJsIGltYWdlIiwidGV4dFVybFZpZGVvIjoiVXJsIHZpZGVvIHlvdXR1YmUgb3IgdmltZW8iLCJ0ZXh0VXJsTGluayI6IlVybCBsaW5rIiwidGV4dFRvRGlzcGxheSI6IlRleHQgdG8gZGlzcGxheSIsInRleHRDaGFyYWN0ZXJlc01pbiI6Ik1pbmltYWwgMiBrYXJha3RlciIsInRleHROYXZOZXh0IjoiU2VsYW5qdXRueWEiLCJ0ZXh0TmF2UHJldiI6IlNlYmVsdW1ueWEiLCJ0ZXh0TXNnRGVsZXRlQ29tbWVudCI6IkRvIHlvdSB3YW50IGRlbGV0ZSB0aGlzIGNvbW1lbnQ/IiwidGV4dExvYWRNb3JlIjoiTG9hZCBtb3JlIiwidGV4dExpa2VzIjoiTGlrZXMifTsKLyogXV0+ICovCg=="></script>
    <script type='text/javascript' src='wp-content/plugins/weddingpress/addons/comment-kit//js/wdp_script.js?ver=2.7.6' id='wdp_js_script-js'></script>
    <script type='text/javascript' src='wp-content/plugins/weddingpress/addons/comment-kit//js/libs/jquery.jPages.min.js?ver=0.7' id='wdp_jPages-js'></script>
    <script type='text/javascript' src='wp-content/plugins/weddingpress/addons/comment-kit//js/libs/jquery.textareaCounter.js?ver=2.0' id='wdp_textCounter-js'></script>
    <script type='text/javascript' src='wp-content/plugins/weddingpress/addons/comment-kit//js/libs/jquery.placeholder.min.js?ver=2.0.7' id='wdp_placeholder-js'></script>
    <script defer type='text/javascript' src='wp-content/plugins/weddingpress/addons/comment-kit//js/libs/autosize.min.js?ver=1.14' id='wdp_autosize-js'></script>
    <script defer type='text/javascript' src='wp-content/plugins/weddingpress/assets/js/wdp-swiper.min.js' id='wdp-swiper-js-js'></script>
    <script defer type='text/javascript' src='wp-content/cache/autoptimize/autoptimize_single_8afa9f827ca7b244f1993984bfa26cf4.js' id='weddingpress-qr-js'></script>
    <script defer type='text/javascript' src='wp-content/cache/autoptimize/autoptimize_single_b9c1ff6c95afe5367997309e4fb77325.js' id='wdp-horizontal-js-js'></script>
    <script defer type='text/javascript' src='wp-content/plugins/weddingpress/assets/js/exad-scripts.min.js?ver=2.8.5' id='exad-main-script-js'></script>
    <script defer id="happy-elementor-addons-js-extra" src="data:text/javascript;base64,Ci8qIDwhW0NEQVRBWyAqLwp2YXIgSGFwcHlMb2NhbGl6ZSA9IHsiYWpheF91cmwiOiJodHRwczpcL1wvaW52aXRlYW1zLmNvbVwvd3AtYWRtaW5cL2FkbWluLWFqYXgucGhwIiwibm9uY2UiOiJlNzc1N2Q5OTVkIn07Ci8qIF1dPiAqLwo="></script>
    <script defer type='text/javascript' src='wp-content/plugins/2/assets/js/happy-addons.min.js?ver=3.3.0' id='happy-elementor-addons-js'></script>
    <script defer type='text/javascript' src='wp-content/plugins/6d/assets/js/dynamic-countdown.min.js?ver=2.2.5' id='dce-dynamic-countdown-js'></script>
    <script defer type='text/javascript' src='wp-content/plugins/6d/assets/lib/dayjs/dayjs.min.js?ver=2.2.5' id='dce-dayjs-js'></script>
    <script defer type='text/javascript' src='wp-content/plugins/elementor-pro/assets/lib/lottie/lottie.min.js?ver=5.6.6' id='lottie-js'></script>
    <script defer type='text/javascript' src='wp-content/plugins/elementor/assets/lib/e-gallery/js/e-gallery.min.js?ver=1.2.0' id='elementor-gallery-js'></script>
    <script defer type='text/javascript' src='wp-content/plugins/6d/assets/lib/clipboard.js/clipboard.min.js?ver=2.2.5' id='dce-clipboard-js-js'></script>
    <script defer type='text/javascript' src='wp-content/plugins/elementor-pro/assets/js/webpack-pro.runtime.min.js?ver=3.4.2' id='elementor-pro-webpack-runtime-js'></script>
    <script defer type='text/javascript' src='wp-content/plugins/elementor/assets/js/webpack.runtime.min.js?ver=3.5.6' id='elementor-webpack-runtime-js'></script>
    <script defer type='text/javascript' src='wp-content/plugins/elementor/assets/js/frontend-modules.min.js?ver=3.5.6' id='elementor-frontend-modules-js'></script>
    <script defer id="elementor-pro-frontend-js-before" src="data:text/javascript;base64,CnZhciBFbGVtZW50b3JQcm9Gcm9udGVuZENvbmZpZyA9IHsiYWpheHVybCI6Imh0dHBzOlwvXC9pbnZpdGVhbXMuY29tXC93cC1hZG1pblwvYWRtaW4tYWpheC5waHAiLCJub25jZSI6IjIwMWJkMTcyMjgiLCJ1cmxzIjp7ImFzc2V0cyI6Imh0dHBzOlwvXC9pbnZpdGVhbXMuY29tXC93cC1jb250ZW50XC9wbHVnaW5zXC9lbGVtZW50b3ItcHJvXC9hc3NldHNcLyIsInJlc3QiOiJodHRwczpcL1wvaW52aXRlYW1zLmNvbVwvd3AtanNvblwvIn0sImkxOG4iOnsidG9jX25vX2hlYWRpbmdzX2ZvdW5kIjoiTm8gaGVhZGluZ3Mgd2VyZSBmb3VuZCBvbiB0aGlzIHBhZ2UuIn0sInNoYXJlQnV0dG9uc05ldHdvcmtzIjp7ImZhY2Vib29rIjp7InRpdGxlIjoiRmFjZWJvb2siLCJoYXNfY291bnRlciI6dHJ1ZX0sInR3aXR0ZXIiOnsidGl0bGUiOiJUd2l0dGVyIn0sImdvb2dsZSI6eyJ0aXRsZSI6Ikdvb2dsZSsiLCJoYXNfY291bnRlciI6dHJ1ZX0sImxpbmtlZGluIjp7InRpdGxlIjoiTGlua2VkSW4iLCJoYXNfY291bnRlciI6dHJ1ZX0sInBpbnRlcmVzdCI6eyJ0aXRsZSI6IlBpbnRlcmVzdCIsImhhc19jb3VudGVyIjp0cnVlfSwicmVkZGl0Ijp7InRpdGxlIjoiUmVkZGl0IiwiaGFzX2NvdW50ZXIiOnRydWV9LCJ2ayI6eyJ0aXRsZSI6IlZLIiwiaGFzX2NvdW50ZXIiOnRydWV9LCJvZG5va2xhc3NuaWtpIjp7InRpdGxlIjoiT0siLCJoYXNfY291bnRlciI6dHJ1ZX0sInR1bWJsciI6eyJ0aXRsZSI6IlR1bWJsciJ9LCJkaWdnIjp7InRpdGxlIjoiRGlnZyJ9LCJza3lwZSI6eyJ0aXRsZSI6IlNreXBlIn0sInN0dW1ibGV1cG9uIjp7InRpdGxlIjoiU3R1bWJsZVVwb24iLCJoYXNfY291bnRlciI6dHJ1ZX0sIm1peCI6eyJ0aXRsZSI6Ik1peCJ9LCJ0ZWxlZ3JhbSI6eyJ0aXRsZSI6IlRlbGVncmFtIn0sInBvY2tldCI6eyJ0aXRsZSI6IlBvY2tldCIsImhhc19jb3VudGVyIjp0cnVlfSwieGluZyI6eyJ0aXRsZSI6IlhJTkciLCJoYXNfY291bnRlciI6dHJ1ZX0sIndoYXRzYXBwIjp7InRpdGxlIjoiV2hhdHNBcHAifSwiZW1haWwiOnsidGl0bGUiOiJFbWFpbCJ9LCJwcmludCI6eyJ0aXRsZSI6IlByaW50In19LCJmYWNlYm9va19zZGsiOnsibGFuZyI6ImlkX0lEIiwiYXBwX2lkIjoiIn0sImxvdHRpZSI6eyJkZWZhdWx0QW5pbWF0aW9uVXJsIjoiaHR0cHM6XC9cL2ludml0ZWFtcy5jb21cL3dwLWNvbnRlbnRcL3BsdWdpbnNcL2VsZW1lbnRvci1wcm9cL21vZHVsZXNcL2xvdHRpZVwvYXNzZXRzXC9hbmltYXRpb25zXC9kZWZhdWx0Lmpzb24ifX07Cg=="></script>
    <script defer type='text/javascript' src='wp-content/plugins/elementor-pro/assets/js/frontend.min.js?ver=3.4.2' id='elementor-pro-frontend-js'></script>
    <script defer type='text/javascript' src='wp-content/plugins/elementor/assets/lib/waypoints/waypoints.min.js?ver=4.0.2' id='elementor-waypoints-js'></script>
    <script type='text/javascript' src='wp-includes/js/jquery/ui/core.min.js?ver=1.13.2' id='jquery-ui-core-js'></script>
    <script defer type='text/javascript' src='wp-content/plugins/elementor/assets/lib/swiper/swiper.min.js?ver=5.3.6' id='swiper-js'></script>
    <script defer type='text/javascript' src='wp-content/plugins/elementor/assets/lib/share-link/share-link.min.js?ver=3.5.6' id='share-link-js'></script>
    <script defer type='text/javascript' src='wp-content/plugins/elementor/assets/lib/dialog/dialog.min.js?ver=4.9.0' id='elementor-dialog-js'></script>
    <script defer id="elementor-frontend-js-before" src="data:text/javascript;base64,CnZhciBlbGVtZW50b3JGcm9udGVuZENvbmZpZyA9IHsiZW52aXJvbm1lbnRNb2RlIjp7ImVkaXQiOmZhbHNlLCJ3cFByZXZpZXciOmZhbHNlLCJpc1NjcmlwdERlYnVnIjpmYWxzZX0sImkxOG4iOnsic2hhcmVPbkZhY2Vib29rIjoiQmFnaWthbiBkaSBGYWNlYm9vayIsInNoYXJlT25Ud2l0dGVyIjoiQmFnaWthbiBkaSBUd2l0dGVyIiwicGluSXQiOiJCdWF0IFBpbiIsImRvd25sb2FkIjoiVW5kdWgiLCJkb3dubG9hZEltYWdlIjoiVW5kdWggZ2FtYmFyIiwiZnVsbHNjcmVlbiI6IkxheWFyIFBlbnVoIiwiem9vbSI6IlBlcmJlc2FyIiwic2hhcmUiOiJCYWdpa2FuIiwicGxheVZpZGVvIjoiUHV0YXIgVmlkZW8iLCJwcmV2aW91cyI6IlNlYmVsdW1ueWEiLCJuZXh0IjoiU2VsYW5qdXRueWEiLCJjbG9zZSI6IlR1dHVwIn0sImlzX3J0bCI6ZmFsc2UsImJyZWFrcG9pbnRzIjp7InhzIjowLCJzbSI6NDgwLCJtZCI6NzY4LCJsZyI6MTAyNSwieGwiOjE0NDAsInh4bCI6MTYwMH0sInJlc3BvbnNpdmUiOnsiYnJlYWtwb2ludHMiOnsibW9iaWxlIjp7ImxhYmVsIjoiUG9uc2VsIiwidmFsdWUiOjc2NywiZGVmYXVsdF92YWx1ZSI6NzY3LCJkaXJlY3Rpb24iOiJtYXgiLCJpc19lbmFibGVkIjp0cnVlfSwibW9iaWxlX2V4dHJhIjp7ImxhYmVsIjoiRWtzdHJhIFNlbHVsZXIiLCJ2YWx1ZSI6ODgwLCJkZWZhdWx0X3ZhbHVlIjo4ODAsImRpcmVjdGlvbiI6Im1heCIsImlzX2VuYWJsZWQiOmZhbHNlfSwidGFibGV0Ijp7ImxhYmVsIjoiVGFibGV0IiwidmFsdWUiOjEwMjQsImRlZmF1bHRfdmFsdWUiOjEwMjQsImRpcmVjdGlvbiI6Im1heCIsImlzX2VuYWJsZWQiOnRydWV9LCJ0YWJsZXRfZXh0cmEiOnsibGFiZWwiOiJUYWJsZXQgRWtzdHJhIiwidmFsdWUiOjEyMDAsImRlZmF1bHRfdmFsdWUiOjEyMDAsImRpcmVjdGlvbiI6Im1heCIsImlzX2VuYWJsZWQiOmZhbHNlfSwibGFwdG9wIjp7ImxhYmVsIjoiTGFwdG9wIiwidmFsdWUiOjEzNjYsImRlZmF1bHRfdmFsdWUiOjEzNjYsImRpcmVjdGlvbiI6Im1heCIsImlzX2VuYWJsZWQiOmZhbHNlfSwid2lkZXNjcmVlbiI6eyJsYWJlbCI6IkxheWFyIGxlYmFyIiwidmFsdWUiOjI0MDAsImRlZmF1bHRfdmFsdWUiOjI0MDAsImRpcmVjdGlvbiI6Im1pbiIsImlzX2VuYWJsZWQiOmZhbHNlfX19LCJ2ZXJzaW9uIjoiMy41LjYiLCJpc19zdGF0aWMiOmZhbHNlLCJleHBlcmltZW50YWxGZWF0dXJlcyI6eyJlX2ltcG9ydF9leHBvcnQiOnRydWUsImVfaGlkZGVuX3dvcmRwcmVzc193aWRnZXRzIjp0cnVlLCJsYW5kaW5nLXBhZ2VzIjp0cnVlLCJlbGVtZW50cy1jb2xvci1waWNrZXIiOnRydWUsImZhdm9yaXRlLXdpZGdldHMiOnRydWUsImFkbWluLXRvcC1iYXIiOnRydWUsImZvcm0tc3VibWlzc2lvbnMiOnRydWV9LCJ1cmxzIjp7ImFzc2V0cyI6Imh0dHBzOlwvXC9pbnZpdGVhbXMuY29tXC93cC1jb250ZW50XC9wbHVnaW5zXC9lbGVtZW50b3JcL2Fzc2V0c1wvIn0sInNldHRpbmdzIjp7InBhZ2UiOltdLCJlZGl0b3JQcmVmZXJlbmNlcyI6W10sImR5bmFtaWNvb28iOltdfSwia2l0Ijp7ImJvZHlfYmFja2dyb3VuZF9iYWNrZ3JvdW5kIjoiY2xhc3NpYyIsImFjdGl2ZV9icmVha3BvaW50cyI6WyJ2aWV3cG9ydF9tb2JpbGUiLCJ2aWV3cG9ydF90YWJsZXQiXSwiZ2xvYmFsX2ltYWdlX2xpZ2h0Ym94IjoieWVzIiwibGlnaHRib3hfZW5hYmxlX2NvdW50ZXIiOiJ5ZXMiLCJsaWdodGJveF9lbmFibGVfZnVsbHNjcmVlbiI6InllcyIsImxpZ2h0Ym94X2VuYWJsZV96b29tIjoieWVzIn0sInBvc3QiOnsiaWQiOjEyODI0NSwidGl0bGUiOiJUSEUlMjBXRURESU5HJTIwT0YlM0ElMjBJcmZhbiUyMCUyNiUyMFZpdGEiLCJleGNlcnB0IjoiIiwiZmVhdHVyZWRJbWFnZSI6Imh0dHBzOlwvXC9pbnZpdGVhbXMuY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIzXC8wMVwvdGh1bWJuYWlsX2lyZmFuLXZpdGEuanBnIn19Owo="></script>
    <script defer type='text/javascript' src='wp-content/plugins/elementor/assets/js/frontend.min.js?ver=3.5.6' id='elementor-frontend-js'></script>
    <script defer type='text/javascript' src='wp-content/plugins/elementor-pro/assets/js/preloaded-elements-handlers.min.js?ver=3.4.2' id='pro-preloaded-elements-handlers-js'></script>
    <script defer type='text/javascript' src='wp-content/plugins/elementor/assets/js/preloaded-modules.min.js?ver=3.5.6' id='preloaded-modules-js'></script>
    <script type='text/javascript' src='wp-content/plugins/elementor-pro/assets/lib/sticky/jquery.sticky.min.js?ver=3.4.2' id='e-sticky-js'></script>
    <script defer id="weddingpress-wdp-js-extra" src="data:text/javascript;base64,Ci8qIDwhW0NEQVRBWyAqLwp2YXIgY2V2YXIgPSB7ImFqYXhfdXJsIjoiaHR0cHM6XC9cL2ludml0ZWFtcy5jb21cL3dwLWFkbWluXC9hZG1pbi1hamF4LnBocCIsInBsdWdpbl91cmwiOiJodHRwczpcL1wvaW52aXRlYW1zLmNvbVwvd3AtY29udGVudFwvcGx1Z2luc1wvd2VkZGluZ3ByZXNzXC8ifTsKLyogXV0+ICovCg=="></script>
    <script defer type='text/javascript' src='wp-content/plugins/weddingpress/assets/js/wdp.min.js?ver=2.8.5' id='weddingpress-wdp-js'></script>
    <script defer type='text/javascript' src='wp-content/plugins/6d/assets/js/settings.min.js?ver=2.2.5' id='dce-settings-js'></script>
    <script defer type='text/javascript' src='wp-content/plugins/6d/assets/js/fix-background-loop.min.js?ver=2.2.5' id='dce-fix-background-loop-js'></script>
    <script type="text/javascript">
        $('#tombolSubmit').on("click",function(){
          var author = $('#author').val();
          var hubungan = $('#hubungan').val();
          var konfirmasi = $('#konfirmasi').val();
          var comment = $('#comment').val();
          console.log(comment);
          if (author != '' && hubungan != '' && konfirmasi != '' && comment != '') {
            $('#wedding_comment').append(`
              <li class="comment even thread-even depth-1 wdp-item-comment" id="wdp-item-comment-63709" data-likes="0">
                <div id="wdp-comment-63709" class="wdp-comment wdp-clearfix">
                <div class="wdp-comment-avatar">
                    <img alt src="https://ui-avatars.com/api/?name=P&rounded=true&bold=true&background=fcb1fe&format=svg&color=ffffff" srcset="https://ui-avatars.com/api/?name=P&rounded=true&bold=true&background=fcb1fe&format=svg&color=ffffff 2x" height="96" width="96"></img>
                </div><!--.wdp-comment-avatar-->
                <div class="wdp-comment-content">
                    <div class="wdp-comment-info">
                        <a class='wdp-commenter-name' title='${author}'>${author}</a>
                        <span class=wdp-post-author><i class='fas fa-check-circle'></i> ${konfirmasi}</span>
                        <br>
                        <span class="hubungan">
                        — ${hubungan}
                        </span>
                    </div>
                    <!--.wdp-comment-info-->
                    <div class="wdp-comment-text">
                        <p>${comment}</p>
                    </div>
                    <!--.wdp-comment-text-->
                    <div class="wdp-comment-actions">
                    </div>
                </div>
              </li><!-- #comment-## -->
            `);
          }
          // location.reload();
        })

        function name(params) {
           
        }
    </script>
  </body>
</html>